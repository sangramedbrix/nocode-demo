# hexa-nocode-web

## Getting started

To install dependencies:

```
npm i
```

or

```
yarn
```

To run:

```
npm start
```

or

```
yarn start
```

Access http://localhost:3000
