import React from 'react';
import RecentAppsBox from '../../components/Cards/RecentAppsBox';
import RecentBlocksBox from '../../components/Cards/RecentBlocksBox';
import RecentApp from '../../assets/images/img1.png';
import RecentBlocks from '../../assets/images/img2.png';
import RecentActivitiesList from '../../components/Lists';
import { Col, Row } from 'antd';
const AppComponent = () => {
  return (
    <>
      <div className="d-card-container pad-3">
        <Row className="dash-cards" gutter={[16, 25]}>
          <Col xl={6} lg={6} md={12} sm={12} xs={24}>
            <RecentAppsBox
              card_title="My Recent Apps"
              card_description="Create new App and edit Existing App from the list"
              card_img_src={RecentApp}
              recent_apps_list_item_heading="Staff Management"
              recent_apps_list_item_page="0 Pages"
              recent_apps_list_item_date="Last Edited On: 21/11/2021, 10:15:29 AM"
            />
          </Col>
          <Col xl={6} lg={6} md={12} sm={12} xs={24}>
            <RecentBlocksBox
              card_title="My Recent Blocks"
              card_description="Create new App and edit Existing App from the list"
              card_img_src={RecentBlocks}
              recent_apps_list_item_heading="Staff Management"
              recent_apps_list_item_page="0 Pages"
              recent_apps_list_item_date="Last Edited On: 21/11/2021, 10:15:29 AM"
            />
          </Col>
          <Col xl={12} lg={12} md={24} sm={24} xs={24}>
            <RecentActivitiesList
              card_title="My Recent Activities"
              card_description="Create new App and edit Existing App from the list"
              card_header="Student Card With Image"
              activity_status="Pending"
              card_desc=" Last Edited On: 21/11/2021, 10:15:29 AM"
            />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default AppComponent;
