import React, { useState, useEffect } from 'react';
import { Table, Modal, Button, Form, Row, Col, Input, Select, DatePicker } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import ViewBoxes from '../../components/ViewBoxes/Index';
import { useLocation } from 'react-router-dom';
import '../../config/config';
import axios from 'axios';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import TextHeadingH4 from '../../components/TextHeadings/TextHeadingH4';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import img from '../../assets/images/app_details_img.png';
import '../../components/Popup/popup.css';
import { useNavigate } from 'react-router-dom';
const { Option, OptGroup } = Select;

import moment from 'moment';

const TableList = (props) => {
  const { state } = useLocation();
  const navigate = useNavigate();
  const [modules, setModules] = useState([]);
  const [dropDownData, setModulesw] = useState([]);
  const [states, setstate] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisiblePage, setIsModalVisiblePage] = useState(false);
  const [pageId, setPageId] = useState();
  const [app, setApp] = useState([]);
  // const [newAppId, setAppId] = useState(0);
  const [pageName, setDeletePageName] = useState();

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState(1);
  const [searchModuleIdValue, setModuleId] = useState(0);

  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });

  console.log('STATE----DATA', state);

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));

      console.log('empty');
      console.log('empty', dropDownData);
    }
  };

  var getAppDetails = () => {
    if (state.from === 'app list') {
      axios
        .post(global.config.API_URL + '/getAppById', { appId: state.data._id })
        .then((response) => {
          console.log('response data in app from 1', response);
          setApp(response.data.data[0]);
          // setAppId(state.data._id);
        })
        .catch((error) => console.log(error));
    } else {
      axios
        .post(global.config.API_URL + '/getAppById', { appId: state.data.appId })
        .then((response) => {
          console.log('response data in app from 2', response);
          setApp(response.data.data[0]);
          // setAppId(state.data.appId);
        })
        .catch((error) => console.log(error));
    }
  };

  var getModules = async () => {
    if (state.moduleId === 0) {
      axios
        .post(global.config.API_URL + '/getModules', { appId: state.data._id })
        .then((respondse) => {
          console.log('MODULEdfgdfgdgdf', respondse);
          setModulesw(respondse.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  const getApps = () => {
    if (state.moduleId === 0) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: state.data._id,
          moduleId: 0
        })
        .then((response) => {
          console.log('REPO', response);
          setstate(response.data.data);
          setModules(response.data.data);
        })
        .catch((error) => console.log(error));
    } else if (state.appId === 0) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: 0,
          moduleId: state.data._id
        })
        .then((response) => {
          console.log('REPO', response);
          setstate(response.data.data);
          setModules(response.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  //FILTER code for
  const handleChange = (event) => {
    setSearchTypeNameValue(event.target.value);
    if (state.moduleId === 0) {
      axios
        .post('http://localhost:3000/pageFilters', {
          appId: state.data._id,
          moduleId: searchModuleIdValue,
          searchTypeNameValue: event.target.value,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (state.appId === 0) {
      axios
        .post('http://localhost:3000/pageFilters', {
          moduleId: state.data._id,
          appId: '',
          searchTypeNameValue: event.target.value,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  const handleChangeDropDown = (event) => {
    setSearchTypeSortValue(event);
    if (state.moduleId === 0) {
      console.log('moduleId', searchModuleIdValue);
      axios
        .post('http://localhost:3000/pageFilters', {
          appId: state.data._id,
          moduleId: searchModuleIdValue,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: event
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (state.appId === 0) {
      axios
        .post('http://localhost:3000/pageFilters', {
          moduleId: state.data._id,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: event
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  function onChange(value, dateString) {
    console.log('Selected Time: ', value);
    setSearchTypeDateValue(dateString);
    console.log('searchTypeNameValue Time: ', searchTypeNameValue);
    console.log('searchTypeSortValue Time: ', searchTypeSortValue);
    console.log('Formatted Selected Time: ', dateString);
    if (state.moduleId === 0) {
      axios
        .post('http://localhost:3000/pageFilters', {
          moduleId: state.data._id,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: dateString,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (state.appId === 0) {
      axios
        .post('http://localhost:3000/pageFilters', {
          moduleId: state.data._id,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: dateString,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  }

  const handleChangeStatus = (event) => {
    setModuleId(event);
    if (state.moduleId === 0) {
      if (event === 'all') {
        setModuleId(0);
        axios
          .post('http://localhost:3000/pageFilters', {
            appId: state.data._id,
            moduleId: '',
            searchTypeNameValue: searchTypeNameValue,
            searchTypeDateValue: searchTypeDateValue,
            searchTypeSortValue: searchTypeSortValue
          })
          .then((response) => {
            console.log('response data in module id', response);
            setstate(response.data.data);
            setModules(response.data.data);
          })
          .catch((error) => console.log(error));
      } else {
        axios
          .post('http://localhost:3000/pageFilters', {
            appId: state.data._id,
            moduleId: event,
            searchTypeNameValue: searchTypeNameValue,
            searchTypeDateValue: searchTypeDateValue,
            searchTypeSortValue: searchTypeSortValue
          })
          .then((responw) => {
            console.log('search response', responw);
            setModules(responw.data.data);
            // setApps(res.data.data);
          })
          .catch((error) => console.log('error', error));
      }
    } else if (state.appId === 0) {
      console.log('jnfgndfgfdg', event);
      axios
        .post(global.config.API_URL + '/searchModulePageByName', {
          moduleId: state.data._id,
          searchType: 'status',
          searchString: event
        })
        .then((respons) => {
          console.log('search response', respons);
          setModules(respons.data.data);
          // setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  useEffect(() => {
    getApps();
    getAppDetails();
    getModules();
    // const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    // setCompanyId(data[0].companyId);
    // deleteFunctionCall()
  }, []);

  let cart = [];
  let newData = JSON.stringify(modules);

  const res = (item) => {
    return (
      <div className="app_details_box">
        <div className="app_details_box_img_div">
          <img src={img} />
          {/* <img
            src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
            alt=''
          /> */}
        </div>
        <div className="app_details_box_text_div">
          <div className="app_details_title">{item.pageName}</div>
          <div className="app_details_sub_title">{item.pageName}</div>
        </div>
      </div>
    );
  };

  function deleteModule(pageItem, pageName) {
    console.log('page id', pageItem);
    setPageId(pageItem);
    setDeletePageName(pageName);
    setIsModalVisible(true);
  }

  function updatePage(pageItem, pageName, pageDescription) {
    console.log('page id', pageItem, pageName);
    setPageId(pageItem);
    setCreatePage((prevState) => ({
      ...prevState,
      name: pageName,
      description: pageDescription,
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisiblePage(true);
  }

  const updatePageData = () => {
    console.log(pageId);

    axios
      .put(global.config.API_URL + `/updatePage/${pageId}`, {
        name: createPage.name,
        description: createPage.description
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisiblePage(false);
          getApps();
        }
      })
      .catch((error) => console.log(error));
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deletePage/${pageId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisible(false);
          getApps();
          props.getPageCount();
        }
      })
      .catch((error) => console.log(error));
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCancelPage = () => {
    setIsModalVisiblePage(false);
  };

  const redirectToTheme = (items) => {
    let newArray = [];
    newArray.push(items, { appId: state.data._id }, app, state.from);
    console.log('newArray', newArray);
    navigate('/designpage', { state: newArray });
  };

  const deleteAction = (pageItem, pageName, pageDescription, pageNames, item) => {
    return (
      <div className="app_details_action_div">
        {/* <EyeOutlined onClick={() => redirectToTheme(item)} /> */}
        <i className="icon_view" onClick={() => redirectToTheme(item)}></i>
        {/* <FormOutlined onClick={() => updatePage(pageItem, pageName, pageDescription)} /> */}
        <i
          className="icon_edit"
          onClick={() => updatePage(pageItem, pageName, pageDescription)}></i>
        {/* <DeleteOutlined onClick={() => deleteModule(pageItem, pageNames)} /> */}
        <i className="icon_delete" onClick={() => deleteModule(pageItem, pageNames)}></i>
      </div>
    );
  };

  const lastDate = (updated_at) => {
    return <div className="last_date_date">{moment(updated_at).format('Do dddd, YYYY')}</div>;
  };

  const valuesArray = JSON.parse(newData);
  {
    valuesArray.map(function (item) {
      cart.push({
        key: item._id,
        app_details: res(item),
        module: item.moduleName,
        status: 'Approved',
        comments: item.pageDescription,
        last_date_date: lastDate(item.pageUpdateDate),
        action: deleteAction(item._id, item.pageName, item.pageDescription, item.pageName, item)
      });
      console.log('id', item);
    });
  }

  console.log('PAGELISTING', cart);
  const columns = [
    {
      title: 'Pages',
      dataIndex: 'app_details',
      width: '28%'
    },
    {
      title: 'Module',
      dataIndex: 'module',
      width: '10%'
    },
    // {
    //   title: 'Status',
    //   dataIndex: 'status',
    //   width: '11%',
    //   render: (state) => <div className='inprogress'>{state}</div>
    // },
    {
      title: 'Description',
      dataIndex: 'comments',
      width: '25%'
    },
    {
      title: 'Last Modified Date',
      dataIndex: 'last_date_date',
      render: (last_date_date) => <div className="last_date_date">{last_date_date}</div>
    },
    {
      title: ' ',
      dataIndex: 'action'
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name
    })
  };

  const [selectionType] = useState('checkbox');
  return (
    <>
      {console.log('app', states)}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3
                className="text_align_center"
                text_h3={`You are about to delete a '${pageName}' page`}
              />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version..
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisiblePage}
          onOk={handleOk}
          onCancel={handleCancelPage}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Edit a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger" style={{ marginRight: '214px' }}>
                  {' '}
                  Page name is required
                </p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                value={createPage.description}
                discriptionChangeFunction={discriptionChange}
              />
              {createPage.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Description is required</span>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelPage}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={updatePageData}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
      <div className="ptb-2">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <div className="Search_box">
              <Row gutter={[10, 10]}>
                <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                  <Input placeholder="Search ..." onChange={handleChange} />
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select
                    defaultValue="Sort by Layout"
                    onChange={handleChangeDropDown}
                    style={{ width: '100%' }}>
                    <OptGroup label="Sort by ">
                      <Option value="1">Sort by A to Z</Option>
                      <Option value="-1">Sort by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                {state.moduleId === 0 ? (
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Select Module"
                      onChange={handleChangeStatus}
                      style={{ width: '100%' }}>
                      <OptGroup label="Select Module">
                        <Option value="all">All</Option>
                        {dropDownData.map((item, index) => (
                          <Option key={index} value={item._id}>
                            {item.name}
                          </Option>
                        ))}
                      </OptGroup>
                    </Select>
                  </Col>
                ) : (
                  ''
                )}
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <DatePicker size="large" format="MM/DD/YYYY" onChange={onChange}>
                    <Input
                      placeholder="Last Date"
                      type=""
                      title=""
                      size="12"
                      autocomplete="off"
                      value=""
                    />
                  </DatePicker>
                </Col>
              </Row>
            </div>
          </Col>
          <Col xl={4} lg={4} md={24} sm={24} xs={24}>
            <ViewBoxes
              grid="inactive"
              list="active"
              listlink="/app/pages"
              gridlink="/app_page_grid"
              data={state}
            />
          </Col>
        </Row>
      </div>
      <div>
        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          columns={columns}
          dataSource={cart}
        />
      </div>
    </>
  );
};

export default TableList;

TableList.propTypes = {
  getPageCount: PropTypes.func
};
