import React from 'react';
import ThemeSelectComonent from './Component';
import PropTypes from 'prop-types';

const ThemeSelect = (props) => {
  console.log('theme select props', props);
  return (
    <>
      <ThemeSelectComonent
        getPageCount={props.getPageCount}
        pageList={props.pageList}
        modules={props.modules}
        updatePageData={props.updatePageData}
        deleteFunctionCall={props.deleteFunctionCall}
      />
    </>
  );
};

export default ThemeSelect;

ThemeSelect.propTypes = {
  getPageCount: PropTypes.func,
  pageList: PropTypes.func,
  updatePageData: PropTypes.func,
  deleteFunctionCall: PropTypes.func,
  modules: PropTypes.array
};
