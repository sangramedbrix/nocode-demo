import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import '../../config/config';
import { Modal, Button, Form, Row, Col, Dropdown, Menu, Input, Select, DatePicker } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import ViewBoxes from '../../components/ViewBoxes/Index';
import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import TextHeadingH4 from '../../components/TextHeadings/TextHeadingH4';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import img from '../../assets/images/app_details_img.png';
import '../../components/Popup/popup.css';
import { useNavigate, useParams } from 'react-router-dom';
const { Option, OptGroup } = Select;

import moment from 'moment';
const AppPageGrid = (props) => {
  const { appId, moduleId } = useParams();
  console.log('APPIS', appId);
  console.log('moduleId', moduleId);
  const { state } = useLocation();
  const navigate = useNavigate();
  const [dropDownData, setModulesw] = useState([]);
  const [states, setstate] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisiblePage, setIsModalVisiblePage] = useState(false);
  const [pageId, setPageId] = useState();
  const [modules, setModules] = useState(props.modules);
  const [app, setApp] = useState([]);
  const [pageName, setDeletePageName] = useState();
  const [isModalVisibleAddPage, setIsModalVisibleAddPage] = useState(false);
  const [isModalVisibleDescription, setIsModalVisibleDescription] = useState(false);
  const [moduleDescription, setModuleDescription] = useState();

  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState(1);
  const [searchModuleIdValue, setModuleId] = useState(0);

  console.log('state data', state);

  const showModalAddPage = () => {
    setIsModalVisibleAddPage(true);
  };

  const handleCancelAddPage = () => {
    setIsModalVisibleAddPage(false);
  };
  const handleOkAddPage = () => {
    setIsModalVisibleAddPage(false);
  };

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));

      console.log('empty');
    }
  };

  var getAppDetails = () => {
    axios
      .post(global.config.API_URL + '/getAppById', { appId: appId })
      .then((response) => {
        console.log('response data is', response);
        setApp(response.data.data[0]);
      })
      .catch((error) => console.log(error));
  };

  //FILTER code for
  const handleChange = (event) => {
    setSearchTypeNameValue(event.target.value);
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          appId: appId,
          moduleId: searchModuleIdValue,
          searchTypeNameValue: event.target.value,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: moduleId,
          appId: '',
          searchTypeNameValue: event.target.value,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  const handleChangeDropDown = (event) => {
    setSearchTypeSortValue(event);
    if (appId != '' && !moduleId) {
      console.log('moduleId', searchModuleIdValue);
      axios
        .post(global.config.API_URL + '/pageFilters', {
          appId: appId,
          moduleId: searchModuleIdValue,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: event
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: moduleId,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: event
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  function onChange(value, dateString) {
    console.log('Selected Time: ', value);
    setSearchTypeDateValue(dateString);
    console.log('searchTypeNameValue Time: ', searchTypeNameValue);
    console.log('searchTypeSortValue Time: ', searchTypeSortValue);
    console.log('Formatted Selected Time: ', dateString);
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: '',
          appId: appId,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: dateString,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: moduleId,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: dateString,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  }

  const handleChangeStatus = (event) => {
    setModuleId(event);
    if (appId != '' && !moduleId) {
      if (event === 'all') {
        setModuleId(0);
        axios
          .post(global.config.API_URL + '/pageFilters', {
            appId: appId,
            moduleId: '',
            searchTypeNameValue: searchTypeNameValue,
            searchTypeDateValue: searchTypeDateValue,
            searchTypeSortValue: searchTypeSortValue
          })
          .then((response) => {
            console.log('response data in module id', response);
            setstate(response.data.data);
            setModules(response.data.data);
          })
          .catch((error) => console.log(error));
      } else {
        axios
          .post(global.config.API_URL + '/pageFilters', {
            appId: appId,
            moduleId: event,
            searchTypeNameValue: searchTypeNameValue,
            searchTypeDateValue: searchTypeDateValue,
            searchTypeSortValue: searchTypeSortValue
          })
          .then((responw) => {
            console.log('search response', responw);
            setModules(responw.data.data);
            // setApps(res.data.data);
          })
          .catch((error) => console.log('error', error));
      }
    } else if (appId != '' && moduleId != '') {
      console.log('jnfgndfgfdg', event);
      axios
        .post(global.config.API_URL + '/searchModulePageByName', {
          moduleId: moduleId,
          searchType: 'status',
          searchString: event
        })
        .then((respons) => {
          console.log('search response', respons);
          setModules(respons.data.data);
          // setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  var getModules = async () => {
    if (appId != '') {
      axios
        .post(global.config.API_URL + '/getModules', { appId: appId })
        .then((respondse) => {
          console.log('MODULEdfgdfgdgdf', respondse);
          setModulesw(respondse.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  const getApps = () => {
    console.log('page List');
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: appId,
          moduleId: 0
        })
        .then((response) => {
          console.log('response data in module id', response);
          setModules(response.data.data);
          // setstate(response.data.data);
        })
        .catch((error) => console.log(error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: 0,
          moduleId: moduleId
        })
        .then((response) => {
          console.log('response data', response);
          setModules(response.data.data);
          // setstate(response.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  useEffect(() => {
    getApps();
    getAppDetails();
    getModules();
    // const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    // setCompanyId(data[0].companyId);
    // deleteFunctionCall()
  }, []);

  console.log('PAGES DATA', modules);

  let cart = [];
  let newData = JSON.stringify(modules);

  const res = (item) => {
    return (
      <div className="app_details_box">
        <div className="app_details_box_img_div">
          <img src={img} />
          {/* <img
            src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
            alt=""
          /> */}
        </div>
        <div className="app_details_box_text_div">
          <div className="app_details_title">{item.moduleName}</div>
          <div className="app_details_sub_title">{item.moduleName}</div>
        </div>
      </div>
    );
  };

  function deleteModule(pageItem, pageName) {
    console.log('page id', pageItem);
    setPageId(pageItem);
    setDeletePageName(pageName);
    setIsModalVisible(true);
  }

  function updatePage(pageItem, pageName, pageDescription) {
    console.log('page id', pageItem, pageName);
    setPageId(pageItem);
    setCreatePage((prevState) => ({
      ...prevState,
      name: pageName,
      description: pageDescription,
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisiblePage(true);
  }

  const updatePageData = () => {
    axios
      .put(global.config.API_URL + `/updatePage/${pageId}`, {
        name: createPage.name,
        description: createPage.description
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisiblePage(false);
          getApps();
        }
      })
      .catch((error) => console.log(error));
  };

  const deleteFunctionCall = () => {
    console.log('page id', pageId);
    axios
      .delete(global.config.API_URL + `/deletePage/${pageId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisible(false);
          getApps();
          props.getPageCount();
        }
      })
      .catch((error) => console.log(error));
  };

  const createPageModule = () => {
    if (createPage.name != '') {
      axios
        .post(global.config.API_URL + '/createPage', {
          moduleId: state.data._id,
          pageName: createPage.name,
          description: createPage.description
        })
        .then((response) => {
          console.log(response);

          if (response.data.message === 'Page created!') {
            setCreatePage((prevState) => ({ ...prevState, name: '', description: '' }));
            setIsModalVisibleAddPage(false);
            getApps();
            props.pageList();
            props.getPageCount();
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      if (createPage.name == '') {
        setCreatePage((prevState) => ({ ...prevState, nameValidation: true }));
      }
    }
  };

  const handleOk = () => {
    setIsModalVisible(false);
    setIsModalVisibleDescription(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setIsModalVisibleDescription(false);
  };

  const handleCancelPage = () => {
    setCreatePage((prevState) => ({
      ...prevState,
      name: '',
      description: '',
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisiblePage(false);
  };

  const redirectToThemes = (items) => {
    navigate(`/app/${appId}/module/${items.moduleId}/page/${items._id}`);
  };

  const deleteAction = (pageItem, pageName, pageDescription, pageNames, item) => {
    return (
      <div className="app_details_action_div">
        {/* <EyeOutlined onClick={() => redirectToThemes(item)} /> */}
        <i className="icon_view" onClick={() => redirectToThemes(item)}></i>
        {/* <DeleteOutlined onClick={() => deleteModule(pageItem, pageNames)} /> */}
        <i className="icon_delete" onClick={() => deleteModule(pageItem, pageNames)}></i>
        {/* <FormOutlined onClick={() => updatePage(pageItem, pageName, pageDescription)} /> */}
        <i
          className="icon_edit"
          onClick={() => updatePage(pageItem, pageName, pageDescription)}></i>
      </div>
    );
  };

  const lastDate = (updated_at) => {
    return <div className="last_date_date">{moment(updated_at).format('Do dddd, YYYY')}</div>;
  };

  const valuesArray = JSON.parse(newData);
  {
    valuesArray.map(function (item) {
      cart.push({
        id: item._id,
        app_details: res(item),
        module: item.pageName,
        status: 'Approved',
        comments: item.moduleDescription,
        last_date_date: lastDate(item.pageUpdateDate),
        action: deleteAction(
          item._id,
          item.moduleName,
          item.moduleDescription,
          item.moduleName,
          item
        )
      });
      console.log('id', item);
    });
  }

  // const redirectToTheme = () => {
  //   navigate('/page_template');
  // };

  const redirectToThemess = (items) => {
    navigate(`/app/${appId}/module/${items.moduleId}/page/${items._id}`);
  };

  console.log('PAGELISTING', cart);

  return (
    <>
      {console.log('app', states)}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3
                className="text_align_center"
                text_h3={`You are about to delete a '${pageName}' page`}
              />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version..
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      <Modal
        title=""
        footer={null}
        onOk={handleOk}
        onCancel={handleCancel}
        visible={isModalVisibleDescription}>
        {moduleDescription}
      </Modal>

      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisibleAddPage}
          onOk={handleOkAddPage}
          onCancel={handleCancelAddPage}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger" style={{ marginRight: '214px' }}>
                  {' '}
                  Page name is required
                </p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={createPage.description}
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelAddPage}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={createPageModule}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisiblePage}
          onOk={handleOk}
          onCancel={handleCancelPage}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Edit a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger"> Page name is required</p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                value={createPage.description}
                discriptionChangeFunction={discriptionChange}
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelPage}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={updatePageData}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      <div className="ptb-2">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <div className="Search_box">
              <Row gutter={[10, 10]}>
                <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                  <Input placeholder="Search ..." onChange={handleChange} />
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select
                    defaultValue="Sort by Layout"
                    onChange={handleChangeDropDown}
                    style={{ width: '100%' }}>
                    <OptGroup label="Sort by ">
                      <Option value="1">Sort by A to Z</Option>
                      <Option value="-1">Sort by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                {appId != '' && !moduleId ? (
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Select Module"
                      onChange={handleChangeStatus}
                      style={{ width: '100%' }}>
                      <OptGroup label="Select Module">
                        <Option value="all">All</Option>
                        {dropDownData.map((item, index) => (
                          <Option key={index} value={item._id}>
                            {item.name}
                          </Option>
                        ))}
                      </OptGroup>
                    </Select>
                  </Col>
                ) : (
                  ''
                )}
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <div className="datepicker">
                    <DatePicker
                      placeholder="Last Date"
                      format="MM/DD/YYYY"
                      // style={{boxShadow: '0px 2px 10px rgb(0 0 0 / 10%)'}}
                      onChange={onChange}></DatePicker>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>

          <Col xl={2} lg={4} md={24} sm={24} xs={24}>
            {appId === '' ? (
              <div>
                <Button className="ext-btn" onClick={showModalAddPage}>
                  Add Page
                </Button>
              </div>
            ) : (
              ''
            )}
          </Col>

          <Col xl={2} lg={4} md={24} sm={24} xs={24}>
            {appId != '' && !moduleId ? (
              <ViewBoxes
                grid="active"
                list="inactive"
                gridlink={`/app/${appId}/pages/grid`}
                listlink={`/app/${appId}/pages`}
                //data={state}
              />
            ) : (
              <ViewBoxes
                grid="active"
                list="inactive"
                gridlink={`/app/${appId}/module/${moduleId}/grid`}
                listlink={`/app/${appId}/module/${moduleId}`}
                //data={state}
              />
            )}
          </Col>
        </Row>
      </div>

      <div>
        <Row gutter={[16, 25]}>
          {(() => {
            console.log('page grid', modules);
            return modules.map((createdModule) => {
              return (
                <>
                  <Col xl={6} lg={6} md={8} sm={12} xs={24}>
                    <div className="AssetManagementAppCard">
                      <div className="AssetManagementAppCardDiv">
                        <div className="card-body">
                          <Row>
                            <Col xs={22}>
                              <h3 className="title">{createdModule.pageName} </h3>
                              <span>{createdModule.moduleName}</span>
                            </Col>
                            <Col xs={2} className="option-dots">
                              <Dropdown
                                placement="bottomRight"
                                overlay={(() => {
                                  return (
                                    <Menu className="list_menu">
                                      <Menu.Item>
                                        <span
                                          target="_blank"
                                          onClick={() => redirectToThemess(createdModule)}>
                                          {' '}
                                          Page Design{' '}
                                        </span>
                                      </Menu.Item>
                                      <Menu.Item>
                                        <span
                                          target="_blank"
                                          onClick={() =>
                                            updatePage(
                                              createdModule._id,
                                              createdModule.pageName,
                                              createdModule.pageDescription
                                            )
                                          }>
                                          {' '}
                                          Edit{' '}
                                        </span>
                                      </Menu.Item>
                                      <Menu.Item>
                                        <a
                                          target="_blank"
                                          rel="noopener noreferrer"
                                          onClick={() =>
                                            deleteModule(createdModule._id, createdModule.pageName)
                                          }>
                                          {' '}
                                          Delete{' '}
                                        </a>
                                      </Menu.Item>
                                    </Menu>
                                  );
                                })()}>
                                <a
                                  className="ant-dropdown-link"
                                  onClick={(e) => e.preventDefault()}>
                                  <i className="icon_more"></i>
                                </a>
                              </Dropdown>
                            </Col>
                          </Row>

                          <p className="sub_title">
                            {moment(createdModule.pageUpdateDate).format('Do dddd, YYYY')}{' '}
                          </p>
                          <div style={{ padding: '6% 0' }}>
                            <div className="small_txt" maxLength="20">
                              {(() => {
                                if (createdModule.pageDescription != '') {
                                  if (createdModule.pageDescription.length > 60) {
                                    return (
                                      <p>
                                        {createdModule.pageDescription.substring(0, 60)}
                                        {/* <Tooltip title="Read More"> */}
                                        <a
                                          onClick={() => {
                                            setIsModalVisibleDescription(true);
                                            setModuleDescription(createdModule.pageDescription);
                                          }}>
                                          <span
                                            style={{
                                              marginLeft: 5,
                                              fontSize: 12
                                            }}>
                                            Read more...
                                          </span>
                                        </a>
                                        {/* </Tooltip> */}
                                      </p>
                                    );
                                  } else {
                                    return (
                                      <>
                                        <p>{createdModule.pageDescription}</p>
                                      </>
                                    );
                                  }
                                }
                              })()}
                              {/* {createdModule.description.substring(0, 100)}<a onClick={() => {
                                                  setIsModalVisibleDescription(true)
                                                  setModuleDescription(createdModule.description)
                                                }} style={{marginTop: 26,marginRight: 3}}>...</a>
                                               */}
                            </div>
                            {/* <div className="large-txt">Start now creating pages.</div> */}
                          </div>
                          <div className="btn-create">
                            <Button
                              type="primary"
                              size="medium"
                              onClick={() => {
                                let newArray = [];
                                let items = {
                                  moduleId: createdModule.moduleId,
                                  pageName: createdModule.pageName
                                };
                                newArray.push(items, { appId: app._id }, app, state.from);

                                navigate('/page_template', { state: newArray });
                              }}>
                              Select Page Theme
                            </Button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </>
              );
            });
          })()}
        </Row>
      </div>
    </>
  );
};

export default AppPageGrid;

AppPageGrid.propTypes = {
  getPageCount: PropTypes.func,
  updatePageData: PropTypes.func,
  deleteFunctionCall: PropTypes.func,
  pageList: PropTypes.func,
  modules: PropTypes.array
};
