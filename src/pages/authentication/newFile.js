// import '../config';
import axios from 'axios';

export const newFile = {
  isValidAppModule,
  isValidObjectId
};

function isValidObjectId(id) {
  if (id.match(/^[0-9a-fA-F]{24}$/)) {
    return true;
  } else {
    return false;
  }
}

function isValidAppModule(appId) {
  axios
    .post('http://localhost:3000/checkValidAppModule', {
      appId: appId
    })
    .then((response) => {
      if (response.length > 0) {
        return true;
      } else {
        return false;
      }
    })
    .catch((error) => console.log('error', error));
}
