import React from 'react';
import { Row, Col } from 'antd';
import 'antd/dist/antd.css';
import FormEmpty from '../../../components/Forms/FormEmpty';
import SideBar from '../../../components/Sidebar';
import ElementSetting from '../../../components/Forms/ElementSetting';
import './style.css';

const components = () => {
  return (
    <>
      <div className="elemenent_stting_editor">
        <Row gutter={[20, 20]}>
          <Col xl={4}>
            <SideBar />
          </Col>
          <Col xl={14}>
            <FormEmpty />
          </Col>
          <Col xl={6}>
            <ElementSetting />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default components;
