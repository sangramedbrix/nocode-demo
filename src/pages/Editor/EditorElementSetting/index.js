import React from 'react';
import Headers from '../../../components/Header';
import SubheaderComponent from '../../Canvas/SubheaderComponent';
import { Layout } from 'antd';
import Appcomponent from './components';
import './style.css';

const { Content } = Layout;

const EditorElementsSettings = () => {
  return (
    <>
      <div className="elemenent_stting_editor_main">
        <Layout>
          <Headers />
          <SubheaderComponent />
          <Layout>
            <Content>
              <Appcomponent />
            </Content>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default EditorElementsSettings;
