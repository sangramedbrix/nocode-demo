import React from 'react';
import Headers from '../../../components/Header';
// import SubheaderComponent from '../../Canvas/SubheaderComponent';
import Appcomponent from './components';
import { Layout } from 'antd';
import { useParams } from 'react-router-dom';
import './style.css';
// import axios from 'axios';

const { Content } = Layout;

const EditorFormPage = () => {
  const { pageId } = useParams();
  // console.log("page id in editor",pageId)

  return (
    <>
      <div className="editor_form_page_main">
        <Layout>
          <Headers />
          {/* <SubheaderComponent cancelUrl="/app/pages" /> */}
          <Layout>
            <Content>
              <Appcomponent pageId={pageId} />
            </Content>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default EditorFormPage;
