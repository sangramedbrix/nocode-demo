import React from 'react';
import Headers from '../../../components/Header';
import SubheaderComponent from '../../Canvas/SubheaderComponent';
import { Layout } from 'antd';
import Appcomponent from './components';

const { Content } = Layout;

const EditorFormBlank = () => {
  return (
    <>
      <div className="form_empty_editor_main">
        <Layout>
          <Headers />
          <SubheaderComponent />
          <Layout>
            <Content>
              <Appcomponent />
            </Content>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default EditorFormBlank;
