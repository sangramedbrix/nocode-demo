import React from 'react';
import { Row, Col } from 'antd';
import 'antd/dist/antd.css';
import FormPageComponent from '../../../components/Forms/FormPageComponent';
import FormEmpty from '../../../components/Forms/FormEmpty';
import SideBar from '../../../components/Sidebar';

const Appcomponent = () => {
  return (
    <>
      <div className="form_empty_editor">
        <Row gutter={[20, 10]}>
          <Col xl={4}>
            <SideBar />
          </Col>
          <Col xl={1}></Col>
          <Col xl={13}>
            <FormEmpty />
          </Col>
          <Col xl={6}>
            <FormPageComponent />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Appcomponent;
