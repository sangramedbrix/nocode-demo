import React from 'react';
// import '../../../components/theme.css';
import { Divider, Input, Switch, Button } from 'antd';
import Template from '../../../assets/images/template.png';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

const Genral = () => {
  return (
    <>
      <div className="genral">
        <div className="title">General</div>
        <div className="section1">
          <div className="section1-title">Page Title</div>
          <Input placeholder="Type Here" style={{ border: 'none', fontSize: 14 }} />
        </div>
        <Divider className="mtb-1" />
        <div className="section2">
          <div className="section2-title">Page Description</div>
          <Input
            placeholder="Type Here"
            style={{ border: 'none', fontSize: 14, color: '#333333' }}
          />
        </div>
        <Divider className="mtb-1" />
        <div className="section6">
          <div>
            <span className="section3-title">Active App Theme</span>
            <p className="section3-subtitle"> Hexaqua Marina</p>
          </div>
          <div>
            <Button className="btn-change mtb-2">Change</Button>
          </div>
        </div>
        <div className="progress" style={{ width: '98%' }}></div>
        <Divider className="mtb-1" />
        <div>
          <div className="section4-title">App Template</div>
          <div className="section6">
            <div>
              <img src={Template} alt="template" width="45px" className="temp-img" />
            </div>
            <div>
              <div className="section4-subtitle">
                <span> Basic Information Form </span>
              </div>
            </div>
            <div>
              <Button className="btn-change">Change</Button>
            </div>
          </div>
          <Divider className="mtb-1" />
          <div>
            <h3 className="section6-title">Permissions</h3>
            <div className="section5">
              <div className="section6-active-title">Active Status</div>
              <div>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </div>
            </div>
            <Divider className="mtb-2" />
            <div className="section6">
              <div className="section6-active-title">Validation</div>
              <div>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </div>
            </div>
            <Divider className="mtb-2" />
            <div className="section6">
              <div>
                <span className="section6-active-title">Edit Access</span>
                <p className="section6-edit">
                  If Yes, Admin and Data Analyst can edit and change the design
                </p>
              </div>
              <div>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </div>
            </div>

            <Divider className="mtb-2" />
            <div className="section5">
              <div className="section6-active-title">Data Connector</div>
              <div>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Genral;
