import React from 'react';
import { Row, Col } from 'antd';
import Genral from './Genral';
import BaseColors from './BaseColors';
import BaseTypography from './BaseTypography';

const Appcomponent = () => {
  return (
    <>
      <div className="edit_setting bg-light-blue box-padding">
        <h3>Page Settings</h3>
        <Row gutter={[30, 30]}>
          <Col xl={8}>
            <Genral />
          </Col>
          <Col xl={8}>
            <BaseColors />
          </Col>
          <Col xl={8}>
            <BaseTypography />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Appcomponent;
