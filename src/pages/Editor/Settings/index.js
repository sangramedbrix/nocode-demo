import React from 'react';
import Headers from '../../../components/Header';
import SubheaderComponent from '../../Canvas/SubheaderComponent';
import Appcomponent from './components';
import '../../../assets/styles/index.css';

const SettingEdit = () => {
  return (
    <>
      <div className="edit_setting_main">
        <Headers />
        <SubheaderComponent />
        <Appcomponent />
      </div>
    </>
  );
};

export default SettingEdit;
