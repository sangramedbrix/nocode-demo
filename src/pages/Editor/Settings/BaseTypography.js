import { React, useState } from 'react';
import { Slider, InputNumber, Row, Col, Divider } from 'antd';

const BaseTypography = () => {
  const [inputValue, setInputValue] = useState(1);
  const onChange = (value) => {
    setInputValue(value);
  };

  return (
    <>
      <div className="base_typography">
        <div className="title">Base Typography</div>
        <Row>
          <div className="sub_title">Base Font-Size</div>
          <Col xl={20}>
            <Slider
              min={1}
              max={20}
              onChange={onChange}
              value={typeof inputValue === 'number' ? inputValue : 0}
            />
          </Col>
          <Col xl={4}>
            <InputNumber min={1} max={20} value={inputValue + ' px'} onChange={onChange} />
          </Col>
        </Row>
        <Divider className="mtb-1" />
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">Body & Content Font-family</p>
              <p className="title">Poppins</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">Body & Content Font-Size</p>
              <p className="title">16px Regular</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">Headings Font-family</p>
              <p className="title">Poppins</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">Title</p>
              <p className="title">44px Bold</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">H1</p>
              <p className="title">36px Bold</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">H2</p>
              <p className="title">24px Semibold</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">H3</p>
              <p className="title">18px Semibold</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">Medium Text</p>
              <p className="title">14px Regular</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
        <div className="style_div">
          <div className="style_box">
            <div>
              <p className="sub_title">Tiny Text</p>
              <p className="title">12px Regular</p>
            </div>
            <div className="center form-icn">
              <i className="icon_edit_purple"></i>
            </div>
          </div>
          <Divider className="mtb-1" />
        </div>
      </div>
    </>
  );
};

export default BaseTypography;
