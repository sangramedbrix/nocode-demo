import React from 'react';
import { Row, Col, Divider, Button } from 'antd';
import Primary from '../../../assets/images/bace_color/Primary.png';
import Secondary from '../../../assets/images/bace_color/Secondary.png';
import button from '../../../assets/images/bace_color/Button.png';
import Active from '../../../assets/images/bace_color/Active.png';
import Hover from '../../../assets/images/bace_color/Hover.png';

const BaseColors = () => {
  return (
    <>
      <div className="base_color">
        <div className="base_color_section">
          <div className="title">Base Colors</div>
          <Row>
            <Col xl={5}>
              {' '}
              <span className="sub_title"> Primary </span> <img src={Primary} />
            </Col>
            <Col xl={5}>
              {' '}
              <span className="sub_title">Secondary </span> <img src={Secondary} />
            </Col>
            <Col xl={5}>
              {' '}
              <span className="sub_title"> Button </span> <img src={button} />
            </Col>
            <Col xl={5}>
              {' '}
              <span className="sub_title"> Active </span> <img src={Active} />
            </Col>
            <Col xl={4}>
              {' '}
              <span className="sub_title"> Hover </span> <img src={Hover} />
            </Col>
          </Row>
          <Divider className="mtb-1" />
        </div>
        <div className="button_section">
          <div className="title">Buttons</div>
          <div className="Spacing_div">
            <div className="button_section_subtitle">Primary Button</div>
            <div>
              <Button className="btn-change">Change</Button>
            </div>
          </div>
          <Divider className="mtb-1" />
          <div className="Spacing_div">
            <div className="button_section_subtitle">Secondary Button</div>
            <div>
              <Button className="btn-change">Change</Button>
            </div>
          </div>
          <Divider className="mtb-1" />
          <div className="Spacing_div">
            <div className="button_section_subtitle">Text Button</div>
            <div>
              <Button className="btn-change">Change</Button>
            </div>
          </div>
        </div>
        <Divider className="mtb-1" />
        <div className="spacing_section">
          <div className="title">Spacing</div>
          <div className="Spacing_div">
            <div className="spacing_section_align_left">Margin</div>
            <div className="spacing_section_align_right">Add Outside Spacing</div>
          </div>
          <Divider className="mtb-1" />
          <div className="Spacing_div">
            <div className="spacing_section_align_left">Padding</div>
            <div className="spacing_section_align_right">Add Inside Spacing</div>
          </div>
          <Divider className="mtb-1" />
          <div className="Spacing_div">
            <div className="spacing_section_align_left">Text Overflow</div>
            <div className="spacing_section_align_right">Change Overflow Behaviour</div>
          </div>
          <Divider className="mtb-1" />
          <div className="Spacing_div">
            <div className="spacing_section_align_left">Background</div>
            <div className="spacing_section_align_right">Add Inside Spacing</div>
          </div>
          <Divider className="mtb-1" />
        </div>
      </div>
    </>
  );
};

export default BaseColors;
