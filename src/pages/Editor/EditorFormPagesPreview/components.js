/**
 * <ReactFormBuilder />
 */

import React from 'react';
import { Row, Col, Select } from 'antd';
import 'antd/dist/antd.css';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import Preview from '../../../pages/CustomiseFormIo/preview';
// import Toolbar from '../../../pages/CustomiseFormIo/toolbar';
import ReactFormGenerator from '../../../pages/CustomiseFormIo/form';
import store from '../../../pages/CustomiseFormIo/stores/store';
import Registry from '../../../pages/CustomiseFormIo/stores/registry';
// import SideBar from '../../../components/Sidebar';
// import FormPageComponent from '../../../components/Forms/FormPageComponent';
import PropTypes from 'prop-types';
import SubheaderComponent from '../../Canvas/SubheaderComponent';

// import { useLocation, withRouter  } from 'react-router-dom';
// import FormBlank from '../../../components/Forms/FormBlank';
// import { FormBuilder } from 'react-formio';
// import Layouts from '../../../components/Drawer/Layouts';
// import {
//   CheckCircleOutlined,
//   CaretDownOutlined,
//   EyeOutlined,
//   MoreOutlined
// } from '@ant-design/icons';
import {
  PlusOutlined,
  MinusOutlined,
  DesktopOutlined,
  TabletOutlined,
  MobileOutlined,
  RedoOutlined,
  UndoOutlined
} from '@ant-design/icons';
import axios from 'axios';
import alertify from 'alertifyjs';

// const { SubMenu } = Menu;
// const { Sider } = Layout;
const { Option } = Select;

// const savemenus = (
//   <Menu>
//     <Menu.Item key="0">
//       <a href="">Save as Draft</a>
//     </Menu.Item>
//     <Menu.Item key="1">
//       <a href="">Save as Local Copy</a>
//     </Menu.Item>
//     <Menu.Item key="3">Save as Version</Menu.Item>
//   </Menu>
// );

// const menu = (
//   <Menu>
//     <Menu.Item key="0">
//       <a href="">Edit Page Settings</a>
//     </Menu.Item>
//     <Menu.Item key="1">
//       <a href="">Show Version History</a>
//     </Menu.Item>
//     <Menu.Item key="3">Export to PDF</Menu.Item>
//   </Menu>
// );

class Appcomponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      editElement: null,
      visible: false,
      style: 'wrapper-container',
      activeScreen: '',
      tabViewScreen: '',
      mobileViewScreen: '',
      data: [],
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false
    };
    this.editModeOn = this.editModeOn.bind(this);

    const update = this._onChange.bind(this);
    // this._onSubmit = this._onSubmit.bind(this);

    store.subscribe((state) => update(state.data));
  }

  componentDidMount() {
    // const { appId, moduleId, pageId } = useParams();
    // const { appId } = this.props.params;
    console.log('params', this.props.params);
  }

  showPreview() {
    console.log(JSON.stringify(this.state.data));
    this.setState({
      previewVisible: true
    });
  }

  showModal = () => {};

  // save page function
  savePage = (params) => {
    console.log('data');
    console.log(this.state.data, params);

    if (this.state.data.length > 0) {
      axios
        .post('http://localhost:3000/savePage', {
          pageId: params.pageId,
          pageJson: JSON.stringify(this.state.data)
        })
        .then((response) => {
          console.log(response);
          alertify.set('notifier', 'position', 'top-right');
          alertify.success('Your page is successfully saved');
        })
        .catch((e) => console.log(e));
    } else {
      alertify.set('notifier', 'position', 'top-right');
      alertify.error('Sorry! Create page design.');
    }
  };

  showShortPreview() {
    this.setState({
      shortPreviewVisible: true
    });
  }

  showRoPreview() {
    this.setState({
      roPreviewVisible: true
    });
  }

  closePreview() {
    this.setState({
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false
    });
  }

  _onChange(data) {
    this.setState({
      data
    });
  }

  // const [style, setStyle] = useState('wrapper-container');
  // const [activeScreen, setActiveScreen] = useState('screen');
  // const [tabViewScreen, setTabViewScreen] = useState('');
  // const [mobileViewScreen, setMobileViewScreen] = useState('');

  editModeOn(data, e) {
    e.preventDefault();
    e.stopPropagation();
    if (this.state.editMode) {
      this.setState({ editMode: !this.state.editMode, editElement: null });
    } else {
      this.setState({ editMode: !this.state.editMode, editElement: data });
    }
  }

  desktopView = () => {
    // console.log('you just clicked');
    this.setState({
      style: 'desktop-container-wrapper',
      activeScreen: 'activescreen',
      tabViewScreen: '',
      mobileViewScreen: ''
    });
  };
  tabView = () => {
    this.setState({
      style: 'tab-container-wrapper',
      activeScreen: '',
      tabViewScreen: 'activescreen',
      mobileViewScreen: ''
    });
  };
  mobileView = () => {
    this.setState({
      style: 'mobile-container-wrapper',
      activeScreen: '',
      tabViewScreen: '',
      mobileViewScreen: 'activescreen'
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
    // setVisible(false);
  };
  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  manualEditModeOff() {
    if (this.state.editMode) {
      this.setState({
        editMode: false,
        editElement: null
      });
    }
  }

  getPageJson = () => {};

  // eslint-disable-next-line no-unused-vars
  _onSubmit(data) {
    console.log('onSubmit', data);
    // Place code to post json data to server here
  }

  render() {
    console.log('props data', this.props);
    const toolbarProps = {
      showDescription: this.props.show_description
    };
    if (this.props.toolbarItems) {
      toolbarProps.items = this.props.toolbarItems;
    }

    // let modalClass = 'modal';
    // if (this.state.previewVisible) {
    //   modalClass += ' show d-block';
    // }

    // var shortModalClass = 'modal short-modal';
    // if (this.state.shortPreviewVisible) {
    //   shortModalClass += ' show d-block';
    // }

    // var roModalClass = 'modal ro-modal';
    // if (this.state.roPreviewVisible) {
    //   roModalClass += ' show d-block';
    // }

    return (
      <>
        <SubheaderComponent cancelUrl="/app/pages" savePage={this.savePage} />
        {/* <div className="subheader_components">
          <Row className="subheader_components_box" gutter={[0, 10]} align="middle">
            <Col xl={12} lg={24} md={24} sm={24} xs={24}>
              <Row className="first_row" align="middle" gutter={[10, 8]}>
                <Col xl={2} lg={2} md={4} sm={2} xs={5}>
                
                  <div className="cancel-box text-center">
                   
                    <Button
                      type="link"
                      onClick={() => {
                        this.showModal;
                      }}>
                      <i className="icon_back_arrow text-center" />{' '}
                      <span className="mt-1">Cancel</span>
                    </Button>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col xl={12} lg={24} md={24} sm={24} xs={24}>
              <Row className="seconed_row" align="middle" gutter={[10, 8]}>
                <Col xl={4} lg={4} md={4} sm={4} xs={8}>
                  <div className="preview-btn" onClick={this.savePage}>
                    <Dropdown overlay={savemenus} trigger={['click']}>
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <Button style={{ width: '100%' }} icon={<CheckCircleOutlined />}>
                          {' '}
                          Save {<CaretDownOutlined />}
                        </Button>
                      </a>
                    </Dropdown>
                  </div>
                </Col>
                <Col className="preview-btn" xl={4} lg={4} md={4} sm={4} xs={8}>
                  <Button
                    onClick={() => this.showPreview()}
                    style={{ width: '100%' }}
                    icon={<EyeOutlined />}>
                    Preview
                  </Button>
                </Col>
                <Col className="publish-btn" xl={4} lg={4} md={4} sm={4} xs={8}>
                  <Button
                    style={{ width: '100%' }}
                    type="primary"
                    icon={<i className="icon_rocket" />}>
                    {' '}
                    Publish{' '}
                  </Button>
                </Col>
                <Col className="more-btn" xl={2} lg={2} md={2} sm={4} xs={3}>
                  <Dropdown overlay={menu} trigger={['click']}>
                    <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                      <Button type="" icon={<MoreOutlined />}>
                        {' '}
                      </Button>
                    </a>
                  </Dropdown>
                </Col>
              </Row>
            </Col>
          </Row>
        </div> */}
        {console.log('state data', JSON.stringify(this.state.data))}
        <ReactFormGenerator
          download_path=""
          back_action="/"
          back_name="Back"
          // answer_data={answers}
          action_name="Save"
          form_action="/api/form"
          form_method="POST"
          // skip_validations={true}
          onSubmit={this._onSubmit}
          variables={this.props.variables}
          data={this.state.data}
        />
        {/* preview design page popup */}
        {/* {this.state.previewVisible && (

          <div className={modalClass} role="dialog">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content" style={{ padding: 20 }}>
                <ReactFormGenerator
                  download_path=""
                  back_action="/"
                  back_name="Back"
                  // answer_data={answers}
                  action_name="Save"
                  form_action="/api/form"
                  form_method="POST"
                  // skip_validations={true}
                  onSubmit={this._onSubmit}
                  variables={this.props.variables}
                  data={this.state.data}
                />

                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-default"
                    data-dismiss="modal"
                    onClick={this.closePreview.bind(this)}>
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        )} */}

        {/* end preview design popup */}

        <DndProvider backend={HTML5Backend}>
          <div className="form_page_editor">
            <Row gutter={[20, 20]}>
              <Col xl={24}>
                {/* <FormBlank /> */}
                <div className="empty_box">
                  <div className="search_bar">
                    <div className="do_undo_div">
                      <div className="icon_undo">
                        <UndoOutlined />
                      </div>
                      <div className="icon_redo">
                        <RedoOutlined />
                      </div>
                    </div>

                    <div className="select_div">
                      <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="Search to Select"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        filterSort={(optionA, optionB) =>
                          optionA.children
                            .toLowerCase()
                            .localeCompare(optionB.children.toLowerCase())
                        }>
                        <Option value="1">Personal Information</Option>
                        <Option value="2">Personal Information</Option>
                        <Option value="3">Personal Information</Option>
                      </Select>
                    </div>

                    <div className="zoom-icons">
                      <div className="icon_zoom_in">
                        {' '}
                        <PlusOutlined />{' '}
                      </div>
                      <span>100%</span>
                      <div className="icon_zoom_out">
                        {' '}
                        <MinusOutlined />{' '}
                      </div>
                    </div>

                    <div className="screens">
                      <div className={`icon_desktop ${this.state.activeScreen}`}>
                        <DesktopOutlined onClick={this.desktopView} />
                      </div>
                      <div className={`icon_tab ${this.state.tabViewScreen}`}>
                        <TabletOutlined onClick={this.tabView} />
                      </div>
                      <div className={`icon_mob ${this.state.mobileViewScreen}`}>
                        <MobileOutlined onClick={this.mobileView} />
                      </div>
                    </div>
                  </div>
                  {/* <div className="empty_box_div"> */}
                  <div className="temp">
                    <div className={`react-form-builder ${this.state.style}`}>
                      {/* <div className="desktop-container-wrapper"></div> */}
                      <Preview
                        files={this.props.files}
                        manualEditModeOff={this.manualEditModeOff.bind(this)}
                        showCorrectColumn={this.props.showCorrectColumn}
                        parent={this}
                        data={this.props.data}
                        url={this.props.url}
                        saveUrl={this.props.saveUrl}
                        onLoad={this.props.onLoad}
                        onPost={this.props.onPost}
                        editModeOn={this.editModeOn}
                        editMode={this.state.editMode}
                        variables={this.props.variables}
                        registry={Registry}
                        editElement={this.state.editElement}
                        renderEditForm={this.props.renderEditForm}
                      />
                    </div>
                  </div>
                  {/* </div> */}
                </div>
              </Col>
              {/* <Col xl={18}>
            <FormBuilder />
          </Col> */}
              {/* <Col xl={6}>
                <FormPageComponent />
              </Col> */}
            </Row>
          </div>
        </DndProvider>

        {/* <DndProvider backend={HTML5Backend}>
       <div>
          <div className="react-form-builder clearfix">
            <div>
              <Preview
                files={this.props.files}
                manualEditModeOff={this.manualEditModeOff.bind(this)}
                showCorrectColumn={this.props.showCorrectColumn}
                parent={this}
                data={this.props.data}
                url={this.props.url}
                saveUrl={this.props.saveUrl}
                onLoad={this.props.onLoad}
                onPost={this.props.onPost}
                editModeOn={this.editModeOn}
                editMode={this.state.editMode}
                variables={this.props.variables}
                registry={Registry}
                editElement={this.state.editElement}
                renderEditForm={this.props.renderEditForm}
              />
              <Toolbar {...toolbarProps} customItems={this.props.customToolbarItems} />
            </div>
          </div>
        </div>
      </DndProvider> */}
      </>
    );
  }
}

export default Appcomponent;

Appcomponent.propTypes = {
  show_description: PropTypes.string,
  toolbarItems: PropTypes.string,
  updateElement: PropTypes.func,
  files: PropTypes.array,
  showCorrectColumn: PropTypes.string,
  data: PropTypes.array,
  url: PropTypes.string,
  saveUrl: PropTypes.string,
  onLoad: PropTypes.string,
  variables: PropTypes.string,
  renderEditForm: PropTypes.func,
  params: PropTypes.object,
  onPost: PropTypes.func
};

// const FormBuilders = {};
// FormBuilders.ReactFormBuilder = Appcomponent;
// FormBuilders.ReactFormGenerator = ReactFormGenerator;
// FormBuilders.ElementStore = store;
// FormBuilders.Registry = Registry;

// export default FormBuilders;

// export {
//     Appcomponent , ReactFormGenerator, store as ElementStore, Registry,
// };
