import React from 'react';
import Headers from '../../../components/Header';
// import SubheaderComponent from '../../Canvas/SubheaderComponent';
import Appcomponent from './components';
import { Layout } from 'antd';
import './style.css';
// import { useParams } from 'react-router-dom';

const { Content } = Layout;

const EditorFormPage = () => {
  return (
    <>
      <div className="editor_form_page_main">
        <Layout>
          <Headers />
          {/* <SubheaderComponent cancelUrl="/app/pages" /> */}
          <Layout>
            <Content>
              <Appcomponent />
            </Content>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default EditorFormPage;
