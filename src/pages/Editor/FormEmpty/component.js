import React from 'react';

import FormPageComponent from '../../../components/Forms/FormPageComponent';

import './style.css';

const AppComponent = () => {
  return (
    <>
      <div className="page_type">
        <FormPageComponent />
      </div>
    </>
  );
};

export default AppComponent;
