import { MoreOutlined } from '@ant-design/icons';
import React from 'react';
import 'antd/dist/antd.css';
import { Button, Row, Col, Select, Form } from 'antd';
import '../../../components/theme.css';
import './style.css';

const { Option } = Select;

const SubheaderComponent = () => {
  return (
    <div className="subheader_components">
      <Row className="subheader_components_box" gutter={[0, 10]} align="middle">
        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
          <Row className="first_row" align="middle" gutter={[10, 8]}>
            <Col xl={2} lg={2} md={4} sm={2} xs={5}>
              {/* <div  className='cancel-box'>
                <i className='icon_back_arrow'></i> Cancel

              </div> */}
              <div className="cancel-box text-center">
                {/* onClick={() => {
                navigate('/app/pages', {
                  state: { data: { _id: props.appId }, from: 'app list', moduleId: 0 }
                });
              }} */}
                <Button type="link">
                  <i className="icon_back_arrow text-center" /> <span className="mt-1">Cancel</span>
                </Button>
              </div>
            </Col>
            <Col className="dropdown-border-none vertical-line" xl={8} lg={6} md={5} sm={6} xs={12}>
              {/* <div > */}
              <Form.Item label="Module" style={{ width: '100%' }}>
                <Select placeholder="Students" style={{ width: '100%' }}>
                  <Option value="jack">Students</Option>
                  <Option value="lucy">Students</Option>
                  <Option value="tom">Students</Option>
                </Select>
              </Form.Item>
              {/* </div> */}
            </Col>
            <Col className="dropdown-border-none vertical-line" xl={8} lg={6} md={6} sm={5} xs={10}>
              {/* <div > */}
              {/* </div> */}
              <Form.Item label="Page" style={{ width: '100%' }}>
                <Select placeholder="Personal Information">
                  <Option value="jack">Personal Information</Option>
                  <Option value="lucy">Personal Information</Option>
                  <Option value="tom">Personal Information</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xl={6} lg={6} md={6} sm={8} xs={12}>
              {' '}
              Status: <span className="status-div"> Action Pending </span>
            </Col>
          </Row>
        </Col>
        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
          <Row className="seconed_row" align="middle" gutter={[10, 8]}>
            <Col xl={6} lg={6} md={6} sm={7} xs={8}>
              {/* <i className='icon_comment ' >  </i><IconLink   text=' Comments (0)' /> */}
              {/* <i className='icon_comment ' >  </i> */}
              <Button type="link">
                <i className="icon_comment_link" />{' '}
                <span className="margin-right"> Comments (0)</span>
              </Button>
              {/* <Button className='preview-btn' style={{ width: '100%' }} > <i className='icon_comment' />
                Comments (0)
                </Button> */}
            </Col>
            <Col xl={4} lg={4} md={4} sm={4} xs={8}>
              <div className="preview-btn">
                <Button style={{ width: '100%' }}>
                  <i className="circular_tick"></i>
                  Save
                  <i className="icon_triangle_down"></i>
                </Button>
                {/* <Select icon={<CheckCircleOutlined />} defaultValue='Save ' style={{ width: '100%' }}>
                    <Option value='jack'>Jack</Option>
                    <Option value='lucy'>Lucy</Option>
                    <Option value='tom'>Tom</Option>
                  </Select> */}
              </div>
            </Col>
            <Col className="preview-btn" xl={4} lg={4} md={4} sm={4} xs={8}>
              <Button style={{ width: '100%' }}>
                <i className="icon_eye"></i>
                Preview
              </Button>
            </Col>
            <Col xl={4} lg={4} md={4} sm={4} xs={8}>
              <Button style={{ width: '100%' }} type="primary" icon={<i className="icon_rocket" />}>
                {' '}
                Publish{' '}
              </Button>
            </Col>
            <Col className="more-btn" xl={2} lg={2} md={2} sm={4} xs={3}>
              <Button icon={<MoreOutlined />}> </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default SubheaderComponent;
