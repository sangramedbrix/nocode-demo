import React from 'react';
import Headers from '../../../components/Header';
import SideBar from '../../../components/Sidebar';
import AppComponent from './component';
import { Layout } from 'antd';
import SubheaderComponent from '../../../pages/Canvas/SubheaderComponent';

const { Sider, Content } = Layout;

const FormEmptys = () => {
  return (
    <Layout>
      <Headers />
      <SubheaderComponent />
      <Layout>
        <Sider>
          <SideBar />
        </Sider>
        <Content>
          <AppComponent />
        </Content>
      </Layout>
    </Layout>
  );
};

export default FormEmptys;
