import React from 'react';
import { Row, Col } from 'antd';
import 'antd/dist/antd.css';
import FormEmpty from '../../../components/Forms/FormEmpty';
import SideBar from '../../../components/Sidebar';
import './style.css';
import EmptyChat from '../../../components/Forms/EmptyChat';

const Appcomponent = () => {
  return (
    <>
      <div className="editor_chat">
        <Row gutter={[20, 20]}>
          <Col xl={4}>
            <SideBar />
          </Col>
          <Col xl={14}>
            <FormEmpty />
          </Col>
          <Col xl={6}>
            {/* <AllComentsTab /> */}
            <EmptyChat />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Appcomponent;
