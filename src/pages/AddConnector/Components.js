import React from 'react';
import 'antd/dist/antd.css';
import InputText from '../../components/InputsFields/InputText';
import InputCheckbox from '../../components/InputsFields/InputCheckbox';
import Buttons from '../../components/Buttons/Buttons';
import './create_new_app.css';

const CreateNewAppComponents = () => {
  return (
    <>
      <div className="flex-container">
        <div className="containers">
          <div className="form-box">
            <div className="mtb-2 bolder">
              <div className="loginsubtext">App Name</div>
              <InputText placeholder_text="Enter App Name" input_size="medium" />
            </div>

            <div className="mtb-1 ">
              <div className="loginsubtext bolder">Status</div>
              <div className="">
                <InputCheckbox check_text="Yes, I agree to show my App to public" />
              </div>
            </div>
            <div className="mtb-1 btn-div">
              <Buttons button_color="link" button_title="Cancel" button_size="medium" />
              <Buttons
                button_color="primary"
                button_title="Select Theam"
                button_size="medium"
                style={{ borderRadius: '10%' }}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateNewAppComponents;
