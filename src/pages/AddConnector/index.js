import React from 'react';
import CreateNewAppComponents from './Components';

const CreateNewApp = () => {
  return (
    <>
      <CreateNewAppComponents />
    </>
  );
};

export default CreateNewApp;
