import React, { useState, useEffect } from 'react';
import {
  Row,
  Col,
  Input,
  Select,
  Dropdown,
  Menu,
  Button,
  Modal,
  Form,
  Checkbox,
  DatePicker
} from 'antd';
import CreateModulePopup from '../../components/Popup/CreateModulePopup';
import '../../config/config';
import Tile1 from '../../components/Subheader/Tile1';
import Tile2 from '../../components/Subheader/Tile2';
import Tile3 from '../../components/Subheader/Tile3';
import Tile5 from '../../components/Subheader/Tile5';
import ViewBoxes from '../../components/ViewBoxes/Index';
import { DeleteOutlined } from '@ant-design/icons';
import TextHeadingH4 from '../../components/TextHeadings/TextHeadingH4';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import EmptyBox from './EmptyBox';
import axios from 'axios';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu';
import InputText from '../../components/InputsFields/InputText';
import '../../assets/styles/index.css';
import '../../components/Popup/popup.css';
import Headers from '../../components/Header';
import moment from 'moment';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import { auth } from '../../authentication/auth';

const { Sider, Content } = Layout;
const { Option, OptGroup } = Select;

const AppCreated = () => {
  const { appId } = useParams();
  console.log('APPIS', appId);
  const [modules, setModules] = useState([]);
  const [moduleDescription, setModuleDescription] = useState();
  const [moduleId, setModuleId] = useState();
  const [app, setApp] = useState([]);
  const [companyId, setCompanyId] = useState();
  const [moduleCount, setModuleCount] = useState();
  const [pageCount, setPageCount] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleDelete, setIsModalVisibleDelete] = useState(false);
  const [isModalVisibleModule, setIsModalVisibleModule] = useState(false);
  const [isModalVisibleDescription, setIsModalVisibleDescription] = useState(false);
  const [checked, setChecked] = useState(false);
  const [module, setModuleData] = useState([]);
  const [deleteModuleId, setDeleteModuelId] = useState();
  const [deleteModuleName, setDeleteModuleName] = useState();
  const [createModule, setModule] = useState({
    name: '',
    description: '',
    isActive: 0,
    nameValidation: '',
    descriptionValidation: '',
    checked: true
  });

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeStatusValue, setSearchTypeStatusValue] = useState('');
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState(1);

  const { state } = useLocation();
  const navigate = useNavigate();

  console.log('new state in app created datas', useLocation());
  const storage = JSON.parse(localStorage.getItem('isAuthenticated'));
  const username = storage[0].firstName + storage[0].lastName;
  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });

  // return useLocation();

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
      setModule((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    }
  };

  function onChangeCheck(e) {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      setChecked(true);
      setModule((prevState) => ({ ...prevState, isActive: 1 }));
    } else if (e.target.checked === false) {
      setChecked(false);
      setModule((prevState) => ({ ...prevState, isActive: 0 }));
    }
  }

  const deleteModule = (moduleId, moduleName) => {
    setDeleteModuelId(moduleId);
    setDeleteModuleName(moduleName);
    setIsModalVisibleDelete(true);
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deleteModule/${deleteModuleId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisibleDelete(false);
          getModules();
          getModulesCount();
          getModules();
        }
      })
      .catch((error) => console.log(error));
  };

  //FILTER DATA CODES
  const handleChange = (event) => {
    setSearchTypeNameValue(event.target.value);
    console.log('jnfgndfgfdg', event.target.value);
    axios
      .post(global.config.API_URL + '/getAppModuleList', {
        appId: appId,
        searchTypeNameValue: event.target.value,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue
      })
      .then((response) => {
        console.log('search response', response);
        setModules(response.data.data);
        // setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeDropDown = (event) => {
    setSearchTypeSortValue(event);
    axios
      .post(global.config.API_URL + '/getAppModuleList', {
        appId: appId,
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: event,
        searchByStatusValue: searchTypeStatusValue
      })
      .then((respo) => {
        console.log('MODULE RdfgdfgdfgdESPONSE', respo);
        setModules(respo.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeStatus = (event) => {
    setSearchTypeStatusValue(event);
    if (event == 'all') {
      setSearchTypeStatusValue('');
      axios
        .post(global.config.API_URL + '/getAppModuleList', {
          appId: appId,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: ''
        })
        .then((respon) => {
          setModules(respon.data.data);
        })
        .catch((error) => console.log(error));
    } else {
      axios
        .post(global.config.API_URL + '/getAppModuleList', {
          appId: appId,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: event
        })
        .then((respon) => {
          setModules(respon.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  function onChange(value, dateString) {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
    setSearchTypeDateValue(dateString);
    axios
      .post(global.config.API_URL + '/getAppModuleList', {
        appId: appId,
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: dateString,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue
      })
      .then((responseData) => {
        setModules(responseData.data.data);
      })
      .catch((error) => console.log(error));
  }

  var getModules = async () => {
    axios
      .post(global.config.API_URL + '/getModules', { appId: appId })
      .then((response) => {
        console.log('MODULE RESPONSE response idsss', response);
        setModules(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const getPages = () => {
    axios
      .post(global.config.API_URL + '/gePages', { moduleId: '61de7af378842127c7f7acc2' })
      .then((response) => {
        console.log('response is', response);
        // setPages(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const showModal = (module_id) => {
    setModuleId(module_id);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    setIsModalVisibleDescription(false);
  };

  const handleCancel = () => {
    setCreatePage((prevState) => ({
      ...prevState,
      name: '',
      description: '',
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisible(false);
    setIsModalVisibleDescription(false);
  };

  const handleCancelDelete = () => {
    setIsModalVisibleDelete(false);
  };

  const nameChangeModule = (name) => {
    if (name != '') {
      setModule((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setModule((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const handleCancelModule = () => {
    setIsModalVisibleModule(false);
    setModuleData([]);
  };

  const editModule = (moduleId) => {
    console.log(moduleId);

    axios
      .post(global.config.API_URL + `/getDetailById`, {
        id: moduleId,
        tableName: 'modules'
      })
      .then((response) => {
        console.log(typeof response.data.data.isActive);
        console.log('response', response);
        setModuleData(response.data.data);
        setModule((prevState) => ({
          ...prevState,
          name: response.data.data.name,
          description: response.data.data.description,
          isActive: response.data.data.isActive,
          nameValidation: false,
          descriptionValidation: false
        }));

        if (response.data.data.isActive === 1) {
          setChecked(true);
          console.log('if');
        } else {
          setChecked(false);
          console.log('else');
        }
      })
      .catch((error) => console.log(error));

    setTimeout(() => {
      setIsModalVisibleModule(true);
    }, 1000);
  };

  const onUpdateModule = () => {
    axios
      .put(global.config.API_URL + `/updateModule/${module._id}`, {
        name: createModule.name,
        description: createModule.description,
        isActive: createModule.isActive
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisibleModule(false);
          getModules();
        }
      })
      .catch((error) => console.log(error));
  };

  const createPageModule = () => {
    // console.log(moduleId);
    if (createPage.name != '') {
      axios
        .post(global.config.API_URL + '/createPage', {
          moduleId: moduleId,
          pageName: createPage.name,
          description: createPage.description
        })
        .then((response) => {
          console.log('page response', response);
          // if (response.data.message === 'Page created!') {
          setCreatePage((prevState) => ({ ...prevState, name: '', description: '' }));

          let newArray = [];
          let items = {
            moduleId: moduleId,
            pageName: createPage.name
          };
          newArray.push(items, { appId: appId }, app);
          setTimeout(() => {
            navigate(`/app/${appId}/module/${moduleId}/page/${response.data.message._id}`);
          }, 2000);
          // }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      if (createPage.name == '') {
        setCreatePage((prevState) => ({ ...prevState, nameValidation: true }));
      }
    }
  };

  // "appId":"61f7af0094851d3e601c929d",
  // "searchTypeNameValue":"Chat",
  // "searchTypeDateValue":"2022-02-02",
  // "searchTypeSortValue":"-1",
  // "searchByStatusValue":"1"

  const getModulesCount = () => {
    axios
      .post(global.config.API_URL + '/getModuleCount', { appId: appId })
      .then((res) => {
        console.log('response data call', res);
        if (res.data.data.length > 0) {
          setModuleCount(res.data.data[0].count);
        } else {
          setModuleCount(0);
        }
      })
      .catch((error) => console.log(error));
  };

  const getPageCount = () => {
    console.log('dataaaa');
    axios
      .post(global.config.API_URL + '/getAllPages', { appId: appId, moduleId: 0 })
      .then((res) => {
        console.log('page count', res);
        setPageCount(res.data.data.length);
      })
      .catch((error) => console.log(error));
  };

  const getAppDetails = () => {
    auth.isValidAppModule(appId);
    axios
      .post(global.config.API_URL + '/getAppById', { appId: appId })
      .then((response) => {
        console.log('APP DETAILS IS', response.data.data.length);
        if (response.data.data.length == 0) {
          navigate('/apps');
        }
        setApp(response.data.data[0]);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    if (!appId) {
      window.location.href = '/apps';
    }
    auth.isValidObjectId(appId, 0);
    getAppDetails();
    getModules();
    getModulesCount();
    getPageCount();
    getPages();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setCompanyId(data[0].companyId);
  }, []);

  return (
    <>
      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger"> Page name is required</p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={createPage.description}
              />
              {createPage.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Description is required</span>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancel}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={createPageModule}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* edit modal popup here */}

      <Modal
        title=""
        footer={null}
        onOk={handleOk}
        onCancel={handleCancel}
        visible={isModalVisibleDescription}>
        {moduleDescription}
      </Modal>

      <div className="create_module">
        <Modal
          destroyOnClose={true}
          visible={isModalVisibleModule}
          className="create_module_popup"
          onCancel={handleCancelModule}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Edit Module" />

          <Form layout="vertical">
            <Form.Item
              label="Module Name"
              name="Module Name"
              rules={[
                {
                  required: true
                }
              ]}>
              <InputText
                placeholder_text="Enter Module Name"
                value={module.name}
                nameChangeFunction={nameChangeModule}
              />
              {createModule.nameValidation === true ? (
                <p className="text-danger"> Module name is required</p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description">
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={module.description}
              />
              {createModule.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Module description is required</span>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item>
              <Checkbox size="small" checked={checked} onChange={onChangeCheck}>
                Yes, I agree to show my module to public
              </Checkbox>
            </Form.Item>
            {/* <Form.Item
                    label='Status'
                    name='Status'
                    valuePropName='checked'
                    wrapperCol={{
                      offset: 1,
                      span: 20
                    }}>
                    <Checkbox checked={true} onChange={onChange}>
                      Yes, I agree to show my module to public
                    </Checkbox>
                  </Form.Item> */}

            <Form.Item
              wrapperCol={{
                offset: 4,
                span: 16
              }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelModule}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={onUpdateModule}>
                  {' '}
                  Update{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* delete popup */}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisibleDelete}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3={`You are about to delete the '${deleteModuleName}' module`} />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version.
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button
                type="primary"
                size="large"
                className="cancel"
                onClick={() => handleCancelDelete()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent activeKey="3" />
          </Sider>
          <Content>
            <div
              className="containers bg-light-blue pad-3"
              style={{ overflowY: 'auto', zIndex: 1, height: '100vh' }}>
              <div className="subheader-container ptb-1">
                <Row>
                  <Col xl={7} lg={7} md={12} sm={12} xs={24}>
                    <Tile1
                      appName={app.name}
                      userName={username}
                      appLogo={
                        global.config.API_URL +
                        `/uploads/company/${companyId}/app/${appId}/logo/${app.logoFileName}`
                      }
                      createdDate={moment(app.createdAt).format('Do dddd, YYYY')}
                      moduleCount={moduleCount}
                      pageCount={pageCount}
                      appId={appId}
                      appData={app}
                    />
                  </Col>
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <Tile2 />
                  </Col>
                  <Col xl={6} lg={5} md={12} sm={12} xs={24}>
                    <Tile3 />
                  </Col>
                  <Col xl={5} lg={6} md={12} sm={12} xs={24}>
                    <Tile5 appId={appId} />
                  </Col>
                </Row>
              </div>

              <div className="ptb-2">
                <Row>
                  <Col xl={20} lg={20} md={24} sm={24} xs={24}>
                    <div className="Search_box">
                      <Row gutter={[10, 10]}>
                        <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                          <Input placeholder="Search ..." onChange={handleChange} />
                        </Col>
                        <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                          <Select
                            defaultValue="Sort by Layout"
                            onChange={handleChangeDropDown}
                            style={{ width: '100%' }}>
                            <OptGroup label="Sort by ">
                              <Option value="1">Sort by A to Z</Option>
                              <Option value="-1">Sort by Z to A</Option>
                            </OptGroup>
                          </Select>
                        </Col>
                        <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                          <Select
                            defaultValue="Status"
                            onChange={handleChangeStatus}
                            style={{ width: '100%' }}>
                            <OptGroup label="Status">
                              <Option value="all">All</Option>
                              <Option value="1">Public</Option>
                              <Option value="0">Private</Option>
                            </OptGroup>
                          </Select>
                        </Col>
                        <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                          <div className="datepicker">
                            <DatePicker
                              placeholder="Last Date"
                              format="MM/DD/YYYY"
                              onChange={onChange}></DatePicker>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                  <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                    <ViewBoxes
                      grid="active"
                      list="inactive"
                      listlink={`/app/${appId}/modules/list`}
                      gridlink={`/app/${appId}/modules`}
                      data={state}
                    />
                  </Col>
                </Row>
              </div>
              <div className="ptb-1">
                {console.log('length', typeof modules.length)}
                {(() => {
                  if (modules.length > 0) {
                    return (
                      <>
                        <div className="">
                          <Row gutter={[30, 30]}>
                            <Col xl={6}>
                              <CreateModulePopup
                                moduleName="Add New Module"
                                getModulesData={getModules}
                                getModulesCount={getModulesCount}
                              />
                            </Col>

                            {(() => {
                              return modules.map((createdModule, i) => {
                                return (
                                  <>
                                    {/* <Row> */}
                                    <Col xl={6} key={i} style={{ width: 'auto' }}>
                                      <div className="AssetManagementAppCard">
                                        <div className="AssetManagementAppCardDiv">
                                          <div className="card-body">
                                            <Row>
                                              <Col xs={22}>
                                                <div className="title">{createdModule.name} </div>
                                                <div className="sub_title">
                                                  {moment(createdModule.createdAt).format(
                                                    'Do dddd, YYYY'
                                                  )}{' '}
                                                </div>
                                                {createdModule.isActive === 0 ? (
                                                  <span className="status-private"> ● Private</span>
                                                ) : (
                                                  <span className="status-public"> ● Public</span>
                                                )}

                                                {/* {createdModule.isActive == 0 ? <span style={{color: 'rgb(210, 181, 85)'}}>Private</span> : <span style={{color:'green'}}>Public</span>} */}
                                              </Col>
                                              <Col xs={2} className="option-dots">
                                                <Dropdown
                                                  placement="bottomRight"
                                                  overlay={(() => {
                                                    console.log('modules', createdModule);
                                                    return (
                                                      <Menu className="list_menu">
                                                        <Menu.Item>
                                                          <a
                                                            target="_blank"
                                                            rel="noopener noreferrer"
                                                            onClick={() => {
                                                              navigate(
                                                                `/app/${appId}/module/${createdModule._id}`,
                                                                {
                                                                  state: {
                                                                    data: createdModule,
                                                                    appId: 0
                                                                  }
                                                                }
                                                              );
                                                            }}>
                                                            {' '}
                                                            View Pagesd{' '}
                                                          </a>
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                          <a
                                                            target="_blank"
                                                            onClick={() =>
                                                              editModule(createdModule._id)
                                                            }>
                                                            {' '}
                                                            Edit{' '}
                                                          </a>
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                          <a
                                                            target="_blank"
                                                            rel="noopener noreferrer"
                                                            onClick={() =>
                                                              deleteModule(
                                                                createdModule._id,
                                                                createdModule.name
                                                              )
                                                            }>
                                                            {' '}
                                                            Delete{' '}
                                                          </a>
                                                        </Menu.Item>
                                                      </Menu>
                                                    );
                                                  })()}>
                                                  <a
                                                    className="ant-dropdown-link"
                                                    onClick={(e) => e.preventDefault()}>
                                                    <i className="icon_more"></i>
                                                  </a>
                                                </Dropdown>
                                              </Col>
                                            </Row>

                                            <div>
                                              <div className="small_txt" maxLength="20">
                                                {(() => {
                                                  if (createdModule.description != '') {
                                                    if (createdModule.description.length > 60) {
                                                      return (
                                                        <p>
                                                          {createdModule.description.substring(
                                                            0,
                                                            60
                                                          )}
                                                          {/* <Tooltip title="Read More"> */}
                                                          <a
                                                            onClick={() => {
                                                              setIsModalVisibleDescription(true);
                                                              setModuleDescription(
                                                                createdModule.description
                                                              );
                                                            }}>
                                                            <span
                                                              style={{
                                                                marginLeft: 5,
                                                                fontSize: 12
                                                              }}>
                                                              Read more...
                                                            </span>
                                                          </a>
                                                          {/* </Tooltip> */}
                                                        </p>
                                                      );
                                                    } else {
                                                      return (
                                                        <>
                                                          <p>{createdModule.description}</p>
                                                        </>
                                                      );
                                                    }
                                                  }
                                                })()}
                                                {/* {createdModule.description.substring(0, 100)}<a onClick={() => {
                                                  setIsModalVisibleDescription(true)
                                                  setModuleDescription(createdModule.description)
                                                }} style={{marginTop: 26,marginRight: 3}}>...</a>
                                               */}
                                              </div>
                                              <div className="large-txt">
                                                Start now creating pages.
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="btn-create">
                                          <Button
                                            type="primary"
                                            size="medium"
                                            className="Buton_small"
                                            onClick={() => showModal(createdModule._id)}>
                                            Create New Page
                                          </Button>
                                        </div>
                                      </div>
                                    </Col>
                                    {/* </Row> */}
                                  </>
                                );
                              });
                            })()}

                            {/* <AddNewModule
                            moduleName='Module List'
                            module_id='sfd'
                            card_title='dfd'
                            card_date='dsf'
                            card_description='dsd'
                            getModules={getModules}
                            getModulesCount={getModulesCount}
                            searchFilter={searchFilter}
                          /> */}
                          </Row>
                        </div>
                      </>
                    );
                  } else {
                    return (
                      <>
                        <Row gutter={[10, 10]}>
                          <Col xl={6} lg={8} md={10} sm={12} xs={24}>
                            <div>
                              <CreateModulePopup
                                moduleName="Add New Module"
                                getModulesData={getModules}
                                getModulesCount={getModulesCount}
                              />
                            </div>
                          </Col>
                          <Col xl={18} lg={16} md={14} sm={12} xs={24}>
                            <EmptyBox />
                          </Col>
                        </Row>
                      </>
                    );
                  }
                })()}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default AppCreated;
