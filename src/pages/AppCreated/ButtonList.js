import React from 'react';
import 'antd/dist/antd.css';
import { Row, Col, Input, Select } from 'antd';

const { Option } = Select;

const ButtonList = () => {
  return (
    <div className="app_create_Search_Box">
      <Row gutter={[10, 10]}>
        <Col xl={10} lg={8} md={8} sm={7} xs={24}>
          <Input placeholder="Search ..." />
        </Col>
        <Col xl={3} lg={4} md={4} sm={4} xs={24}>
          <Select defaultValue="Type" style={{ width: '100%' }}>
            <Option value="type1">Type 1 </Option>
            <Option value="type2">Type 2 </Option>
          </Select>
        </Col>
        <Col xl={3} lg={4} md={4} sm={4} xs={24}>
          <Select defaultValue="Status" style={{ width: '100%' }}>
            <Option value="status1">Status 1</Option>
            <Option value="status2">Status 2</Option>
          </Select>
        </Col>
        <Col xl={3} lg={4} md={4} sm={4} xs={24}>
          <Select defaultValue="Comments" style={{ width: '100%' }}>
            <Option value="comments1">Comments 1</Option>
            <Option value="comments2">Comments 2</Option>
          </Select>
        </Col>
        <Col xl={3} lg={4} md={4} sm={4} xs={24}>
          <Select defaultValue="Last Date" style={{ width: '100%' }}>
            <Option value="lastDay1">Last Date 1</Option>
            <Option value="lastDay2">Last Date 2</Option>
          </Select>
        </Col>
      </Row>
    </div>
  );
};

export default ButtonList;
