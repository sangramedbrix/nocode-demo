import React from 'react';
import { Row, Col } from 'antd';
import BlocksGridView from '../../components/Cards/BlocksGridView';
import SearchBox from '../../components/Dropdowns/SearchBox';
import ViewBoxes from '../../components/ViewBoxes/Index';
import Aa from '../../assets/images/img1.png';

const BlockGrid = () => {
  return (
    <>
      <div className="select_theam_search_box mb-5">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <SearchBox />
          </Col>
          <Col xl={4} lg={4} md={24} sm={24} xs={24}>
            <ViewBoxes />
          </Col>
        </Row>
      </div>
      <div>
        <Row className="cardrow mtb-2 mb-5" gutter={[16, 25]}>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 1"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 2"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 3"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 4"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 5"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 6"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 7"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 8"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24} className="theam_select_card_col">
            <BlocksGridView
              block_grid_icon={Aa}
              card_title="Block 9"
              app_grid_date="7th December,2021"
              status="&bull; In Progress"
              footer_="0 page"
              footer_comments="0 Comments"
              footer_public_private="Public"
            />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default BlockGrid;
