import React from 'react';
import Components from './components';
import Headers from '../../components/Header';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu/component';
import '../../assets/styles/index.css';

const { Sider, Content } = Layout;

const RecentActivitiesList = () => {
  return (
    <>
      <Headers />

      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent activeKey="3" />
          </Sider>
          <Content>
            <Components />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default RecentActivitiesList;
