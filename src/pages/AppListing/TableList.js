import React, { useEffect, useState } from 'react';
import '../../config/config';
import { Row, Col, Modal, Button, Table, Input, Select, DatePicker, Tooltip } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import axios from 'axios';
import moment from 'moment';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import InputFile from '../../components/InputsFields/InputFile';
import Dropdowns from '../../components/Dropdowns/Dropdowns';
import AppImageBanner from '../../assets/images/app_banner.png';
import { Checkbox } from 'antd';
import ViewBoxes from '../../components/ViewBoxes/Index';

const { Option, OptGroup } = Select;

const TableList = (props) => {
  const [companyId, setCompanyId] = useState();
  const [authUser, setAuthUser] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleApp, setIsModalVisibleApp] = useState(false);
  const [appId, setAppId] = useState();
  const [app, setApp] = useState([]);
  const [apps, setApps] = useState([]);
  const [appCategory, setAppCategory] = useState([]);
  const [checked, setChecked] = useState(false);
  const [deleteAppName, setDeleteAppName] = useState();
  const [createNewApp, setCreateNewApp] = useState({
    name: '',
    category: '',
    description: '',
    isActive: 0,
    file: [],
    originalFileName: '',
    nameValidation: '',
    categoryValidation: '',
    appTypeValidation: '',
    appType: '',
    descriptionValidation: '',
    imageValidation: ''
  });

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeStatusValue, setSearchTypeStatusValue] = useState('');
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState('');
  const [searchTypeCategoryId, setSearchTypeCategoryId] = useState('');
  const [searchAppType, setSearchAppType] = useState('');

  const getApps = () => {
    axios
      .get(global.config.API_URL + '/getApps')
      .then((response) => {
        console.log('response apps data', response);
        setApps(response.data.data);
        //  setCount(array)
      })
      .catch((error) => console.log(error));
  };
  const handleChangeApplicationType = (event) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      appType: event,
      appTypeValidation: false
    }));
  };
  const handleChange = (event) => {
    console.log('jnfgndfgfdg', event.target.value);
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());
    console.log(current);
    console.log(
      event.target.value,
      searchTypeDateValue,
      searchTypeSortValue,
      searchTypeStatusValue,
      searchTypeCategoryId,
      searchAppType
    );

    setSearchTypeNameValue(event.target.value);
    // setSearchTypeStatusValue('');
    // setSearchTypeDateValue(current);
    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: event.target.value,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((res) => {
        console.log('response data', res);
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeDropDown = (event) => {
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());

    setSearchTypeSortValue(event);

    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: event,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((res) => {
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeType = (event) => {
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());

    setSearchAppType(event);

    if (event === '') {
      axios
        .post(global.config.API_URL + '/getAppList', {
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: searchTypeStatusValue,
          searchByCategoryId: searchTypeCategoryId,
          appType: ''
        })
        .then((res) => {
          setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    } else {
      axios
        .post(global.config.API_URL + '/getAppList', {
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: searchTypeStatusValue,
          searchByCategoryId: searchTypeCategoryId,
          appType: event
        })
        .then((res) => {
          setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  const handleChangeCategory = (event) => {
    console.log(event);
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());

    setSearchTypeCategoryId(event);

    if (event === '') {
      axios
        .post(global.config.API_URL + '/getAppList', {
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: searchTypeStatusValue,
          searchByCategoryId: '',
          appType: searchAppType
        })
        .then((res) => {
          console.log(res);
          setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    } else {
      axios
        .post(global.config.API_URL + '/getAppList', {
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: searchTypeStatusValue,
          searchByCategoryId: event,
          appType: searchAppType
        })
        .then((res) => {
          console.log(res);
          setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  const handleChangeStatus = (event) => {
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());
    setSearchTypeStatusValue(event);
    if (event === 'all') {
      axios
        .post(global.config.API_URL + '/getAppList', {
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: '',
          searchByCategoryId: searchTypeCategoryId,
          appType: searchAppType
        })
        .then((res) => {
          console.log(res);
          setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    } else {
      axios
        .post(global.config.API_URL + '/getAppList', {
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue,
          searchByStatusValue: event,
          searchByCategoryId: searchTypeCategoryId,
          appType: searchAppType
        })
        .then((res) => {
          console.log(res);
          setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  function onChangeDate(value, dateString) {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
    setSearchTypeDateValue(dateString);
    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: dateString,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((responseData) => {
        console.log(responseData);
        setApps(responseData.data.data);
      })
      .catch((error) => console.log(error));
  }

  let cartData = [{}];
  let newData1 = JSON.stringify(apps);
  const valuesArray = JSON.parse(newData1);
  {
    valuesArray.map(function (item) {
      //call api
      axios
        .post(global.config.API_URL + '/getModuleCount', { appId: item._id })
        .then((countValue) => {
          setTimeout(function () {
            cartData.push(countValue.data);
          }, 5000);
          //console.log("countValue",Object.keys(countValue.data).length)
          // cartData.push({
          //   key: 1010
          // });
        })
        .catch((error) => console.log(error));

      console.log('item', item._id);
    });
  }

  console.log('APP-CART DATA', cartData);
  var counts = Object.keys(cartData).length;
  console.log('fdlgldfgfdi', counts);

  let cart = [];
  let newData = JSON.stringify(apps);
  console.log('ALLLLLLL', newData);
  const deleteAction = (Id, name) => {
    console.log('222222', Id);
    return (
      <div className="app_details_action_div">
        <Tooltip title="Edit">
          {' '}
          <i className="icon_view" onClick={() => editApp(Id)}></i>
          {/* <FormOutlined onClick={() => editApp(Id)} />{' '} */}
        </Tooltip>

        <Tooltip title="Delete">
          <i className="icon_delete" onClick={() => deleteApp(Id, name)}></i>
          {/* <DeleteOutlined onClick={() => deleteApp(Id, name)} /> */}
        </Tooltip>
      </div>
    );
  };

  const lastDate = (updated_at) => {
    return <div className="last_date_date">{moment(updated_at).format('Do dddd, YYYY')}</div>;
  };

  const res = (item) => {
    return (
      <div className="app_details_box">
        {/* <div className="app_details_box_img_div">
          <img
            src={global.config.API_URL + `/uploads/company/${companyId}/app/${item._id}/logo/${item.logoFileName}`}
          />
        </div> */}
        <div className="app_details_box_text_div">
          <div className="app_details_title">{item.name}</div>
        </div>
      </div>
    );
  };

  const valuesArrays = JSON.parse(newData);
  {
    valuesArrays.map(function (item) {
      var statusValue;
      if (item.isActive === 1) {
        statusValue = 'Public';
      } else {
        statusValue = 'Private';
      }
      cart.push({
        key: item._id,
        app_details: res(item),
        status: statusValue,
        comments: item.description,
        last_date_date: lastDate(item.updatedAt),
        action: deleteAction(item._id, item.name, item)
      });
      console.log('MODULE_ID=====>>>>>>', item);
    });
  }

  const columns = [
    {
      title: 'APP Name',
      dataIndex: 'app_details',
      width: '28%'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      width: '11%',
      render: (state) => (
        <div style={{ color: `${state === 'Public' ? 'green' : '#D2B555'}` }}>{state}</div>
      )
    },
    {
      title: 'Comments',
      dataIndex: 'comments',
      width: '25%',
      render: (comments) => <div className="comments">{comments}</div>
    },
    {
      title: 'Last Modified Date',
      dataIndex: 'last_date_date',
      render: (last_date_date) => <div className="last_date_date">{last_date_date}</div>
    },
    {
      title: ' ',
      dataIndex: 'action'
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      // setSelectedRowKeys(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name
    })
  };

  const categoryChange = (category) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      category: category,
      categoryValidation: false
    }));
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));
      console.log('empty');
    }
  };

  const getAppCategory = () => {
    axios
      .get(global.config.API_URL + '/getAppCategory')
      .then((response) => {
        console.log(response);
        setAppCategory(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const deleteApp = (appId, appName) => {
    console.log(appId);
    setAppId(appId);
    setDeleteAppName(appName);
    setIsModalVisible(true);
    // window.location.reload();
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deleteApp/${appId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisible(false);
          props.getApps();
        }
        window.location.reload();
      })
      .catch((error) => console.log(error), window.location.reload());
  };

  const editApp = (appId) => {
    console.log('APP_ID====>>>>>', appId);
    axios
      .post(global.config.API_URL + `/getDetailById`, { id: appId, tableName: 'apps' })
      .then((response) => {
        console.log('APP RESPONSE DATA', response);
        setApp(response.data.data);
        setCreateNewApp((prevState) => ({
          ...prevState,
          name: response.data.data.name,
          appType: response.data.data.appType,
          description: response.data.data.description,
          originalFileName: response.data.data.logoFileName,
          isActive: response.data.data.isActive,
          nameValidation: false,
          descriptionValidation: false
        }));
        if (response.data.data.isActive === 1) {
          setChecked(true);
        } else {
          setChecked(false);
        }
      })
      .catch((error) => console.log(error));

    setTimeout(() => {
      setIsModalVisibleApp(true);
    }, 1000);
  };

  const nameChange = (name) => {
    console.log('app name', name);
    if (name != '') {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleOk = () => {
    setIsModalVisibleApp(false);
  };

  const handleCancelApp = () => {
    setIsModalVisibleApp(false);
  };

  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      setChecked(true);
      setCreateNewApp((prevState) => ({ ...prevState, isActive: 1 }));
    } else {
      setChecked(false);
      setCreateNewApp((prevState) => ({ ...prevState, isActive: 0 }));
    }
  };

  const selectFile = (file) => {
    console.log('file', file);
    setCreateNewApp((prevState) => ({ ...prevState, file: file, imageValidation: false }));
  };

  const onUpdate = () => {
    setIsModalVisibleApp(false);
    axios
      .put(global.config.API_URL + `/updateApp/${app._id}`, {
        name: createNewApp.name,
        category: createNewApp.category,
        appType: createNewApp.appType,
        updatedUserId: authUser._id,
        description: createNewApp.description,
        isActive: createNewApp.isActive
      })
      .then((response) => {
        console.log('response data', response);
        getApps();
        updateImage();
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getApps();
    getAppCategory();
    // getModuleCount();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setAuthUser(data[0]);
    setCompanyId(data[0].companyId);
  }, []);

  const updateImage = () => {
    const formData = new FormData();
    if (createNewApp.file.length == 0) {
      const originalImage = app.logoFileName;
      formData.append('checkImage', 0);
      formData.append('image', originalImage);
    } else if (createNewApp.file.length == undefined) {
      formData.append('checkImage', 1);
      formData.append('image', createNewApp['file'].file.originFileObj);
      formData.append('logoFileName', app.logoFileName);
    }

    formData.append('companyId', companyId);
    formData.append('id', app._id);

    axios
      .post(global.config.API_URL + `/updateImage`, formData)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => console.log(error));
  };

  const [selectionType] = useState('checkbox');

  console.log('APP DETAILS', app);

  return (
    <>
      {/* delete popup */}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3={`You are about to delete the '${deleteAppName}' app`} />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version.
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* <div className='create_module'>
        <Modal
          visible={isModalVisible}
          className='delete_popup'
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className='confirm_box'>
            <div className='icon'>
              <WarningOutlined className='d-flex text-center' />
            </div>
            <div className='heading'>
              <TextHeadingH3 text_h3='Are you sure you want to delete this app?' />
            </div>
            <div className='description'>
              <p>{props.popup_description} </p>
            </div>
            <div className='buttons_div d-flex text-center'>
              <Button
                type='primary'
                size='large'
                className='Buton_small'
                onClick={() => deleteFunctionCall()}>
                'Yes!
              </Button>
              <Button
                type='danger'
                size='large'
                className='Buton_small'
                onClick={() => handleCancel()}>
                No, Go Back!
              </Button>
            </div>
          </div>
        </Modal>
      </div> */}

      {/* modal for create your own app */}
      <div>
        <Modal
          destroyOnClose={true}
          className="modal-container"
          width={'100%'}
          height={'100%'}
          style={{ top: 0 }}
          header={false}
          footer={false}
          visible={isModalVisibleApp}
          onOk={handleOk}
          onCancel={handleCancelApp}>
          <div className="flex-container bg-light-blue">
            <div className="containers">
              <div className="createApp-form-box">
                <div className="box-title">
                  <div className="headertext text-center">Edit your app...</div>
                  {/* <div className='subheadertxt text-center'>
                    No code App Maker to Build Your App
                  </div> */}
                </div>
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>App Name
                  </div>
                  <InputText
                    placeholder_text="Enter App Name"
                    nameChangeFunction={nameChange}
                    value={app.name}
                  />
                </div>
                <div className="bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>Category
                  </div>
                  <Dropdowns
                    option={appCategory}
                    dropdown_title="Select"
                    value={createNewApp.category}
                    categoryId={app.categoryId}
                    categoryChangeFunction={categoryChange}
                  />
                </div>
                <div className=" bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>Application Type
                  </div>
                  <Select
                    className="dd-box"
                    defaultValue={createNewApp.appType === 1 ? 'Web' : 'Mobile'}
                    onChange={handleChangeApplicationType}>
                    <Option value="1">Web</Option>
                    <Option value="2">Mobile</Option>
                  </Select>
                </div>
                <div className="mtb-1 bolder">
                  <div className="createAppSubtxt">Description</div>
                  <InputTextArea
                    // placeholder_text=''
                    discriptionChangeFunction={discriptionChange}
                    value={app.description}
                  />
                </div>
                <div className="mtb-1 ">
                  <div className="createAppSubtxt bolder">Status</div>
                  <div className="checkbox2">
                    {' '}
                    <Checkbox size="small" checked={checked} onChange={onChange}></Checkbox>
                    <span style={{ marginLeft: 8 }}> Yes, I agree to show my App to public</span>
                  </div>
                </div>
                <div className="mtb-2 ">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span> App Icon
                  </div>
                  <div className="text-input-upload">
                    <InputFile text="No choosen file" fileSelect={selectFile} />
                  </div>
                </div>

                <Row gutter={[70, 0]}>
                  <Col xl={1} lg={1} md={1} sm={1} xs={1}>
                    {/* <img
                        src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
                      /> */}
                  </Col>
                </Row>

                <div className="mtb-1 btn-div">
                  <div className="app-link">
                    <Button onClick={() => handleCancelApp()}>Cancel</Button>
                  </div>
                  <div className="app-button">
                    <Button onClick={onUpdate}>Update</Button>
                    {/* <Buttons button_title='Select Theme' button_size='medium'  onClick={onSubmit} className='app-button-text' /> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="appimgcontainer">
              <div className="create-banner-text">
                <div className="banner-titletop">App Builder to build apps</div>
                <div className="banner-subtitletop">in five minutes without Code </div>
              </div>
              <div>
                <div className="side-img text-center">
                  <img className="m-auto" width="70%" alt="example" src={AppImageBanner} />
                </div>
                <div className="create-banner-text">
                  <div className="banner-title text-center">Design. Build. Launch</div>
                  <div className="banner-subtitle">
                    Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code
                    Required!!!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
      {/* end  modal for create your own app */}
      <div>
        <div className="select_theam_search_box mb-5">
          <Row>
            <Col xl={20} lg={20} md={24} sm={24} xs={24}>
              <div className="Search_box">
                <Row gutter={[10, 10]}>
                  <Col xl={7} lg={8} md={9} sm={9} xs={24}>
                    <Input placeholder="Search ..." onChange={handleChange} />
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Sort by Layout"
                      onChange={handleChangeDropDown}
                      style={{ width: '100%' }}>
                      <OptGroup label="Sort by ">
                        <Option value="1">Sort by A to Z</Option>
                        <Option value="-1">Sort by Z to A</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Type"
                      onChange={handleChangeType}
                      style={{ width: '100%' }}>
                      <OptGroup label="Filter by type">
                        <Option value="">All</Option>
                        <Option value="1">Web</Option>
                        <Option value="2">Mobile</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Category"
                      onChange={handleChangeCategory}
                      style={{ width: '100%' }}>
                      <OptGroup label="filter by category">
                        <Option value="">All</Option>
                        {appCategory.map((cat, i) => {
                          return (
                            <Option value={cat._id} key={i}>
                              {cat.name}
                            </Option>
                          );
                        })}
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Status"
                      onChange={handleChangeStatus}
                      style={{ width: '100%' }}>
                      <OptGroup label="Status">
                        <Option value="">All</Option>
                        <Option value="1">Public</Option>
                        <Option value="0">Private</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    {/* <DatePicker  size="large" format="MM/DD/YYYY"  placeholder="Last Date" onChange={onChangeDate} />  */}
                    <div className="datepicker">
                      <DatePicker
                        placeholder="Last Date"
                        format="MM/DD/YYYY"
                        onChange={onChangeDate}></DatePicker>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xl={4} lg={4} md={24} sm={24} xs={24}>
              <ViewBoxes grid="inactive" list="active" listlink="/app/list" gridlink="/apps" />
            </Col>
          </Row>
        </div>
        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          pagination={{
            defaultPageSize: 4,
            showSizeChanger: true,
            pageSizeOptions: ['10', '20', '30']
          }}
          columns={columns}
          dataSource={cart}
        />
      </div>
    </>
  );
};

export default TableList;

TableList.propTypes = {
  app_grid_icon: PropTypes.string,
  card_title: PropTypes.string,
  status: PropTypes.string,
  app_grid_date: PropTypes.string,
  footer_module: PropTypes.string,
  footer_page: PropTypes.string,
  footer_comments: PropTypes.string,
  appData: PropTypes.array,
  getApps: PropTypes.func,
  popup_description: PropTypes.string,
  appId: PropTypes.string
};
