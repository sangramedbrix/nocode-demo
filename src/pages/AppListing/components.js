import React from 'react';
import RecentActivies from './RecentActivities';
import MyApps from './MyApps';
import MyBlocks from '../AppList/BlockGrid';
import { useNavigate } from 'react-router-dom';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

const Components = () => {
  const navigate = useNavigate();

  function callback(key) {
    if (key == 1) {
      navigate('/recentapps');
    } else if (key == 2) {
      navigate('/apps');
    }
  }

  return (
    <>
      <div className="containers bg-light-blue box-padding">
        <div className="recent_activitivites_list_div">
          <Tabs defaultActiveKey="2" onChange={callback}>
            <TabPane tab="Recent Activities" key="1">
              <RecentActivies />
            </TabPane>
            <TabPane tab="My Apps" key="2">
              <MyApps />
            </TabPane>
            <TabPane tab="My Blocks" key="3">
              <MyBlocks />
            </TabPane>
          </Tabs>
        </div>
      </div>
    </>
  );
};

export default Components;
