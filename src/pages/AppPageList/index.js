import React from 'react';
import Headers from '../../components/Header';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu/component';
import AppComponents from './components';
import '../../assets/styles/index.css';

const { Sider, Content } = Layout;

const AppPageList = () => {
  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent activeKey="3" />
          </Sider>
          <Content>
            <AppComponents />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default AppPageList;
