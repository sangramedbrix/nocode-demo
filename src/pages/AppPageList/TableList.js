import React, { useState, useEffect } from 'react';
import { Table, Modal, Button, Form, Row, Col, Input, Select, DatePicker, Tooltip } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import ViewBoxes from '../../components/ViewBoxes/Index';
import '../../config/config';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import TextHeadingH4 from '../../components/TextHeadings/TextHeadingH4';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import img from '../../assets/images/app_details_img.png';
import '../../components/Popup/popup.css';
import { useNavigate, useParams } from 'react-router-dom';
import { auth } from '../../authentication/auth';
const { Option, OptGroup } = Select;

import moment from 'moment';

const TableList = (props) => {
  console.log('table list props', props);
  const { appId, moduleId } = useParams();
  console.log('APPIS', appId);
  console.log('moduleId', moduleId);
  const { state } = useLocation();
  const navigate = useNavigate();
  const [modules, setModules] = useState([]);
  const [dropDownData, setModulesw] = useState([]);
  const [states, setstate] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisiblePage, setIsModalVisiblePage] = useState(false);
  const [isModalVisibleAddPage, setIsModalVisibleAddPage] = useState(false);
  const [pageId, setPageId] = useState();
  const [app, setApp] = useState([]);
  // const [newAppId, setAppId] = useState(0);
  const [pageName, setDeletePageName] = useState();

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState(1);
  const [searchModuleIdValue, setModuleId] = useState(0);
  const [isModalVisibleDescription, setIsModalVisibleDescription] = useState();
  const [moduleDescription, setModuleDescription] = useState();

  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });

  console.log('STATE----DATA', state, app);

  const showModalAddPage = () => {
    setIsModalVisibleAddPage(true);
  };

  const handleCancelAddPage = () => {
    setIsModalVisibleAddPage(false);
  };
  const handleOkAddPage = () => {
    setIsModalVisibleAddPage(false);
  };

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));

      console.log('empty');
      console.log('empty', dropDownData);
    }
  };

  var getAppDetails = () => {
    if (moduleId && appId) {
      auth.isValidAppModulePages(appId, moduleId);
    }
    axios
      .post(global.config.API_URL + '/getAppById', { appId: appId })
      .then((response) => {
        console.log('response data in app from 2', response);
        setApp(response.data.data[0]);
        // setAppId(state.data.appId);
      })
      .catch((error) => console.log(error));
  };

  var getModules = async () => {
    if (appId != '') {
      axios
        .post(global.config.API_URL + '/getModules', { appId: appId })
        .then((respondse) => {
          console.log('MODULEdfgdfgdgdf', respondse);
          setModulesw(respondse.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  const getApps = () => {
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: appId,
          moduleId: 0
        })
        .then((response) => {
          console.log('REPO', response);
          setstate(response.data.data);
          setModules(response.data.data);
        })
        .catch((error) => console.log(error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: 0,
          moduleId: moduleId
        })
        .then((response) => {
          console.log('REPO', response);
          setstate(response.data.data);
          setModules(response.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  //FILTER code for
  const handleChange = (event) => {
    setSearchTypeNameValue(event.target.value);
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          appId: appId,
          moduleId: searchModuleIdValue,
          searchTypeNameValue: event.target.value,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: moduleId,
          appId: '',
          searchTypeNameValue: event.target.value,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  const handleChangeDropDown = (event) => {
    setSearchTypeSortValue(event);
    if (appId != '' && !moduleId) {
      console.log('moduleId', searchModuleIdValue);
      axios
        .post(global.config.API_URL + '/pageFilters', {
          appId: appId,
          moduleId: searchModuleIdValue,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: event
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: moduleId,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: searchTypeDateValue,
          searchTypeSortValue: event
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };

  function onChange(value, dateString) {
    console.log('Selected Time: ', value);
    setSearchTypeDateValue(dateString);
    console.log('searchTypeNameValue Time: ', searchTypeNameValue);
    console.log('searchTypeSortValue Time: ', searchTypeSortValue);
    console.log('Formatted Selected Time: ', dateString);
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: '',
          appId: appId,
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: dateString,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((responw) => {
          setModules(responw.data.data);
        })
        .catch((error) => console.log('error', error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/pageFilters', {
          moduleId: moduleId,
          appId: '',
          searchTypeNameValue: searchTypeNameValue,
          searchTypeDateValue: dateString,
          searchTypeSortValue: searchTypeSortValue
        })
        .then((respons) => {
          setModules(respons.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  }

  const handleChangeStatus = (event) => {
    setModuleId(event);
    if (appId != '' && !moduleId) {
      if (event === 'all') {
        setModuleId(0);
        axios
          .post(global.config.API_URL + '/pageFilters', {
            appId: appId,
            moduleId: '',
            searchTypeNameValue: searchTypeNameValue,
            searchTypeDateValue: searchTypeDateValue,
            searchTypeSortValue: searchTypeSortValue
          })
          .then((response) => {
            console.log('response data in module id', response);
            setstate(response.data.data);
            setModules(response.data.data);
          })
          .catch((error) => console.log(error));
      } else {
        axios
          .post(global.config.API_URL + '/pageFilters', {
            appId: appId,
            moduleId: event,
            searchTypeNameValue: searchTypeNameValue,
            searchTypeDateValue: searchTypeDateValue,
            searchTypeSortValue: searchTypeSortValue
          })
          .then((responw) => {
            console.log('search response', responw);
            setModules(responw.data.data);
            // setApps(res.data.data);
          })
          .catch((error) => console.log('error', error));
      }
    } else if (appId != '' && moduleId != '') {
      console.log('jnfgndfgfdg', event);
      axios
        .post(global.config.API_URL + '/searchModulePageByName', {
          moduleId: moduleId,
          searchType: 'status',
          searchString: event
        })
        .then((respons) => {
          console.log('search response', respons);
          setModules(respons.data.data);
          // setApps(res.data.data);
        })
        .catch((error) => console.log('error', error));
    }
  };
  useEffect(() => {
    if (!appId && moduleId) {
      window.location.href = '/apps';
    }
    if (!appId && !moduleId) {
      window.location.href = `/app/${appId}/modules`;
    }

    if (appId && !moduleId) {
      auth.isValidObjectId(appId, 0);
    }
    if (moduleId && !appId) {
      auth.isValidObjectId(0, moduleId);
    }
    if (moduleId && appId) {
      auth.isValidObjectId(appId, moduleId);
    }

    getApps();
    getAppDetails();
    getModules();
    props.pageList();
    // const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    // setCompanyId(data[0].companyId);
    // deleteFunctionCall()
  }, []);

  let cart = [];
  let newData = JSON.stringify(modules);
  console.log('modules data', modules);

  const res = (item) => {
    return (
      <div className="app_details_box">
        <div className="app_details_box_img_div">
          <img src={img} />
          {/* <img
            src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
            alt=''
          /> */}
        </div>
        <div className="app_details_box_text_div">
          <div className="app_details_title">{item.pageName}</div>
          <div className="app_details_sub_title">{item.pageName}</div>
        </div>
      </div>
    );
  };

  function deleteModule(pageItem, pageName) {
    console.log('page id', pageItem);
    setPageId(pageItem);
    setDeletePageName(pageName);
    setIsModalVisible(true);
  }

  function updatePage(pageItem, pageName, pageDescription) {
    console.log('page id', pageItem, pageName);
    setPageId(pageItem);
    setCreatePage((prevState) => ({
      ...prevState,
      name: pageName,
      description: pageDescription,
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisiblePage(true);
  }

  const updatePageData = () => {
    axios
      .put(global.config.API_URL + `/updatePage/${pageId}`, {
        name: createPage.name,
        description: createPage.description
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisiblePage(false);
          getApps();
        }
      })
      .catch((error) => console.log(error));

    // console.log('page id', pageId);
    // console.log('page updated data', createPage);
    // props.updatePageData(pageId, createPage);
    // setIsModalVisiblePage(false);
  };

  const createPageModule = () => {
    if (createPage.name != '') {
      axios
        .post(global.config.API_URL + '/createPage', {
          moduleId: state.data._id,
          pageName: createPage.name,
          description: createPage.description
        })
        .then((response) => {
          console.log(response);

          if (response.data.message === 'Page created!') {
            setCreatePage((prevState) => ({ ...prevState, name: '', description: '' }));
            setIsModalVisibleAddPage(false);
            getApps();
            props.pageList();
            props.getPageCount();
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      if (createPage.name == '') {
        setCreatePage((prevState) => ({ ...prevState, nameValidation: true }));
      }
    }
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deletePage/${pageId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          getApps();
          props.getPageCount();
        }
      })
      .catch((error) => console.log(error));

    setIsModalVisible(false);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    setIsModalVisibleDescription(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setIsModalVisibleDescription(false);
  };

  const handleCancelPage = () => {
    setCreatePage((prevState) => ({
      ...prevState,
      name: '',
      description: '',
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisiblePage(false);
  };

  const redirectToTheme = (items) => {
    navigate(`/app/${appId}/module/${items.moduleId}/page/${items._id}`);
  };

  const deleteAction = (pageItem, pageName, pageDescription, pageNames, item) => {
    return (
      <div className="app_details_action_div">
        <Tooltip title="View">
          {/* <EyeOutlined onClick={() => redirectToTheme(item)} /> */}
          <i className="icon_view" onClick={() => redirectToTheme(item)}></i>
        </Tooltip>
        <Tooltip title="Edit">
          {/* <FormOutlined onClick={() => updatePage(pageItem, pageName, pageDescription)} /> */}
          <i
            className="icon_edit"
            onClick={() => updatePage(pageItem, pageName, pageDescription)}></i>
        </Tooltip>
        <Tooltip title="Delete">
          {/* <DeleteOutlined onClick={() => deleteModule(pageItem, pageNames)} /> */}
          <i className="icon_delete" onClick={() => deleteModule(pageItem, pageNames)}></i>
        </Tooltip>
      </div>
    );
  };

  const lastDate = (updated_at) => {
    return <div className="last_date_date">{moment(updated_at).format('Do dddd, YYYY')}</div>;
  };

  const valuesArray = JSON.parse(newData);
  {
    valuesArray.map(function (item) {
      cart.push({
        key: item._id,
        app_details: res(item),
        module: item.moduleName,
        status: 'Approved',
        comments: item.pageDescription,
        last_date_date: lastDate(item.pageUpdateDate),
        action: deleteAction(item._id, item.pageName, item.pageDescription, item.pageName, item)
      });
      console.log('id', item);
    });
  }

  console.log('PAGELISTING', cart);
  const columns = [
    {
      title: 'Pages',
      dataIndex: 'app_details',
      width: '28%'
    },
    {
      title: 'Module',
      dataIndex: 'module',
      width: '10%'
    },
    // {
    //   title: 'Status',
    //   dataIndex: 'status',
    //   width: '11%',
    //   render: (state) => <div className='inprogress'>{state}</div>
    // },
    {
      title: 'Description',
      dataIndex: 'comments',
      width: '25%',
      render: (comments) => (
        <span className="comments">
          {(() => {
            if (comments != '') {
              if (comments.length > 60) {
                return (
                  <p>
                    {comments.substring(0, 60)}
                    <Tooltip title="Read More">
                      <a
                        onClick={() => {
                          setIsModalVisibleDescription(true);
                          setModuleDescription(comments);
                        }}>
                        <span style={{ marginLeft: 5, fontSize: 12 }}>Read more...</span>
                      </a>
                    </Tooltip>
                  </p>
                );
              } else {
                return (
                  <>
                    <p>{comments}</p>
                  </>
                );
              }
            }
          })()}
        </span>
      )
    },
    {
      title: 'Last Modified Date',
      dataIndex: 'last_date_date',
      render: (last_date_date) => <div className="last_date_date">{last_date_date}</div>
    },
    {
      title: ' ',
      dataIndex: 'action'
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name
    })
  };

  const [selectionType] = useState('checkbox');
  return (
    <>
      <Modal
        title=""
        footer={null}
        onOk={handleOk}
        onCancel={handleCancel}
        visible={isModalVisibleDescription}>
        {moduleDescription}
      </Modal>

      {console.log('app', states)}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3
                className="text_align_center"
                text_h3={`You are about to delete a '${pageName}' page`}
              />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version..
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisibleAddPage}
          onOk={handleOkAddPage}
          onCancel={handleCancelAddPage}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger" style={{ marginRight: '214px' }}>
                  {' '}
                  Page name is required
                </p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={createPage.description}
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelAddPage}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={createPageModule}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisiblePage}
          onOk={handleOk}
          onCancel={handleCancelPage}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Edit a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger"> Page name is required</p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                value={createPage.description}
                discriptionChangeFunction={discriptionChange}
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelPage}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={updatePageData}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
      <div className="ptb-2">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <div className="Search_box">
              <Row gutter={[10, 10]}>
                <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                  <Input placeholder="Search ..." onChange={handleChange} />
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select
                    defaultValue="Sort by Layout"
                    onChange={handleChangeDropDown}
                    style={{ width: '100%' }}>
                    <OptGroup label="Sort by ">
                      <Option value="1">Sort by A to Z</Option>
                      <Option value="-1">Sort by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                {appId != '' && !moduleId ? (
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Select Module"
                      onChange={handleChangeStatus}
                      style={{ width: '100%' }}>
                      <OptGroup label="Select Module">
                        <Option value="all">All</Option>
                        {dropDownData.map((item, index) => (
                          <Option key={index} value={item._id}>
                            {item.name}
                          </Option>
                        ))}
                      </OptGroup>
                    </Select>
                  </Col>
                ) : (
                  ''
                )}
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <div className="datepicker">
                    <DatePicker
                      placeholder="Last Date"
                      format="MM/DD/YYYY"
                      // style={{boxShadow: '0px 2px 10px rgb(0 0 0 / 10%)'}}
                      onChange={onChange}></DatePicker>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
          <Col xl={2} lg={4} md={24} sm={24} xs={24}>
            <div>
              {appId == '' ? (
                <div>
                  <Button className="ext-btn" onClick={showModalAddPage}>
                    Add Page
                  </Button>
                </div>
              ) : (
                ''
              )}
            </div>
          </Col>
          <Col xl={2} lg={4} md={24} sm={24} xs={24}>
            {appId != '' && !moduleId ? (
              <ViewBoxes
                grid="inactive"
                list="active"
                gridlink={`/app/${appId}/pages/grid`}
                listlink={`/app/${appId}/pages`}
                //data={state}
              />
            ) : (
              <ViewBoxes
                grid="inactive"
                list="active"
                gridlink={`/app/${appId}/module/${moduleId}/grid`}
                listlink={`/app/${appId}/module/${moduleId}`}
                //data={state}
              />
            )}
          </Col>
        </Row>
      </div>
      <div>
        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          pagination={{
            defaultPageSize: 4,
            showSizeChanger: true,
            pageSizeOptions: ['10', '20', '30']
          }}
          columns={columns}
          dataSource={cart}
        />
      </div>
    </>
  );
};

export default TableList;

TableList.propTypes = {
  getPageCount: PropTypes.func,
  pageList: PropTypes.func,
  modules: PropTypes.array,
  updatePageData: PropTypes.func,
  deleteFunctionCall: PropTypes.func
};
