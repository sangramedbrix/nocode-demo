import React from 'react';
import { Row, Col } from 'antd';
import SearchBox from '../../components/Dropdowns/SearchBox';
import ViewBoxes from '../../components/ViewBoxes/Index';
import TableList from './TableList';

const MyApps = () => {
  return (
    <>
      <div className="select_theam_search_box mb-5">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <SearchBox placeholder="Search ..." />
          </Col>
          <Col xl={4} lg={4} md={24} sm={24} xs={24}>
            <ViewBoxes />
          </Col>
        </Row>
      </div>

      <div className="">
        <TableList />
      </div>
    </>
  );
};

export default MyApps;
