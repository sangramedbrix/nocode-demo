import React from 'react';
import RecentActiviesa from './RecentActivities';
import MyApps from './MyApps';
import MyBlocks from '../AppList/BlockGrid';
import '../../assets/styles/index.css';
import { Tabs } from 'antd';
import { useNavigate } from 'react-router-dom';

const { TabPane } = Tabs;

const Components = () => {
  const navigate = useNavigate();
  function callback(key) {
    if (key == 1) {
      // navigate('/apps')
    } else if (key == 2) {
      navigate('/apps');
    }
  }
  return (
    <>
      <div className="containers bg-light-blue box-padding">
        <div className="recent_activitivites_list_div">
          <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="Recent Activities" key="1">
              <RecentActiviesa />
            </TabPane>
            <TabPane tab="My Apps" key="2">
              <MyApps />
            </TabPane>
            <TabPane tab="My Blocks" key="3">
              <MyBlocks />
            </TabPane>
          </Tabs>
        </div>
      </div>
    </>
  );
};

export default Components;
