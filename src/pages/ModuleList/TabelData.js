import React, { useState } from 'react';
// import Table from 'ant-responsive-table';
import { Table, Row, Col } from 'antd';
import app_details_img from '../../assets/images/app_details_img.png';

const TableData = () => {
  const columns = [
    // {
    //     title: 'APP Details',
    //     dataIndex: 'app_details',
    //     render: (text) => <img src={app_details_img} alt='' />,
    // },
    {
      title: 'APP Details',
      dataIndex: 'app_details',
      width: '28%',
      render: () => (
        <div className="app_details_box">
          <Row>
            <Col xl={4} lg={4} md={15} sm={24} xm={24}>
              <span className="app_details_box_img_div">
                <img src={app_details_img} alt="" />
              </span>
            </Col>
            <Col xl={20} lg={20} md={24} sm={24} xm={24}>
              <span className="app_details_box_text_div">
                <span className="app_details_title">Student Management Systems</span>
                <span className="app_details_sub_title">Visibility: Public</span>
              </span>
            </Col>
          </Row>
        </div>
      )
    },
    {
      title: 'Module',
      dataIndex: 'module',
      width: '10%'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      width: '11%',
      render: (state) => <span className="inprogress">{state}</span>
    },
    {
      title: 'Comments',
      dataIndex: 'comments',
      width: '25%',
      render: (comments) => <span className="comments">{comments}</span>
    },
    {
      title: 'Last Modified Date',
      dataIndex: 'last_date_date',
      render: (last_date_date) => <span className="last_date_date">{last_date_date}</span>
    },
    {
      title: ' ',
      dataIndex: 'action',
      render: () => (
        <span className="app_details_action_div">
          {/* <EyeOutlined /> */}
          <i className="icon_view"></i>

          {/* <FormOutlined /> */}
          <i className="icon_edit"></i>

          {/* <DeleteOutlined /> */}
          <i className="icon_delete"></i>
        </span>
      )
    }
  ];
  const data = [
    {
      key: '1',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '2',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'In Progress',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '3',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Pending',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '4',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '5',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '6',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name
    })
  };

  const [selectionType] = useState('checkbox');
  return (
    <>
      <div>
        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          columns={columns}
          dataSource={data}
        />
      </div>
    </>
  );
};

export default TableData;
