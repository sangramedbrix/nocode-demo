import React from 'react';
import { Row, Col } from 'antd';
import Tile1 from '../../components/Subheader/Tile1';
import Tile2 from '../../components/Subheader/Tile2';
import Tile3 from '../../components/Subheader/Tile3';
import Tile4 from '../../components/Subheader/Tile4';
import ButtonList from '../AppCreated/ButtonList';
import ViewBoxes from '../../components/ViewBoxes/Index';
import TableData from './TabelData';
import { useLocation } from 'react-router-dom';
import '../../assets/styles/index.css';

const AppComponents = () => {
  const { state } = useLocation();
  console.log('state data', state);

  return (
    <>
      <div className="module_list">
        <div className="sub_header">
          <Row>
            <Col xl={7} lg={7} md={12} sm={12} xs={24}>
              <Tile1 />
            </Col>
            <Col xl={6} lg={6} md={12} sm={12} xs={24}>
              <Tile2 />
            </Col>
            <Col xl={6} lg={5} md={12} sm={12} xs={24}>
              <Tile3 />
            </Col>
            <Col xl={5} lg={6} md={12} sm={12} xs={24}>
              <Tile4 />
            </Col>
          </Row>
        </div>

        <div className="ptb-2">
          <Row>
            <Col xl={20} lg={20} md={24} sm={24} xs={24}>
              <ButtonList />
            </Col>
            <Col xl={4} lg={4} md={24} sm={24} xs={24}>
              <ViewBoxes
                grid="inactive"
                list="active"
                listlink="/module/list"
                gridlink="/app/modules"
                data={state}
              />
            </Col>
          </Row>
        </div>

        <div>
          <TableData />
        </div>
      </div>
    </>
  );
};

export default AppComponents;
