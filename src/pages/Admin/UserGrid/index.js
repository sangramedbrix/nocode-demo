import React from 'react';
import Headers from '../../../components/Header';
import SideMenuComponent from '../../../components/Sidemenu/component';
import AppComponents from './components';
import { Layout } from 'antd';
import '../../../assets/styles/index.css';

const { Sider, Content } = Layout;

const AdminUserGrid = () => {
  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent />
          </Sider>
          <Content>
            <AppComponents />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default AdminUserGrid;
