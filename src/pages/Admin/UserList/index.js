import React from 'react';
import Headers from '../../../components/Header';
import SideMenuComponent from '../../../components/Sidemenu/component';
import AppComponents from './components';
import { Layout } from 'antd';
import '../../../assets/styles/index.css';
// import Approve from '../../../components/Popup/ApprovePopup';

const { Sider, Content } = Layout;

const AdminUserList = () => {
  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent />
          </Sider>
          <Content>
            {/* <Approve
              popup_description="If you cancel this page, you will lose unsaved data and also
not saved in your automatic generated version."
            /> */}
            <AppComponents />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default AdminUserList;
