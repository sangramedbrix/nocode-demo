import React from 'react';
import { Table, Tooltip } from 'antd';
import img from '../../../assets/images/defult_user_img.png';
const columns = [
  {
    title: 'User Profile',
    dataIndex: 'user_profile',
    width: '20%',
    render: (text) => (
      <div className="item-center app_details_box">
        <div className="user_profile_img ">
          <img src={img} />
        </div>
        <div className="app_details_box_text_div ml-1">
          <div className="user_profile_title">{text}</div>
        </div>
      </div>
    )
  },
  {
    title: 'Role',
    dataIndex: 'role',
    width: '7%',
    render: (role) => <div className="role"> {role} </div>
  },
  {
    title: 'Stetus',
    dataIndex: 'stetus',
    width: '8%',
    render: (stetus) => <div className="status_active"> {stetus} </div>
  },
  {
    title: 'Last Activity',
    dataIndex: 'last_activity',
    width: '20%',
    render: (last_activity) => <div className="last_activity"> {last_activity} </div>
  },
  {
    title: 'Last Seen',
    dataIndex: 'last_seen',
    width: '15%',
    render: (last_seen) => <div className="last_seen"> {last_seen} </div>
  },
  {
    title: 'App',
    dataIndex: 'app',
    width: '3%',
    render: (app) => <div className="app"> {app} </div>
  },
  {
    title: 'Module',
    dataIndex: 'module',
    width: '3%',
    render: (app) => <div className="app"> {app} </div>
  },
  {
    title: 'Page',
    dataIndex: 'page',
    width: '3%',
    render: (app) => <div className="app"> {app} </div>
  },
  {
    title: 'Comments',
    dataIndex: 'comments',
    width: '3%',
    render: (app) => <div className="app"> {app} </div>
  },
  {
    title: '',
    dataIndex: '',
    width: '10%',
    render: () => (
      <div className="app_details_action_div">
        <Tooltip title="View">
          {/* <EyeOutlined /> */}
          <i className="icon_view"></i>
        </Tooltip>
        <Tooltip title="Edit">
          {/* <FormOutlined /> */}
          <i className="icon_edit"></i>
        </Tooltip>
        <Tooltip title="Delete">
          {/* <DeleteOutlined /> */}
          <i className="icon_delete"></i>
        </Tooltip>
      </div>
    )
  }
];
const data = [
  {
    key: '1',
    user_profile: 'Leslie Alexander',
    role: 'Admin',
    stetus: 'Active',
    last_activity: 'New page created in Student Information APP',
    last_seen: '7th December, 2021',
    app: 32,
    module: 32,
    page: 32,
    comments: 32
  },
  {
    key: '1',
    user_profile: 'Leslie Alexander',
    role: 'Admin',
    stetus: 'Active',
    last_activity: 'New page created in Student Information APP',
    last_seen: '7th December, 2021',
    app: 32,
    module: 32,
    page: 32,
    comments: 32
  },
  {
    key: '1',
    user_profile: 'Leslie Alexander',
    role: 'Admin',
    stetus: 'Active',
    last_activity: 'New page created in Student Information APP',
    last_seen: '7th December, 2021',
    app: 32,
    module: 32,
    page: 32,
    comments: 32
  },
  {
    key: '1',
    user_profile: 'Leslie Alexander',
    role: 'Admin',
    stetus: 'Active',
    last_activity: 'New page created in Student Information APP',
    last_seen: '7th December, 2021',
    app: 32,
    module: 32,
    page: 32,
    comments: 32
  }
];

const AdminListTable = () => {
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name
    })
  };
  return (
    <>
      <div className="user_admin_list">
        <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
      </div>
    </>
  );
};

export default AdminListTable;
