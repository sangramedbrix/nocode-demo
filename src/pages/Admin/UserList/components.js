import React from 'react';
import { Row, Col, Select, Input, Button } from 'antd';
import UserCount from '../../../components/Admin/UserCount';
import ViewBoxes from '../../../components/ViewBoxes/Index';
import '../../../index.css';
import AdminListTable from './AdminListTable';
// import UserCard from '../../../components/Admin/UserCard';

const { Option } = Select;

const AppComponents = () => {
  return (
    <>
      <div className="user_grid box-padding bg-pink">
        <div className="d-flex mb-10">
          <p className="management"> User Management</p>
          <Button className="create_new_user" size="small">
            Create New User
          </Button>
        </div>

        <Row gutter={[20, 30]}>
          <Col className="user_count">
            <UserCount count="24" text="Total No. Of Users" bg_color="bg-green-circle" />
          </Col>
          <Col className="user_count">
            <UserCount count="16" text="No. Of Editors" bg_color="bg-purple-circle" />
          </Col>
          <Col className="user_count">
            <UserCount count="5" text="No. Of Data Analytics" bg_color="bg-blue-circle" />
          </Col>
          <Col className="user_count">
            <UserCount count="3" text="No. Of Admins" bg_color="bg-red-circle" />
          </Col>
          <Col className="user_count">
            <UserCount count="1" text="No. Of Super Admin" bg_color="bg-purple-circle" />
          </Col>
        </Row>

        <div className="select_theam_search_box mtb-3">
          <Row>
            <Col xl={21} lg={20} md={24} sm={24} xs={24}>
              <div className="Search_box">
                <Row gutter={[10, 10]}>
                  <Col xl={7} lg={7} md={9} sm={9} xs={24}>
                    <Input placeholder="Search ..." />
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Sort by Role"
                      // onChange={handleChangeDropDown}
                      style={{ width: '100%' }}>
                      <Option value="1">Sort by Role </Option>
                      <Option value="-1">Sort by Role</Option>
                    </Select>
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select defaultValue="Status " style={{ width: '100%' }}>
                      <Option value="">Status</Option>
                      <Option value="">Status</Option>
                    </Select>
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select defaultValue="Last Activity" style={{ width: '100%' }}>
                      <Option value="">Last Activity</Option>
                      <Option value="">Last Activity</Option>
                    </Select>
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select defaultValue="Last Seen" style={{ width: '100%' }}>
                      <Option value="">Last Seen</Option>
                      <Option value="">Last Seen </Option>
                    </Select>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xl={3} lg={4} md={24} sm={24} xs={24}>
              <ViewBoxes grid="active" list="inactive" />
            </Col>
          </Row>
        </div>
        <div className="mt-5 admin_list">
          <AdminListTable />
        </div>
      </div>
    </>
  );
};

export default AppComponents;
