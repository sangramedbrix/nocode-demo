import React from 'react';
import RecentActivies from './RecentActivities';
import MyApps from './MyApps';
import MyBlocks from './MyBlocks';

import { Tabs } from 'antd';

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const Components = () => {
  return (
    <>
      <div className="recent_activitivites_list_div">
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="Recent Activies" key="1">
            <RecentActivies />
          </TabPane>
          <TabPane tab="My Apps" key="2">
            <MyApps />
          </TabPane>
          <TabPane tab="My Blocks" key="3">
            <MyBlocks />
          </TabPane>
        </Tabs>
      </div>
    </>
  );
};

export default Components;
