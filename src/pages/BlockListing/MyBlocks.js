import React from 'react';

import TableList from './TableList';

const MyBlocks = () => {
  return (
    <>
      <div>
        <TableList />
      </div>
    </>
  );
};

export default MyBlocks;
