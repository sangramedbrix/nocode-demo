import React, { useState } from 'react';
import { Row, Col, Input, Table, Select, DatePicker, Tooltip } from 'antd';
import app_details_img from '../../assets/images/app_details_img.png';
import ViewBoxes from '../../components/ViewBoxes/Index';

const { Option, OptGroup } = Select;
const TableList = () => {
  const columns = [
    // {
    //     title: 'APP Details',
    //     dataIndex: 'app_details',
    //     render: (text) => <img src={app_details_img} alt='' />,
    // },
    {
      title: 'APP Details',
      dataIndex: 'app_details',
      width: '28%',
      render: () => (
        <div className="app_details_box">
          <div className="app_details_box_img_div">
            <img src={app_details_img} alt="detailed img" />
          </div>
          <div className="app_details_box_text_div">
            <div className="app_details_title">Student Management Systems</div>
            <div className="app_details_sub_title">Visibility: Public</div>
          </div>
        </div>
      )
    },
    {
      title: 'Module',
      dataIndex: 'module',
      width: '10%'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      width: '11%',
      render: (state) => <div className="inprogress">{state}</div>
    },
    {
      title: 'Comments',
      dataIndex: 'comments',
      width: '25%',
      render: (comments) => <div className="comments">{comments}</div>
    },
    {
      title: 'Last Modified Date',
      dataIndex: 'last_date_date',
      render: (last_date_date) => <div className="last_date_date">{last_date_date}</div>
    },
    {
      title: ' ',
      dataIndex: 'action',
      render: () => (
        <div className="app_details_action_div">
          <Tooltip title="View">
            {/* <EyeOutlined /> */}
            <i className="icon_view"></i>
          </Tooltip>
          <Tooltip title="Edit">
            {/* <FormOutlined /> */}
            <i className="icon_edit"></i>
          </Tooltip>
          <Tooltip title="Delete">
            {/* <DeleteOutlined /> */}
            <i className="icon_delete"></i>
          </Tooltip>
        </div>
      )
    }
  ];
  const data = [
    {
      key: '1',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '2',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'In Progress',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '3',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Pending',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '4',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '5',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    },
    {
      key: '6',
      app_details: 'John Brown',
      module: 'Module Name',
      status: 'Approved',
      comments: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
      last_date_date: '7th December, 2021',
      action: ''
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name
    })
  };

  const [selectionType] = useState('checkbox');
  return (
    <>
      <div className="select_theam_search_box mb-5">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <div className="Search_box">
              <Row gutter={[10, 10]}>
                <Col xl={7} lg={8} md={9} sm={9} xs={24}>
                  <Input placeholder="Search ..." />
                </Col>
                <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                  <Select defaultValue="Sort by Layout" style={{ width: '100%' }}>
                    <OptGroup label="Sort by ">
                      <Option value="1">Sort by A to Z</Option>
                      <Option value="-1">Sort by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                  <Select defaultValue="Type" style={{ width: '100%' }}>
                    <OptGroup label="Filter by type">
                      <Option value="all">All</Option>
                      <Option value="1">Web</Option>
                      <Option value="2">Mobile</Option>
                    </OptGroup>
                  </Select>
                </Col>
                <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                  <Select defaultValue="Category" style={{ width: '100%' }}>
                    <OptGroup label="filter by category">
                      <Option value="all">All</Option>
                    </OptGroup>
                  </Select>
                </Col>
                <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                  <Select defaultValue="Status" style={{ width: '100%' }}>
                    <OptGroup label="Status">
                      <Option value="all">All</Option>
                      <Option value="1">Public</Option>
                      <Option value="0">Private</Option>
                    </OptGroup>
                  </Select>
                </Col>
                <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                  <div className="datepicker">
                    <DatePicker placeholder="Last Date" format="MM/DD/YYYY"></DatePicker>
                  </div>
                  {/* <Select defaultValue="Last Date" style={{ width: '100%' }}>
          <OptGroup label="Date ">
            {
              dates.map((date) => {
                return (
                  <Option value="">05 Jan 2022</Option>

                )
              })
            }
           
          </OptGroup>
        </Select> */}
                </Col>
              </Row>
            </div>
          </Col>
          <Col xl={4} lg={4} md={24} sm={24} xs={24}>
            <ViewBoxes grid="inactive" list="active" listlink="/blocklist" gridlink="/blockgrid" />
          </Col>
        </Row>
      </div>

      <div>
        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          columns={columns}
          dataSource={data}
        />
      </div>
    </>
  );
};

export default TableList;
