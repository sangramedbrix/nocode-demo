import React from 'react';

import TableList from './TableList';

const MyApps = () => {
  return (
    <>
      <div>
        <TableList />
      </div>
    </>
  );
};

export default MyApps;
