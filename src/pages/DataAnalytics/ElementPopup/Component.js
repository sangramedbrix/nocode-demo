import React, { useState } from 'react';
import { Tabs, Radio, Modal, Button } from 'antd';
import Design from './Design';
import { CloseOutlined } from '@ant-design/icons';
import './style.css';
const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const AppComponent = () => {
  // ********************************

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  // ********************************

  const [value, setValue] = React.useState(1);

  const onChange = (e) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };
  return (
    <>
      {/* ********** */}

      <div className="element-model">
        <Button type="primary" onClick={showModal}>
          Open Modal
        </Button>
        <Modal
          width="100%"
          style={{ top: 20 }}
          title="Selected Element “Input Filed: Last Name "
          className="element-model"
          footer={[
            <Button key="back" onClick={handleCancel} className="footer-btn-cancle">
              Cancel
            </Button>,
            <Button key="submit" onClick={handleOk} className="footer-btn-apply">
              Apply
            </Button>
          ]}
          visible={isModalVisible}
          closable={false}
          // onOk={handleOk}
          // onCancel={handleCancel}
        >
          <p style={{ transform: 'translateY(-240%)', textAlign: 'right' }}>
            <Button className="header-btn-close">
              Close
              <CloseOutlined />
            </Button>
          </p>
          <div className="pad-2 text-center" style={{ marginTop: '-45px' }}>
            <Radio.Group onChange={onChange} value={value}>
              <Tabs
                defaultActiveKey="1"
                onChange={callback}
                className="element-wrapper-container"
                centered>
                <TabPane tab={<Radio value={1}>Design</Radio>} key="1">
                  <Design />
                </TabPane>
                <TabPane tab={<Radio value={2}>Display</Radio>} key="2">
                  Content of Tab Pane 2
                </TabPane>
                <TabPane tab={<Radio value={3}>Data</Radio>} key="3">
                  Content of Tab Pane 3
                </TabPane>
                <TabPane tab={<Radio value={4}>Validation</Radio>} key="4">
                  Content of Tab Pane 4
                </TabPane>
                <TabPane tab={<Radio value={5}>API</Radio>} key="5">
                  Content of Tab Pane 5
                </TabPane>
                <TabPane tab={<Radio value={6}>Conditional</Radio>} key="6">
                  Content of Tab Pane 6
                </TabPane>
                <TabPane tab={<Radio value={7}>Logic</Radio>} key="7">
                  Content of Tab Pane 7
                </TabPane>
                <TabPane tab={<Radio value={8}>Layout</Radio>} key="8">
                  Content of Tab Pane 8
                </TabPane>
                <TabPane tab={<Radio value={9}>Custom CSS</Radio>} key="9">
                  Content of Tab Pane 9
                </TabPane>
              </Tabs>
            </Radio.Group>
          </div>
        </Modal>
      </div>

      {/* ********** */}
    </>
  );
};

export default AppComponent;
