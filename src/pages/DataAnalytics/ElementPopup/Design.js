import React from 'react';
import { Form, Input, Checkbox, Select, Col, Row } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';
const { Option } = Select;

const Design = () => {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  const onLabelPositionChange = (value) => {
    switch (value) {
      case 'top':
        form.setFieldsValue({
          note: 'Hi, man!'
        });
        return;

      case 'bottom':
        form.setFieldsValue({
          note: 'Hi, lady!'
        });
        return;
    }
  };
  function onChangeCheck(e) {
    console.log(`checked = ${e.target.checked}`);
  }

  return (
    <>
      <Form
        layout="vertical"
        name="basic"
        // labelCol={{
        //     span: 8
        // }}
        // wrapperCol={{
        //     span: 16
        // }}
        initialValues={{
          remember: true
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off">
        <Row gutter={[50, 16]}>
          <Col xs={24} sm={12} md={12} lg={8}>
            <Form.Item
              className="label"
              label="Label"
              name="label"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input name="neha" placeholder="Last Name" className="text-input-box" />
            </Form.Item>
            <Form.Item
              className="label"
              name="labelposition"
              label="Label Position"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Select
                placeholder="Top"
                onChange={onLabelPositionChange}
                allowClear
                className="dd-input-box">
                <Option value="male">Top</Option>
                <Option value="female">Bottom</Option>
              </Select>
            </Form.Item>
            <Form.Item
              className="label"
              name="Description"
              label="Description"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input.TextArea
                maxLength={100}
                placeholder="Description"
                className="text-input-box"
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8}>
            <Form.Item
              className="label"
              label="Placeholder"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input placeholder="Enter Your Last Name" className="text-input-box" />
            </Form.Item>
            <Form.Item
              className="label"
              label="Password"
              name="password"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input.Password className="text-input-box" />
            </Form.Item>
            <Form.Item
              className="label"
              label="Placeholder"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input placeholder="Enter Your Last Name" className="text-input-box" />
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8}>
            <Form.Item
              className="label"
              label="Label"
              name="username"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input placeholder="input placeholder" className="text-input-box" />
            </Form.Item>

            <Form.Item
              className="label"
              label="Password"
              name="password"
              tooltip={{
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />
              }}>
              <Input.Password className="text-input-box" />
            </Form.Item>
            <Form.Item>
              <Checkbox onChange={onChangeCheck}>Checkbox</Checkbox>
              <br />
              <Checkbox onChange={onChangeCheck}>Checkbox</Checkbox>
              <br />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default Design;
