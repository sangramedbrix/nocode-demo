import React from 'react';
// import DemoBar from './demobar';
// eslint-disable-next-line no-unused-vars
import FormBuilder, { Registry } from './index';
// import * as variables from './variables';

// Add our stylesheets for the demo.

// import '../../custom.scss'
import '../../scss/application.scss';
// require('../../../src/App');

const url = '/api/formdata';
const saveUrl = '/api/formdata';

const App = () => {
  return <FormBuilder.ReactFormBuilder url={url} saveUrl={saveUrl} />;
};

export default App;
