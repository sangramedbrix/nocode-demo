/**
 * <ReactFormBuilder />
 */

import React from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import Preview from './preview';
import Toolbar from './toolbar';
import ReactFormGenerator from './form';
import store from './stores/store';
import Registry from './stores/registry';
import PropTypes from 'prop-types';

const data = [
  {
    id: '380E1E27-D945-4881-9EE6-95691BE21EF0',
    element: 'DatePicker',
    text: 'Date',
    static: undefined,
    required: false,
    readOnly: false,
    defaultToday: false,
    canHavePageBreakBefore: true,
    canHaveAlternateForm: true,
    canHaveDisplayHorizontal: true,
    canHaveOptionCorrect: true,
    canHaveOptionValue: true,
    dateFormat: 'MM/dd/yyyy',
    timeFormat: 'hh:mm aa',
    showTimeSelect: false,
    showTimeSelectOnly: false,
    field_name: 'date_picker_E11A01FD-8B58-4C5B-AB35-7C3039863848',
    label: 'Placeholder Label'
  }
];

class ReactFormBuilder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      editElement: null
    };
    this.editModeOn = this.editModeOn.bind(this);
  }

  onLoad = () => {
    console.log(' Load From Data');
    return new Promise((resolve) => {
      resolve(data);
    });
  };

  editModeOn(data, e) {
    e.preventDefault();
    e.stopPropagation();
    if (this.state.editMode) {
      this.setState({ editMode: !this.state.editMode, editElement: null });
    } else {
      this.setState({ editMode: !this.state.editMode, editElement: data });
    }
  }

  manualEditModeOff() {
    if (this.state.editMode) {
      this.setState({
        editMode: false,
        editElement: null
      });
    }
  }

  render() {
    console.log('props data', this.props);
    const toolbarProps = {
      showDescription: this.props.show_description
    };
    if (this.props.toolbarItems) {
      toolbarProps.items = this.props.toolbarItems;
    }
    return (
      <>
        <DndProvider backend={HTML5Backend}>
          <div>
            <div className="react-form-builder clearfix">
              <div>
                <Preview
                  files={this.props.files}
                  manualEditModeOff={this.manualEditModeOff.bind(this)}
                  showCorrectColumn={this.props.showCorrectColumn}
                  parent={this}
                  data={this.props.data}
                  url={this.props.url}
                  saveUrl={this.props.saveUrl}
                  onLoad={this.onLoad}
                  onPost={this.props.onPost}
                  editModeOn={this.editModeOn}
                  editMode={this.state.editMode}
                  variables={this.props.variables}
                  registry={Registry}
                  editElement={this.state.editElement}
                  renderEditForm={this.props.renderEditForm}
                />
                <Toolbar {...toolbarProps} customItems={this.props.customToolbarItems} />
              </div>
            </div>
          </div>
        </DndProvider>
      </>
    );
  }
}

const FormBuilders = {};
FormBuilders.ReactFormBuilder = ReactFormBuilder;
FormBuilders.ReactFormGenerator = ReactFormGenerator;
FormBuilders.ElementStore = store;
FormBuilders.Registry = Registry;

export default FormBuilders;

export { ReactFormBuilder, ReactFormGenerator, store as ElementStore, Registry };

ReactFormBuilder.propTypes,
  ReactFormGenerator.propTypes,
  (Registry.propTypes = {
    show_description: PropTypes.string,
    toolbarItems: PropTypes.object,
    files: PropTypes.array,
    showCorrectColumn: PropTypes.string,
    data: PropTypes.array,
    url: PropTypes.string,
    saveUrl: PropTypes.string,
    onPost: PropTypes.string,
    variables: PropTypes.string,
    renderEditForm: PropTypes.string,
    customToolbarItems: PropTypes.string
  });
