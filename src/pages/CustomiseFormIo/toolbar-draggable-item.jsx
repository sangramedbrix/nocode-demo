/**
 * <ToolbarItem />
 */

import React from 'react';
import { DragSource } from 'react-dnd';
import PropTypes from 'prop-types';
// import { DndContext, DndProvider, DragPreviewImage, useDrag, useDragDropManager, useDragLayer, useDrop } from 'react-dnd';
import ItemTypes from './ItemTypes';
import ID from './UUID';
// import Layoutss from '../../assets/images/icons/layout-frames.png';

const cardSource = {
  beginDrag(props) {
    return {
      id: ID.uuid(),
      index: -1,
      data: props.data,
      onCreate: props.onCreate
    };
  }
};

class ToolbarItem extends React.Component {
  render() {
    const { connectDragSource, data, onClick } = this.props;
    if (!connectDragSource) return null;
    return connectDragSource(
      <div>
        <div className="editor-drawer-content">
          <div onClick={onClick}>
            <div className="icon_bg_clr">
              <i className={`item-center ${data.icon}`}></i>
            </div>
            <span className="tool-label">{data.name}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default DragSource(ItemTypes.CARD, cardSource, (connect) => ({
  connectDragSource: connect.dragSource()
}))(ToolbarItem);

ToolbarItem.propTypes = {
  items: PropTypes.array,
  showDescription: PropTypes.string,
  connectDragSource: PropTypes.func,
  data: PropTypes.object,
  onClick: PropTypes.func
};
