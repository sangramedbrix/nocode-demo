import React from 'react';
import HeaderBar from './header-bar';
import PropTypes from 'prop-types';

const ComponentHeader = (props) => {
  if (props.mutable) {
    return null;
  }
  return (
    <div>
      {props.data.pageBreakBefore && <div className="preview-page-break">Page Break</div>}
      <HeaderBar
        parent={props.parent}
        editModeOn={props.editModeOn}
        data={props.data}
        index={props.index}
        setAsChild={props.setAsChild}
        onDestroy={props._onDestroy}
        onEdit={props.onEdit}
        static={props.data.static}
        required={props.data.required}
      />
    </div>
  );
};

export default ComponentHeader;

ComponentHeader.propTypes = {
  mutable: PropTypes.array,
  data: PropTypes.array,
  parent: PropTypes.array,
  editModeOn: PropTypes.array,
  index: PropTypes.array,
  setAsChild: PropTypes.array,
  _onDestroy: PropTypes.array,
  onEdit: PropTypes.array
  // data.pageBreakBefore: PropTypes.func,
  // files: PropTypes.array,
  // showCorrectColumn: PropTypes.string,
};
