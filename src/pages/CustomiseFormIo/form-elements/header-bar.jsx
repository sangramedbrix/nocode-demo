/**
 * <HeaderBar />
 */

import React from 'react';
import Grip from '../multi-column/grip';
import PropTypes from 'prop-types';

export default class HeaderBar extends React.Component {
  render() {
    return (
      <div className="toolbar-header">
        <span className="badge badge-secondary">{this.props.data.text}</span>
        <div className="toolbar-header-buttons">
          {this.props.data.element !== 'LineBreak' && (
            <div
              className="btn is-isolated"
              onClick={this.props.editModeOn.bind(this.props.parent, this.props.data)}>
              <i className="is-isolated fa fa-edit"></i>
            </div>
          )}
          <div
            className="btn is-isolated"
            onClick={this.props.onDestroy.bind(this, this.props.data)}>
            <i className="is-isolated fa fa-trash"></i>
          </div>
          {!this.props.data.isContainer && (
            <Grip
              data={this.props.data}
              index={this.props.index}
              onDestroy={this.props.onDestroy}
              setAsChild={this.props.setAsChild}
            />
          )}
        </div>
      </div>
    );
  }
}

HeaderBar.propTypes = {
  data: PropTypes.array,
  editModeOn: PropTypes.func,
  parent: PropTypes.string,
  onDestroy: PropTypes.func,
  index: PropTypes.string,
  setAsChild: PropTypes.func
};
