import React from 'react';
import myxss from './myxss';
import PropTypes from 'prop-types';

const ComponentLabel = (props) => {
  const hasRequiredLabel =
    Object.prototype.hasOwnProperty.call(props.data, 'required') &&
    props.data.required === true &&
    !props.read_only;
  const labelText = myxss.process(props.data.label);
  return (
    <label className={props.className || ''}>
      <span dangerouslySetInnerHTML={{ __html: labelText }} />
      {hasRequiredLabel && <span className="label-required badge badge-danger">Required</span>}
    </label>
  );
};

export default ComponentLabel;

ComponentLabel.propTypes = {
  data: PropTypes.object,
  read_only: PropTypes.func,
  className: PropTypes.string
};
