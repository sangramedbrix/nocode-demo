import React from 'react';
import TextAreaAutosize from 'react-textarea-autosize';
import { ContentState, EditorState, convertFromHTML, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import { Editor } from 'react-draft-wysiwyg';

import DynamicOptionList from './dynamic-option-list';
import { get } from './stores/requests';
import ID from './UUID';
import PropTypes from 'prop-types';

const toolbar = {
  options: ['inline', 'list', 'textAlign', 'fontSize', 'link', 'history'],
  inline: {
    inDropdown: false,
    className: undefined,
    options: ['bold', 'italic', 'underline', 'superscript', 'subscript']
  }
};

export default class FormElementsEdit extends React.Component {
  constructor(props) {
    super(props);
    console.log('call');
    this.state = {
      element: this.props.element,
      data: this.props.data,
      dirty: false
    };
  }

  toggleRequired() {
    // const this_element = this.state.element;
  }

  editElementProp(elemProperty, targProperty, e) {
    // elemProperty could be content or label
    // targProperty could be value or checked
    const this_element = this.state.element;
    this_element[elemProperty] = e.target[targProperty];

    this.setState(
      {
        element: this_element,
        dirty: true
      },
      () => {
        if (targProperty === 'checked') {
          this.updateElement();
        }
      }
    );
  }

  onEditorStateChange(index, property, editorContent) {
    // const html = draftToHtml(convertToRaw(editorContent.getCurrentContent())).replace(/<p>/g, '<div>').replace(/<\/p>/g, '</div>');
    const html = draftToHtml(convertToRaw(editorContent.getCurrentContent()))
      .replace(/<p>/g, '')
      .replace(/<\/p>/g, '')
      .replace(/&nbsp;/g, ' ')
      .replace(/(?:\r\n|\r|\n)/g, ' ');
    const this_element = this.state.element;
    this_element[property] = html;

    this.setState({
      element: this_element,
      dirty: true
    });
  }

  updateElement() {
    const this_element = this.state.element;
    // to prevent ajax calls with no change
    if (this.state.dirty) {
      this.props.updateElement.call(this.props.preview, this_element);
      this.setState({ dirty: false });
    }
  }

  convertFromHTML(content) {
    const newContent = convertFromHTML(content);
    if (!newContent.contentBlocks || !newContent.contentBlocks.length) {
      // to prevent crash when no contents in editor
      return EditorState.createEmpty();
    }
    const contentState = ContentState.createFromBlockArray(newContent);
    return EditorState.createWithContent(contentState);
  }

  addOptions() {
    const optionsApiUrl = document.getElementById('optionsApiUrl').value;
    if (optionsApiUrl) {
      get(optionsApiUrl).then((data) => {
        this.props.element.options = [];
        const { options } = this.props.element;
        data.forEach((x) => {
          // eslint-disable-next-line no-param-reassign
          x.key = ID.uuid();
          options.push(x);
        });
        const this_element = this.state.element;
        this.setState({
          element: this_element,
          dirty: true
        });
      });
    }
  }

  componentDidMount() {
    console.log('edit data', this.state.data);
  }

  render() {
    if (this.state.dirty) {
      this.props.element.dirty = true;
    }

    const this_checked = Object.prototype.hasOwnProperty.call(this.props.element, 'required')
      ? this.props.element.required
      : false;
    const this_read_only = Object.prototype.hasOwnProperty.call(this.props.element, 'readOnly')
      ? this.props.element.readOnly
      : false;
    const this_default_today = Object.prototype.hasOwnProperty.call(
      this.props.element,
      'defaultToday'
    )
      ? this.props.element.defaultToday
      : false;
    const this_show_time_select = Object.prototype.hasOwnProperty.call(
      this.props.element,
      'showTimeSelect'
    )
      ? this.props.element.showTimeSelect
      : false;
    const this_show_time_select_only = Object.prototype.hasOwnProperty.call(
      this.props.element,
      'showTimeSelectOnly'
    )
      ? this.props.element.showTimeSelectOnly
      : false;
    const this_checked_inline = Object.prototype.hasOwnProperty.call(this.props.element, 'inline')
      ? this.props.element.inline
      : false;
    const this_checked_bold = Object.prototype.hasOwnProperty.call(this.props.element, 'bold')
      ? this.props.element.bold
      : false;
    const this_checked_italic = Object.prototype.hasOwnProperty.call(this.props.element, 'italic')
      ? this.props.element.italic
      : false;
    const this_checked_center = Object.prototype.hasOwnProperty.call(this.props.element, 'center')
      ? this.props.element.center
      : false;
    const this_checked_page_break = Object.prototype.hasOwnProperty.call(
      this.props.element,
      'pageBreakBefore'
    )
      ? this.props.element.pageBreakBefore
      : false;
    const this_checked_alternate_form = Object.prototype.hasOwnProperty.call(
      this.props.element,
      'alternateForm'
    )
      ? this.props.element.alternateForm
      : false;

    const {
      canHavePageBreakBefore,
      canHaveAlternateForm,
      canHaveDisplayHorizontal,
      canHaveOptionCorrect,
      canHaveOptionValue
    } = this.props.element;

    const this_files = this.props.files.length ? this.props.files : [];
    if (this_files.length < 1 || (this_files.length > 0 && this_files[0].id !== '')) {
      this_files.unshift({ id: '', file_name: '' });
    }

    let editorState;

    if (Object.prototype.hasOwnProperty.call(this.props.element, 'content')) {
      editorState = this.convertFromHTML(this.props.element.content);
    }
    if (Object.prototype.hasOwnProperty.call(this.props.element, 'label')) {
      editorState = this.convertFromHTML(this.props.element.label);
    }

    return (
      <div>
        <div className="clearfix">
          <h4 className="float-left">{this.props.element.text}</h4>
          <i
            className="float-right fa fa-times dismiss-edit"
            onClick={this.props.manualEditModeOff}></i>
        </div>
        {Object.prototype.hasOwnProperty.call(this.props.element, 'content') && (
          <div className="form-group">
            <label className="control-label">Text to display:</label>

            <Editor
              toolbar={toolbar}
              defaultEditorState={editorState}
              onBlur={this.updateElement.bind(this)}
              onEditorStateChange={this.onEditorStateChange.bind(this, 0, 'content')}
              stripPastedStyles={true}
            />
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'file_path') && (
          <div className="form-group">
            <label className="control-label" htmlFor="fileSelect">
              Choose file:
            </label>
            <select
              id="fileSelect"
              className="form-control"
              defaultValue={this.props.element.file_path}
              onBlur={this.updateElement.bind(this)}
              onChange={this.editElementProp.bind(this, 'file_path', 'value')}>
              {this_files.map((file) => {
                const this_key = `file_${file.id}`;
                return (
                  <option value={file.id} key={this_key}>
                    {file.file_name}
                  </option>
                );
              })}
            </select>
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'href') && (
          <div className="form-group">
            <TextAreaAutosize
              type="text"
              className="form-control"
              defaultValue={this.props.element.href}
              onBlur={this.updateElement.bind(this)}
              onChange={this.editElementProp.bind(this, 'href', 'value')}
            />
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'src') && (
          <div>
            <div className="form-group">
              <label className="control-label" htmlFor="srcInput">
                Link to:
              </label>
              <input
                id="srcInput"
                type="text"
                className="form-control"
                defaultValue={this.props.element.src}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'src', 'value')}
              />
            </div>
            <div className="form-group">
              <div className="custom-control custom-checkbox">
                <input
                  id="do-center"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_checked_center}
                  value={true}
                  onChange={this.editElementProp.bind(this, 'center', 'checked')}
                />
                <label className="custom-control-label" htmlFor="do-center">
                  Center?
                </label>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-3">
                <label className="control-label" htmlFor="elementWidth">
                  Width:
                </label>
                <input
                  id="elementWidth"
                  type="text"
                  className="form-control"
                  defaultValue={this.props.element.width}
                  onBlur={this.updateElement.bind(this)}
                  onChange={this.editElementProp.bind(this, 'width', 'value')}
                />
              </div>
              <div className="col-sm-3">
                <label className="control-label" htmlFor="elementHeight">
                  Height:
                </label>
                <input
                  id="elementHeight"
                  type="text"
                  className="form-control"
                  defaultValue={this.props.element.height}
                  onBlur={this.updateElement.bind(this)}
                  onChange={this.editElementProp.bind(this, 'height', 'value')}
                />
              </div>
            </div>
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'label') && (
          <div className="form-group">
            <label>Display Label</label>
            <Editor
              toolbar={toolbar}
              defaultEditorState={editorState}
              onBlur={this.updateElement.bind(this)}
              onEditorStateChange={this.onEditorStateChange.bind(this, 0, 'label')}
              stripPastedStyles={true}
            />
            <br />
            <div className="custom-control custom-checkbox">
              <input
                id="is-required"
                className="custom-control-input"
                type="checkbox"
                checked={this_checked}
                value={true}
                onChange={this.editElementProp.bind(this, 'required', 'checked')}
              />
              <label className="custom-control-label" htmlFor="is-required">
                Required
              </label>
            </div>
            {Object.prototype.hasOwnProperty.call(this.props.element, 'readOnly') && (
              <div className="custom-control custom-checkbox">
                <input
                  id="is-read-only"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_read_only}
                  value={true}
                  onChange={this.editElementProp.bind(this, 'readOnly', 'checked')}
                />
                <label className="custom-control-label" htmlFor="is-read-only">
                  Read only
                </label>
              </div>
            )}
            {Object.prototype.hasOwnProperty.call(this.props.element, 'defaultToday') && (
              <div className="custom-control custom-checkbox">
                <input
                  id="is-default-to-today"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_default_today}
                  value={true}
                  onChange={this.editElementProp.bind(this, 'defaultToday', 'checked')}
                />
                <label className="custom-control-label" htmlFor="is-default-to-today">
                  Default to Today?
                </label>
              </div>
            )}
            {Object.prototype.hasOwnProperty.call(this.props.element, 'showTimeSelect') && (
              <div className="custom-control custom-checkbox">
                <input
                  id="show-time-select"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_show_time_select}
                  value={true}
                  onChange={this.editElementProp.bind(this, 'showTimeSelect', 'checked')}
                />
                <label className="custom-control-label" htmlFor="show-time-select">
                  Show Time Select?
                </label>
              </div>
            )}
            {this_show_time_select &&
              Object.prototype.hasOwnProperty.call(this.props.element, 'showTimeSelectOnly') && (
                <div className="custom-control custom-checkbox">
                  <input
                    id="show-time-select-only"
                    className="custom-control-input"
                    type="checkbox"
                    checked={this_show_time_select_only}
                    value={true}
                    onChange={this.editElementProp.bind(this, 'showTimeSelectOnly', 'checked')}
                  />
                  <label className="custom-control-label" htmlFor="show-time-select-only">
                    Show Time Select Only?
                  </label>
                </div>
              )}
            {(this.state.element.element === 'RadioButtons' ||
              this.state.element.element === 'Checkboxes') &&
              canHaveDisplayHorizontal && (
                <div className="custom-control custom-checkbox">
                  <input
                    id="display-horizontal"
                    className="custom-control-input"
                    type="checkbox"
                    checked={this_checked_inline}
                    value={true}
                    onChange={this.editElementProp.bind(this, 'inline', 'checked')}
                  />
                  <label className="custom-control-label" htmlFor="display-horizontal">
                    Display horizonal
                  </label>
                </div>
              )}
          </div>
        )}

        {this.state.element.element === 'Signature' && this.props.element.readOnly ? (
          <div className="form-group">
            <label className="control-label" htmlFor="variableKey">
              Variable Key:
            </label>
            <input
              id="variableKey"
              type="text"
              className="form-control"
              defaultValue={this.props.element.variableKey}
              onBlur={this.updateElement.bind(this)}
              onChange={this.editElementProp.bind(this, 'variableKey', 'value')}
            />
            <p className="help-block">
              This will give the element a key that can be used to replace the content with a
              runtime value.
            </p>
          </div>
        ) : (
          <div />
        )}

        {canHavePageBreakBefore && (
          <div className="form-group">
            <label className="control-label">Print Options</label>
            <div className="custom-control custom-checkbox">
              <input
                id="page-break-before-element"
                className="custom-control-input"
                type="checkbox"
                checked={this_checked_page_break}
                value={true}
                onChange={this.editElementProp.bind(this, 'pageBreakBefore', 'checked')}
              />
              <label className="custom-control-label" htmlFor="page-break-before-element">
                Page Break Before Element?
              </label>
            </div>
          </div>
        )}

        {canHaveAlternateForm && (
          <div className="form-group">
            <label className="control-label">Alternate/Signature Page</label>
            <div className="custom-control custom-checkbox">
              <input
                id="display-on-alternate"
                className="custom-control-input"
                type="checkbox"
                checked={this_checked_alternate_form}
                value={true}
                onChange={this.editElementProp.bind(this, 'alternateForm', 'checked')}
              />
              <label className="custom-control-label" htmlFor="display-on-alternate">
                Display on alternate/signature Page?
              </label>
            </div>
          </div>
        )}

        {Object.prototype.hasOwnProperty.call(this.props.element, 'step') && (
          <div className="form-group">
            <div className="form-group-range">
              <label className="control-label" htmlFor="rangeStep">
                Step
              </label>
              <input
                id="rangeStep"
                type="number"
                className="form-control"
                defaultValue={this.props.element.step}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'step', 'value')}
              />
            </div>
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'min_value') && (
          <div className="form-group">
            <div className="form-group-range">
              <label className="control-label" htmlFor="rangeMin">
                Min
              </label>
              <input
                id="rangeMin"
                type="number"
                className="form-control"
                defaultValue={this.props.element.min_value}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'min_value', 'value')}
              />
              <input
                type="text"
                className="form-control"
                defaultValue={this.props.element.min_label}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'min_label', 'value')}
              />
            </div>
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'max_value') && (
          <div className="form-group">
            <div className="form-group-range">
              <label className="control-label" htmlFor="rangeMax">
                Max
              </label>
              <input
                id="rangeMax"
                type="number"
                className="form-control"
                defaultValue={this.props.element.max_value}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'max_value', 'value')}
              />
              <input
                type="text"
                className="form-control"
                defaultValue={this.props.element.max_label}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'max_label', 'value')}
              />
            </div>
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'default_value') && (
          <div className="form-group">
            <div className="form-group-range">
              <label className="control-label" htmlFor="defaultSelected">
                Default Selected
              </label>
              <input
                id="defaultSelected"
                type="number"
                className="form-control"
                defaultValue={this.props.element.default_value}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'default_value', 'value')}
              />
            </div>
          </div>
        )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'static') &&
          this.props.element.static && (
            <div className="form-group">
              <label className="control-label">Text Style</label>
              <div className="custom-control custom-checkbox">
                <input
                  id="do-bold"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_checked_bold}
                  value={true}
                  onChange={this.editElementProp.bind(this, 'bold', 'checked')}
                />
                <label className="custom-control-label" htmlFor="do-bold">
                  Bold
                </label>
              </div>
              <div className="custom-control custom-checkbox">
                <input
                  id="do-italic"
                  className="custom-control-input"
                  type="checkbox"
                  checked={this_checked_italic}
                  value={true}
                  onChange={this.editElementProp.bind(this, 'italic', 'checked')}
                />
                <label className="custom-control-label" htmlFor="do-italic">
                  Italic
                </label>
              </div>
            </div>
          )}
        {this.props.element.showDescription && (
          <div className="form-group">
            <label className="control-label" htmlFor="questionDescription">
              Description
            </label>
            <TextAreaAutosize
              type="text"
              className="form-control"
              id="questionDescription"
              defaultValue={this.props.element.description}
              onBlur={this.updateElement.bind(this)}
              onChange={this.editElementProp.bind(this, 'description', 'value')}
            />
          </div>
        )}
        {this.props.showCorrectColumn &&
          this.props.element.canHaveAnswer &&
          !Object.prototype.hasOwnProperty.call(this.props.element, 'options') && (
            <div className="form-group">
              <label className="control-label" htmlFor="correctAnswer">
                Correct Answer
              </label>
              <input
                id="correctAnswer"
                type="text"
                className="form-control"
                defaultValue={this.props.element.correct}
                onBlur={this.updateElement.bind(this)}
                onChange={this.editElementProp.bind(this, 'correct', 'value')}
              />
            </div>
          )}
        {this.props.element.canPopulateFromApi &&
          Object.prototype.hasOwnProperty.call(this.props.element, 'options') && (
            <div className="form-group">
              <label className="control-label" htmlFor="optionsApiUrl">
                Populate Options from API
              </label>
              <div className="row">
                <div className="col-sm-6">
                  <input
                    className="form-control"
                    style={{ width: '100%' }}
                    type="text"
                    id="optionsApiUrl"
                    placeholder="http://localhost:8080/api/optionsdata"
                  />
                </div>
                <div className="col-sm-6">
                  <button onClick={this.addOptions.bind(this)} className="btn btn-success">
                    Populate
                  </button>
                </div>
              </div>
            </div>
          )}
        {Object.prototype.hasOwnProperty.call(this.props.element, 'options') && (
          <DynamicOptionList
            showCorrectColumn={this.props.showCorrectColumn}
            canHaveOptionCorrect={canHaveOptionCorrect}
            canHaveOptionValue={canHaveOptionValue}
            data={this.props.preview.state.data}
            updateElement={this.props.updateElement}
            preview={this.props.preview}
            element={this.props.element}
            key={this.props.element.options.length}
          />
        )}
      </div>
    );
  }
}
FormElementsEdit.defaultProps = { className: 'edit-element-fields' };
FormElementsEdit.propTypes = {
  element: PropTypes.array,
  preview: PropTypes.func,
  data: PropTypes.array,
  updateElement: PropTypes.func,
  files: PropTypes.array,
  showCorrectColumn: PropTypes.string,
  manualEditModeOff: PropTypes.func
};
