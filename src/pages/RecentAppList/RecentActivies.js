import React, { useEffect, useState } from 'react';
import { Row, Col, Input, Select, DatePicker } from 'antd';
import AppsGridView from '../../components/Cards/AppsGridView';
import '../../config/config';
import ViewBoxes from '../../components/ViewBoxes/Index';
import axios from 'axios';
import moment from 'moment';
const { Option, OptGroup } = Select;

const RecentActivies = () => {
  const [apps, setApps] = useState([]);
  const [companyId, setCompanyId] = useState();
  const [category, setCatrgory] = useState([]);

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeStatusValue, setSearchTypeStatusValue] = useState('');
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState();
  const [searchTypeCategoryId, setSearchTypeCategoryId] = useState('');
  const [searchAppType, setSearchAppType] = useState('');

  const getApps = () => {
    axios
      .get(global.config.API_URL + '/appDashboard')
      .then((response) => {
        console.log('response apps data', response.data.data);
        setApps(response.data.data);
      })
      .catch((error) => console.log(error));
  };
  const getAppsCategory = () => {
    axios
      .get(global.config.API_URL + '/getAppCategory')
      .then((response) => {
        console.log('category response', response);
        setCatrgory(response.data.data);
        //  setCount(array)
      })
      .catch((error) => console.log(error));
  };

  const handleChange = (event) => {
    console.log('jnfgndfgfdg', event.target.value);
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());
    console.log(current);

    setSearchTypeNameValue(event.target.value);
    // setSearchTypeStatusValue('');
    setSearchTypeDateValue(current);
    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: event.target.value,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((res) => {
        console.log('response data', res);
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeDropDown = (event) => {
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());

    setSearchTypeSortValue(event);

    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: event,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((res) => {
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeType = (event) => {
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());

    setSearchAppType(event);

    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: event
      })
      .then((res) => {
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeCategory = (event) => {
    console.log(event);
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());

    setSearchTypeCategoryId(event);
    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: event,
        appType: searchAppType
      })
      .then((res) => {
        console.log(res);
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeStatus = (event) => {
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    console.log(followingDay.toLocaleDateString());
    setSearchTypeStatusValue(event);

    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: searchTypeDateValue,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: event,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((res) => {
        console.log(res);
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  function onChangeDate(value, dateString) {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
    setSearchTypeDateValue(dateString);
    axios
      .post(global.config.API_URL + '/getAppList', {
        searchTypeNameValue: searchTypeNameValue,
        searchTypeDateValue: dateString,
        searchTypeSortValue: searchTypeSortValue,
        searchByStatusValue: searchTypeStatusValue,
        searchByCategoryId: searchTypeCategoryId,
        appType: searchAppType
      })
      .then((responseData) => {
        console.log(responseData);
        setApps(responseData.data.data);
      })
      .catch((error) => console.log(error));
  }

  useEffect(() => {
    getApps();
    getAppsCategory();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setCompanyId(data[0].companyId);
  }, []);

  console.log('apps', apps);

  let cartData = [{}];
  let newData1 = JSON.stringify(apps);

  const valuesArray = JSON.parse(newData1);
  {
    valuesArray.map(function (item) {
      //call api
      axios
        .post(global.config.API_URL + '/getModuleCount', { appId: item._id })
        .then((countValue) => {
          setTimeout(function () {
            cartData.push(countValue.data);
          }, 5000);
        })
        .catch((error) => console.log(error));

      console.log('item', item._id);
    });
  }

  console.log('cartData', cartData);
  var counts = Object.keys(cartData).length;
  console.log('fdlgldfgfdi', counts);

  return (
    <>
      <div>
        {/* <Row gutter={5} className="recent_grid_card_div">
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
        </Row> */}
        <div className="select_theam_search_box mb-5">
          <Row>
            <Col xl={20} lg={20} md={24} sm={24} xs={24}>
              <div className="Search_box">
                <Row gutter={[10, 10]}>
                  <Col xl={7} lg={8} md={9} sm={9} xs={24}>
                    <Input placeholder="Search ..." onChange={handleChange} />
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Sort by Layout"
                      onChange={handleChangeDropDown}
                      style={{ width: '100%' }}>
                      <OptGroup label="Sort by ">
                        <Option value="1">Sort by A to Z</Option>
                        <Option value="-1">Sort by Z to A</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Type"
                      onChange={handleChangeType}
                      style={{ width: '100%' }}>
                      <OptGroup label="Filter by type">
                        <Option value="all">All</Option>
                        <Option value="1">Web</Option>
                        <Option value="2">Mobile</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Category"
                      onChange={handleChangeCategory}
                      style={{ width: '100%' }}>
                      <OptGroup label="filter by category">
                        <Option value="all">All</Option>
                        {category.map((cat, i) => {
                          return (
                            <Option value={cat._id} key={i}>
                              {cat.name}
                            </Option>
                          );
                        })}
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Status"
                      onChange={handleChangeStatus}
                      style={{ width: '100%' }}>
                      <OptGroup label="Status">
                        <Option value="all">All</Option>
                        <Option value="1">Public</Option>
                        <Option value="0">Private</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={3} lg={4} md={5} sm={5} xs={24}>
                    <div className="datepicker">
                      <DatePicker
                        placeholder="Last Date"
                        format="MM/DD/YYYY"
                        onChange={onChangeDate}></DatePicker>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xl={4} lg={4} md={24} sm={24} xs={24}>
              <ViewBoxes
                grid="active"
                list="inactive"
                listlink="/recentapp/list"
                gridlink="/apps"
                // data={state=""}
              />
            </Col>
          </Row>
        </div>
        <Row className="cardrow mtb-2 mb-5" gutter={[16, 25]}>
          {apps.map((app, i) => {
            return (
              <Col xl={6} lg={6} md={8} sm={12} xs={24} key={i}>
                {' '}
                <AppsGridView
                  app_grid_icon={
                    global.config.API_URL +
                    `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`
                  }
                  card_title={app.name}
                  app_grid_date={moment(app.createdAt).format('Do dddd, YYYY')}
                  status={`${app.isActive === 1 ? 'Public' : 'Private'}`}
                  footer_module="0 Modules"
                  footer_page="0 page"
                  footer_comments="0 Comments"
                  appData={app}
                  color={`${app.isActive === 1 ? 'green' : '#D2B555'}`}
                  getApps={getApps}
                />
              </Col>
            );
          })}
        </Row>
      </div>
    </>
  );
};

export default RecentActivies;
