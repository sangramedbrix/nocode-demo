import React from 'react';
import RecentActivies from './RecentActivies';
import AppList from './AppList';
import BlockGrid from './BlockGrid';
import { Tabs } from 'antd';
const { TabPane } = Tabs;
import { useNavigate } from 'react-router-dom';

const AppComponent = () => {
  const navigate = useNavigate();

  function callback(key) {
    if (key == 1) {
      // navigate('/apps')
    } else if (key == 2) {
      navigate('/apps');
    } else {
      navigate('/blockgrid');
    }
  }

  return (
    <>
      <div className="containers bg-light-blue box-padding">
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="Recent Activities" key="1">
            <RecentActivies />
          </TabPane>
          <TabPane tab="My Apps" key="2">
            <AppList />
          </TabPane>
          <TabPane tab="My Blocks" key="3">
            <BlockGrid />
          </TabPane>
        </Tabs>
      </div>
    </>
  );
};

export default AppComponent;
