import React from 'react';
import AppComponent from './component';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu';
import Headers from '../../components/Header';
import '../../assets/styles/index.css';

const { Sider, Content } = Layout;

const RecentActivitesGrid = () => {
  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent activeKey="3" />
          </Sider>
          <Content>
            <AppComponent />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default RecentActivitesGrid;
