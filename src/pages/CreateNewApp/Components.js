import React, { useState, useEffect } from 'react';
import '../../config/config';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import { Button } from 'antd';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import InputFile from '../../components/InputsFields/InputFile';
import InputCheckbox from '../../components/InputsFields/InputCheckbox';
import Dropdowns from '../../components/Dropdowns/Dropdowns';
import Buttons from '../../components/Buttons/Buttons';
import AppImageBanner from '../../assets/images/app_banner.png';
import axios from 'axios';

const CreateNewAppComponents = () => {
  const [appCategory, setAppCategory] = useState([]);
  const [createNewApp, setCreateNewApp] = useState({
    name: '',
    category: '',
    description: '',
    isActive: 0,
    file: [],
    nameValidation: '',
    categoryValidation: '',
    descriptionValidation: '',
    imageValidation: ''
  });

  const getAppCategory = () => {
    axios
      .get(global.config.API_URL + '/getAppCategory')
      .then((response) => {
        console.log(response);
        setAppCategory(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const nameChange = (name) => {
    console.log('app name', name);
    if (name != '') {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const categoryChange = (category) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      category: category,
      categoryValidation: false
    }));
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));
      console.log('empty');
    }
  };

  const changeStatus = (status) => {
    if (status === 1) {
      setCreateNewApp((prevState) => ({ ...prevState, isActive: status }));
    } else if (status === 0) {
      setCreateNewApp((prevState) => ({ ...prevState, isActive: status }));
    }
  };

  const selectFile = (file) => {
    console.log('file', file);
    setCreateNewApp((prevState) => ({ ...prevState, file: file, imageValidation: false }));
  };

  const onSubmit = () => {
    if (
      createNewApp.name != '' &&
      createNewApp.description != '' &&
      createNewApp.category != '' &&
      createNewApp.file.length != 0
    ) {
      const formData = new FormData();
      formData.append('image', createNewApp['file'].file.originFileObj);
      formData.append('name', createNewApp.name);
      formData.append('description', createNewApp.description);
      formData.append('category', createNewApp.category);
      formData.append('isActive', createNewApp.isActive);
      axios
        .post(global.config.API_URL + '/createNewApp', formData)
        .then((response) => {
          console.log(response);
          if (response.data.message === 'App created!') {
            setCreateNewApp((prevState) => ({
              ...prevState,
              name: '',
              description: '',
              category: ''
            }));
            // swal({
            //   title: 'Create New App',
            //   text: 'App created',
            //   icon: 'success',
            //   button: 'Ok'
            // });
          }
        })
        .catch((error) => console.log(error));
    } else {
      if (createNewApp.name == '') {
        setCreateNewApp((prevState) => ({ ...prevState, nameValidation: true }));
      } else if (createNewApp.category == '') {
        setCreateNewApp((prevState) => ({ ...prevState, categoryValidation: true }));
      } else if (createNewApp.description == '') {
        setCreateNewApp((prevState) => ({ ...prevState, descriptionValidation: true }));
      } else if (createNewApp.file.length == 0) {
        setCreateNewApp((prevState) => ({ ...prevState, imageValidation: true }));
      }
    }
  };

  useEffect(() => {
    getAppCategory();
  }, []);

  return (
    <>
      <div className="flex-container">
        <div className="containers">
          <div className="createApp-form-box m-auto">
            <div className="box-title">
              <TextHeadingH3
                className="headertext text-center"
                text_h3="Create your own app today"
              />
              <div className="subheadertxt text-center">No code App Maker to Build Your App</div>
            </div>
            <div className="mtb-2 bolder">
              <div className="createAppSubtxt">App Name *</div>
              <InputText
                placeholder_text="Enter App Name"
                value={createNewApp.name}
                nameChangeFunction={nameChange}
              />
              {createNewApp.nameValidation === true ? (
                <span style={{ color: 'red' }}>App name is required</span>
              ) : (
                ''
              )}
            </div>
            <div className="bolder">
              <div className="createAppSubtxt">Category *</div>
              <Dropdowns
                option={appCategory}
                dropdown_title="Select"
                value={createNewApp.category}
                categoryChangeFunction={categoryChange}
              />
              {createNewApp.categoryValidation === true ? (
                <span style={{ color: 'red' }}>Category is required</span>
              ) : (
                ''
              )}
              {/* <SelectDropdown option={appCategory} dropdown_title='Select' value={createNewApp.category} dropdwon_size='medium' categoryChangeFunction={categoryChange} style={{width:'100%' }}/>
                            {
                                createNewApp.categoryValidation === true 
                                ? <span style={{color:'red'}}>Category is required</span>
                                : ''
                            } */}
            </div>
            <div className="mtb-1 bolder">
              <div className="createAppSubtxt">Description</div>
              <InputTextArea
                placeholder_text=""
                discriptionChangeFunction={discriptionChange}
                value={createNewApp.description}
              />
              {createNewApp.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Description is required</span>
              ) : (
                ''
              )}
            </div>
            <div className="mtb-1 ">
              <div className="createAppSubtxt bolder">Status</div>
              <InputCheckbox
                check_text="Yes, I agree to show my App to public"
                statusCheck={changeStatus}
              />
            </div>
            <div className="mtb-2">
              <div className="createAppSubtxt">App Icon *</div>
              <div className="text-input-upload">
                <InputFile text="No choosen file" fileSelect={selectFile} />
                {createNewApp.imageValidation === true ? (
                  <span style={{ color: 'red' }}>App icon is required</span>
                ) : (
                  ''
                )}
              </div>
            </div>

            <div className="mtb-1 btn-div">
              <div className="app-link">
                <Buttons button_color="link" button_title="Cancel" />
              </div>
              <div className="app-button">
                <Button onClick={onSubmit}>Select Theme</Button>
              </div>
            </div>
          </div>
        </div>

        <div className="appimgcontainer">
          <div className="create-banner-text">
            <div className="banner-titletop">App Builder to build apps</div>
            <div className="banner-subtitletop">in five minutes without Code </div>
          </div>
          <div>
            <div className="side-img text-center">
              <img className="m-auto" width="70%" alt="example" src={AppImageBanner} />
            </div>
            <div className="create-banner-text">
              <div className="banner-title text-center">Design. Build. Launch</div>
              <div className="banner-subtitle">
                Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code Required!!!
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateNewAppComponents;
