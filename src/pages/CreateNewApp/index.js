import React from 'react';
import CreateNewAppComponents from './Components';
import '../../assets/styles/index.css';

const CreateNewApp = () => {
  return (
    <>
      <CreateNewAppComponents />
    </>
  );
};

export default CreateNewApp;
