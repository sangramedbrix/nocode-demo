import React, { useState, useEffect } from 'react';
import '../../config/config';
// import Table from 'ant-responsive-table';
import { Table, Modal, Button, Form } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import TextHeadingH4 from '../../components/TextHeadings/TextHeadingH4';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import img from '../../assets/images/app_details_img.png';
import '../../components/Popup/popup.css';
import moment from 'moment';

const TableList = (props) => {
  const { state } = useLocation();
  const [states, setstate] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisiblePage, setIsModalVisiblePage] = useState(false);
  const [pageId, setPageId] = useState();
  // const [app, setApp] = useState([]);
  const [pageName, setDeletePageName] = useState();

  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });

  console.log('state data', state);

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));

      console.log('empty');
    }
  };

  var getAppDetails = () => {
    if (state.from === 'app list') {
      axios
        .post(global.config.API_URL + '/getAppById', { appId: state.data._id })
        .then((response) => {
          console.log('response data isss', response);
          // setApp(response.data.data[0]);
        })
        .catch((error) => console.log(error));
    } else {
      axios
        .post(global.config.API_URL + '/getAppById', { appId: state.data.appId })
        .then((response) => {
          console.log('response data is', response);
          // setApp(response.data.data[0]);
        })
        .catch((error) => console.log(error));
    }
  };

  const getApps = () => {
    if (state.moduleId === 0) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: state.data._id,
          moduleId: 0
        })
        .then((response) => {
          console.log('response data in module id', response);
          setstate(response.data.data);
        })
        .catch((error) => console.log(error));
    } else if (state.appId === 0) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: 0,
          moduleId: state.data._id
        })
        .then((response) => {
          console.log('response data', response);
          setstate(response.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  useEffect(() => {
    getApps();
    getAppDetails();
    // const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    // setCompanyId(data[0].companyId);
    // deleteFunctionCall()
  }, []);

  let cart = [];
  let newData = JSON.stringify(states);

  const res = (item) => {
    return (
      <div className="app_details_box">
        <div className="app_details_box_img_div">
          <img src={img} />
          {/* <img
            src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
            alt=""
          /> */}
        </div>
        <div className="app_details_box_text_div">
          <div className="app_details_title">{item.moduleName}</div>
          <div className="app_details_sub_title">{item.moduleName}</div>
        </div>
      </div>
    );
  };

  function deleteModule(pageItem, pageName) {
    console.log('page id', pageItem);
    setPageId(pageItem);
    setDeletePageName(pageName);
    setIsModalVisible(true);
  }

  function updatePage(pageItem, pageName, pageDescription) {
    console.log('page id', pageItem, pageName);
    setPageId(pageItem);
    setCreatePage((prevState) => ({
      ...prevState,
      name: pageName,
      description: pageDescription,
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisiblePage(true);
  }

  const updatePageData = () => {
    console.log(pageId);

    axios
      .put(global.config.API_URL + `/updatePage/${pageId}`, {
        name: createPage.name,
        description: createPage.description
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisiblePage(false);
          getApps();
        }
      })
      .catch((error) => console.log(error));
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deletePage/${pageId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisible(false);
          getApps();
          props.getPageCount();
        }
      })
      .catch((error) => console.log(error));
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCancelPage = () => {
    setIsModalVisiblePage(false);
  };

  const deleteAction = (pageItem, pageName, pageDescription, pageNames) => {
    return (
      <div className="app_details_action_div">
        {/* <EyeOutlined /> */}
        <i className="icon_view"></i>
        {/* <DeleteOutlined onClick={() => deleteModule(pageItem, pageNames)} /> */}
        <i className="icon_delete" onClick={() => deleteModule(pageItem, pageNames)}></i>
        {/* <FormOutlined onClick={() => updatePage(pageItem, pageName, pageDescription)} /> */}
        <i
          className="icon_edit"
          onClick={() => updatePage(pageItem, pageName, pageDescription)}></i>
      </div>
    );
  };

  const lastDate = (updated_at) => {
    return <div className="last_date_date">{moment(updated_at).format('Do dddd, YYYY')}</div>;
  };

  const valuesArray = JSON.parse(newData);
  {
    valuesArray.map(function (item) {
      cart.push({
        key: item._id,
        app_details: res(item),
        module: item.pageName,
        status: 'Approved',
        comments: item.moduleDescription,
        last_date_date: lastDate(item.pageUpdateDate),
        action: deleteAction(item._id, item.moduleName, item.moduleDescription, item.moduleName)
      });
      console.log('id', item);
    });
  }

  console.log('PAGE LIST DATA', cart);
  const columns = [
    {
      title: 'Pages',
      dataIndex: 'app_details',
      width: '28%'
    },
    {
      title: 'Module',
      dataIndex: 'module',
      width: '10%'
    },
    // {
    //   title: 'Status',
    //   dataIndex: 'status',
    //   width: '11%',
    //   render: (state) => <div className="inprogress">{state}</div>
    // },
    {
      title: 'Comments',
      dataIndex: 'comments',
      width: '25%',
      render: (comments) => <div className="comments">{comments}</div>
    },
    {
      title: 'Last Modified Date',
      dataIndex: 'last_date_date',
      render: (last_date_date) => <div className="last_date_date">{last_date_date}</div>
    },
    {
      title: ' ',
      dataIndex: 'action'
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name
    })
  };

  const [selectionType] = useState('checkbox');
  return (
    <>
      {console.log('app', states)}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3
                className="text_align_center"
                text_h3={`You are about to delete a '${pageName}' page`}
              />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version..
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisiblePage}
          onOk={handleOk}
          onCancel={handleCancelPage}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Edit a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger" style={{ marginRight: '214px' }}>
                  {' '}
                  Page name is required
                </p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                value={createPage.description}
                discriptionChangeFunction={discriptionChange}
              />
              {createPage.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Description is required</span>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelPage}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={updatePageData}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      <div>
        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          columns={columns}
          dataSource={cart}
        />
      </div>
    </>
  );
};

export default TableList;

TableList.propTypes = {
  getPageCount: PropTypes.func
};
