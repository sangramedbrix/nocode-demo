import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import '../../config/config';
import Tile1 from '../../components/Subheader/Tile1';
import Tile2 from '../../components/Subheader/Tile2';
import Tile3 from '../../components/Subheader/Tile3';
import Tile6 from '../../components/Subheader/Tile6';
import { useLocation, useParams } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import { auth } from '../../authentication/auth';
import AppPageGrid from '../AppPageGrid';

const AppComponents = () => {
  const { appId, moduleId } = useParams();
  console.log('APPIS', appId);
  console.log('moduleId', moduleId);
  const [app, setApp] = useState([]);
  const [companyId, setCompanyId] = useState();
  const [moduleCount, setModuleCount] = useState();
  const [pageCount, setPageCount] = useState();
  const [modules, setModules] = useState([]);

  const { state } = useLocation();
  console.log('STATE====>>>>>>', state);
  const storage = JSON.parse(localStorage.getItem('isAuthenticated'));
  const username = storage[0].firstName + storage[0].lastName;

  var getAppDetails = () => {
    if (moduleId && appId) {
      auth.isValidAppModulePages(appId, moduleId);
    }
    axios
      .post(global.config.API_URL + '/getAppById', { appId: appId })
      .then((response) => {
        console.log('response data is', response);
        setApp(response.data.data[0]);
      })
      .catch((error) => console.log(error));
  };

  const getApps = () => {
    console.log('page List');
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: appId,
          moduleId: 0
        })
        .then((response) => {
          console.log('response data in module id', response);
          setModules(response.data.data);
          // setstate(response.data.data);
        })
        .catch((error) => console.log(error));
    } else if (appId != '' && moduleId != '') {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: 0,
          moduleId: moduleId
        })
        .then((response) => {
          console.log('response data', response);
          setModules(response.data.data);
          // setstate(response.data.data);
        })
        .catch((error) => console.log(error));
    }
  };

  var getModulesCount = () => {
    axios
      .post(global.config.API_URL + '/getModuleCount', { appId: appId })
      .then((res) => {
        console.log('response data call', res);
        if (res.data.data.length > 0) {
          setModuleCount(res.data.data[0].count);
        } else {
          setModuleCount(0);
        }
      })
      .catch((error) => console.log(error));
  };

  var getPageCount = () => {
    if (appId != '' && !moduleId) {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          moduleId: 0,
          appId: appId
        })
        .then((res) => {
          console.log('page count', res);
          setPageCount(res.data.data.length);
        })
        .catch((error) => console.log(error));
    } else {
      axios
        .post(global.config.API_URL + '/getAllPages', {
          appId: 0,
          moduleId: moduleId
        })
        .then((res) => {
          console.log('page count', res);
          setPageCount(res.data.data.length);
        })
        .catch((error) => console.log(error));
    }
  };

  const updatePageData = (pageId, createPage) => {
    axios
      .put(global.config.API_URL + `/updatePage/${pageId}`, {
        name: createPage.name,
        description: createPage.description
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          getApps();
        }
      })
      .catch((error) => console.log(error));
  };

  const deleteFunctionCall = (pageId) => {
    console.log('page id', pageId);
    axios
      .delete(global.config.API_URL + `/deletePage/${pageId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          // setIsModalVisible(false);
          getApps();
          // props.getPageCount();
        }
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    if (!appId && !moduleId) {
      window.location.href = '/apps';
    }
    getAppDetails();
    getModulesCount();
    getPageCount();
    getApps();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setCompanyId(data[0].companyId);
  }, []);

  return (
    <>
      <div className="app_page_list pad-3">
        <div className="sub_header">
          <Row>
            <Col xl={7} lg={7} md={12} sm={12} xs={24}>
              <Tile1
                appName={app.name}
                userName={username}
                appLogo={
                  global.config.API_URL +
                  `/uploads/company/${companyId}/app/${appId}/logo/${app.logoFileName}`
                }
                createdDate={moment(app.createdAt).format('Do dddd, YYYY')}
                moduleCount={moduleCount}
                pageCount={pageCount}
                appData={app}
              />
            </Col>
            <Col xl={6} lg={6} md={12} sm={12} xs={24}>
              <Tile2 />
            </Col>
            <Col xl={6} lg={5} md={12} sm={12} xs={24}>
              <Tile3 />
            </Col>
            <Col xl={5} lg={6} md={12} sm={12} xs={24}>
              <Tile6 moduleId={moduleId} pageList={getApps} />
            </Col>
          </Row>
        </div>

        <div className="grid">
          {/* <TableList getPageCount={getPageCount} /> */}
          <AppPageGrid
            getPageCount={getPageCount}
            pageList={getApps}
            modules={modules}
            updatePageData={updatePageData}
            deleteFunctionCall={deleteFunctionCall}
          />
        </div>
      </div>
    </>
  );
};

export default AppComponents;
