import React from 'react';
import Empty from '../../assets/images/icons/empty.png';

const EmptyBox = () => {
  return (
    <>
      <div className="empty-box">
        <div className="empty">
          <div className="empty-img">
            <img src={Empty} alt="empty" />
          </div>
          <div className="labels">
            {/* <div className='title'>Im still empty</div>, */}
            <div className="subtitle">First Create a Module and Get Started to Design a Page</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EmptyBox;
