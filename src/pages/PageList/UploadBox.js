import React from 'react';
import { PlusOutlined } from '@ant-design/icons';
import './style.css';
import CreateNewModal from '../../components/Popup/CreateModulePopup';

const UploadBox = () => {
  return (
    <div className="upload">
      <div className="upload-box">
        <div className="plus-icon">
          <PlusOutlined />
        </div>
        <div className="add-module">Add New Module</div>
      </div>
      <CreateNewModal />
    </div>
  );
};

export default UploadBox;
