import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import '../../config/config';
import CreateModulePopup from '../../components/Popup/CreateModulePopup';
import AddNewModule from '../../components/Cards/AddNewModule';
import Tile1 from '../../components/Subheader/Tile1';
import Tile2 from '../../components/Subheader/Tile2';
import Tile3 from '../../components/Subheader/Tile3';
import Tile4 from '../../components/Subheader/Tile4';
import ButtonList from './ButtonList';
import ViewBoxes from '../../components/ViewBoxes/Index';
import axios from 'axios';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu';
import Headers from '../../components/Header';
import '../../assets/styles/index.css';
const { Sider, Content } = Layout;

const AppCreated = () => {
  // const [modules, setModules] = useState([]);

  const getModules = () => {
    axios
      .get(global.config.API_URL + '/getModules')
      .then((response) => {
        console.log('response is', response);
        // setModules(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getModules();
  }, []);

  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent />
          </Sider>
          <Content>
            <div
              className="containers bg-light-blue"
              // style={{ overflowY: 'auto', zIndex: 1, height: '100vh' }}
            >
              <div className="app_created pad-3">
                <div className="subheader-container">
                  <Row>
                    <Col xl={7} lg={7} md={12} sm={12} xs={24}>
                      <Tile1 />
                    </Col>
                    <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                      <Tile2 />
                    </Col>
                    <Col xl={6} lg={5} md={12} sm={12} xs={24}>
                      <Tile3 />
                    </Col>
                    <Col xl={5} lg={6} md={12} sm={12} xs={24}>
                      <Tile4 />
                    </Col>
                  </Row>
                </div>

                <div className="ptb-2">
                  <Row>
                    <Col xl={20} lg={20} md={24} sm={24} xs={24}>
                      <ButtonList />
                    </Col>
                    <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                      <ViewBoxes />
                    </Col>
                  </Row>
                </div>

                <div className="ptb-1">
                  <div className="flex-container-grid">
                    {/* <Col xl={6} ><UploadBox /></Col> */}
                    <div>
                      <CreateModulePopup moduleName="Create New Page" getModulesData={getModules} />
                    </div>
                    {/* <Col xl={6} ><AddNewModule card_title='Student Information' card_date='7th December, 2021' /></Col> */}
                    {/* {
            modules.map((createdModule,i) => {
                return (
                  <>
                    <div><AddNewModule module_id={createdModule._id} card_title={createdModule.name} card_date={moment(createdModule.createdAt).format('Do dddd, YYYY')} card_description={createdModule.description} /></div>
                      
                  </>
                )
            })
        } */}

                    <AddNewModule
                      moduleName="Page List"
                      module_id="sfd"
                      card_title="dfd"
                      card_date="dsf"
                      card_description="dsd"
                    />
                    {/* <div><AddNewModule card_title='Student Information' card_date='7th December, 2021' /></div>
            <div><AddNewModule card_title='Student Information' card_date='7th December, 2021' /></div>
            <div><AddNewModule card_title='Student Information' card_date='7th December, 2021' /></div> */}
                    {/* <Col xl={12} ><EmptyBox /></Col> */}
                  </div>
                </div>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default AppCreated;
