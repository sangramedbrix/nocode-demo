import React from 'react';
import { Row, Col, Input, Select } from 'antd';

const { Option } = Select;

const ButtonList = () => {
  return (
    <>
      {/* <div className='Search_box'>
                <Row gutter={10}>
                    <Col sm={{ span: 24 }} xl={{ span: 6 }} ><Input placeholder='Search ...' /></Col>
                    <Col sm={{ span: 24 }} xl={{ span: 3 }} >
                        <Select defaultValue='Sort by ' style={{ width: '100%' }} >
                            <OptGroup label='Sort by '>
                                <Option value=''>Sort by A to Z</Option> 
                                <Option value=''>Sort by Z to A</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                    <Col sm={{ span: 24 }} xl={{ span: 3 }} >
                    <Select defaultValue='Status ' style={{ width: '100%' }} >
                            <OptGroup label='Status '>
                                <Option value=''>Done</Option> 
                                <Option value=''>In progress</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                    <Col sm={{ span: 24 }} xl={{ span: 3 }} >
                    <Select defaultValue='Comments' style={{ width: '100%' }} >
                            <OptGroup label='Comments '>
                                <Option value=''>Hello</Option> 
                                <Option value=''>Hello</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                    <Col sm={{ span: 24 }} xl={{ span: 3 }} >
                    <Select defaultValue='Last Date' style={{ width: '100%' }} >
                            <OptGroup label='Date '>
                                <Option value=''>05 Jan 2022</Option> 
                                <Option value=''>04 jan 2022</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                </Row>
            </div> */}

      <div className="app_create_Search_Box">
        <Row gutter={[10, 10]}>
          <Col xl={6} lg={8} md={8} sm={7} xs={24}>
            <Input placeholder="Search ..." />
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Type" style={{ width: '100%' }}>
              <Option value="">Type 1 </Option>
              <Option value="">Type 2 </Option>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Status" style={{ width: '100%' }}>
              <Option value="">Status 1</Option>
              <Option value="">Status 2</Option>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Comments" style={{ width: '100%' }}>
              <Option value="">Comments 1</Option>
              <Option value="">Comments 2</Option>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Last Date" style={{ width: '100%' }}>
              <Option value="">Last Date 1</Option>
              <Option value="">Last Date 2</Option>
            </Select>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default ButtonList;
