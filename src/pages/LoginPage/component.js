import React, { useState, useEffect } from 'react';
import { Button, Divider, Input, Alert } from 'antd';
import '../../config/config';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import LoginImageBanner from '../../assets/images/login_bg.png';
import InputCheckbox from '../../components/InputsFields/InputCheckbox';
import Google from '../../assets/images/icons/google.png';
import LogoSmall from '../../assets/images/logo.png';
import '../../assets/styles/index.css';

import axios from 'axios';

function AppComponent() {
  const [userData, setUserData] = useState({ username: '', password: '' });
  const [errorMessage, setErrorMessage] = useState({ value: '' });
  const [errorEmailMessage, setEmailErrorMessage] = useState({ value: '' });
  const [errorPasswordMessage, setPasswordErrorMessage] = useState({ value: '' });

  const onClose = (e) => {
    console.log(e, 'I was closed.');
  };

  let uname = localStorage.getItem('isAuthenticated');
  function isValid() {
    if (uname) {
      window.location.pathname = '/home';
    }
  }
  isValid();
  useEffect(() => {}, [isValid]);

  const handleInputChange = (e) => {
    setUserData((prevState) => {
      return {
        ...prevState,
        [e.target.name]: e.target.value
      };
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    //if username or password field is empty, return error message
    if (userData.username === '') {
      setEmailErrorMessage(() => ({
        value: 'This field is required'
      }));
      if (userData.password != '') {
        setPasswordErrorMessage(() => ({
          value: ''
        }));
      }
    }
    if (userData.password === '') {
      setPasswordErrorMessage(() => ({
        value: 'This field is required'
      }));

      if (userData.username == '') {
        // const reg = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
        // if (reg.test(userData.username) === true) {
        //   alert('true')
        //   setEmailErrorMessage(() => ({
        //     value: ''
        //   }));
        // } else {alert('false')
        setEmailErrorMessage(() => ({
          value: 'This field is required'
        }));
        // }
      } else {
        setEmailErrorMessage(() => ({
          value: ''
        }));
      }
    }
    if (userData.username === '' && userData.password === '') {
      setEmailErrorMessage(() => ({
        value: 'This field is required'
      }));
      setPasswordErrorMessage(() => ({
        value: 'This field is required'
      }));
    }
    if (userData.username != '' && userData.password != '') {
      axios
        .post(global.config.API_URL + '/login', {
          email: userData.username,
          password: userData.password
        })
        .then((response) => {
          localStorage.setItem('isAuthenticated', JSON.stringify(response.data.data));
          window.location.pathname = '/home';
        })
        .catch((e) => {
          console.log('test', e);
          if (userData.username == '') {
            setEmailErrorMessage(() => ({ value: 'This field is required' }));
          } else {
            setEmailErrorMessage(() => ({ value: '' }));
          }
          if (userData.password == '') {
            setPasswordErrorMessage(() => ({ value: 'This field is required' }));
          } else {
            setPasswordErrorMessage(() => ({ value: '' }));
          }
          // alert('error', e);
          setErrorMessage(() => ({ value: 'Invalid Email/Password' }));
        });
    } else {
      console.log('test');
      //setErrorMessage(() => ({ value: 'Invalid username/password' }));
    }
  };

  return (
    <>
      <div
        className="flex-container bg-white"
        style={{ overflowY: 'auto', zIndex: 1, height: 'auto' }}>
        <div className="containers">
          <div className="text-center">
            <img alt="logo_small" src={LogoSmall} />
          </div>
          <div className="login-form-box">
            <div className="login-title">Login</div>
            <div className="loginsubtitle">Provide your credentials to proceed</div>
            <div className="text-signin-box mtb-3">
              <div className="signin-text">
                <img src={Google} /> Sign in with Google
              </div>
            </div>
            <div className="mtb-2">
              <div className="text-center or-sign-in">OR</div>
              <Divider plain>
                <span className="or-sign-in"> Sign in with Email</span>
              </Divider>
            </div>
            <div className="mtb-1">
              <div>
                {errorMessage.value && (
                  <Alert message={errorMessage.value} type="error" closable onClose={onClose} />
                )}
              </div>
              <div className="label mtb-1">
                Email<span style={{ color: 'red', marginLeft: '2px' }}>*</span>
              </div>
              <Input
                className="text-input-box"
                placeholder="Email@website.com"
                name="username"
                onChange={(e) => handleInputChange(e)}
              />
              <span>
                {errorEmailMessage.value && (
                  <p className="text-danger"> {errorEmailMessage.value} </p>
                )}
              </span>
            </div>
            <div className="mtb-3">
              <div className="label mtb-1">
                Password<span style={{ color: 'red', marginLeft: '2px' }}>*</span>
              </div>
              <Input.Password
                className="text-input-box"
                placeholder=""
                name="password"
                onChange={(e) => handleInputChange(e)}
                iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
              />
              <span>
                {errorPasswordMessage.value && (
                  <p className="text-danger"> {errorPasswordMessage.value} </p>
                )}
              </span>
            </div>
            <div className="mtb-2" style={{ justifyContent: 'space-between', display: 'flex' }}>
              <div className="login-checkbox">
                <InputCheckbox className="remember-me" />
                <span className="label"> Remember Me</span>
              </div>
              <div>
                <Button type="link" block className="forgot-password">
                  Forget Password?
                </Button>
              </div>
            </div>
            <div className="mtb-1 mt-2">
              {/* <ButtonLogin block  button_title='Login' /> */}
              <Button
                className="login-button-text login-button"
                onClick={handleSubmit}
                //className='login-button'
                block>
                Login
              </Button>
            </div>
            <div></div>
          </div>
        </div>

        <div className="imgcontainer">
          <div>
            <div className="side-img m-auto">
              <img alt="example" width="90%" height="90%" src={LoginImageBanner} />
            </div>
            <div className="login-banner-text">
              <div className="login-banner-title">Design. Build. Launch</div>
              <div className="login-banner-subtitle">
                Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code Required!!!
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default AppComponent;
