import React from 'react';
import '../../config/config';
import { Row, Col, Input, Select } from 'antd';
import CreateModulePopup from '../../components/Popup/CreateModulePopup';
import Tile1 from '../../components/Subheader/Tile1';
import Tile2 from '../../components/Subheader/Tile2';
import Tile3 from '../../components/Subheader/Tile3';
import Tile5 from '../../components/Subheader/Tile5';
import ViewBoxes from '../../components/ViewBoxes/Index';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu';
import '../../assets/styles/index.css';
import '../../components/Popup/popup.css';
import Headers from '../../components/Header';
import AssetManagementAppCard from '../../components/Cards/AssetManagementAppCard';
const { Sider, Content } = Layout;

const { Option, OptGroup } = Select;

const ModulePageCount = () => {
  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent activeKey="3" />
          </Sider>
          <Content>
            <div className="containers bg-light-blue pad-3">
              <div className="subheader-container ptb-1">
                <Row>
                  <Col xl={7} lg={7} md={12} sm={12} xs={24}>
                    <Tile1
                    // appName={state.name}
                    // userName={username}
                    // appLogo={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
                    // createdDate={moment(state.createdAt).format('Do dddd, YYYY')}
                    // moduleCount={moduleCount}
                    // pageCount={pageCount}
                    // appId={state._id}
                    />
                  </Col>
                  <Col xl={6} lg={6} md={12} sm={12} xs={24}>
                    <Tile2 />
                  </Col>
                  <Col xl={6} lg={5} md={12} sm={12} xs={24}>
                    <Tile3 />
                  </Col>
                  <Col xl={5} lg={6} md={12} sm={12} xs={24}>
                    <Tile5 />
                  </Col>
                </Row>
              </div>

              <div className="ptb-2">
                <Row>
                  <Col xl={20} lg={20} md={24} sm={24} xs={24}>
                    <div className="Search_box">
                      <Row gutter={[10, 10]}>
                        <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                          <Input placeholder="Search ..." />
                        </Col>
                        <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                          <Select defaultValue="Sort by Layout" style={{ width: '100%' }}>
                            <OptGroup label="Sort by ">
                              <Option value="1">Sort by A to Z</Option>
                              <Option value="-1">Sort by Z to A</Option>
                            </OptGroup>
                          </Select>
                        </Col>
                        <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                          <Select defaultValue="Filter by " style={{ width: '100%' }}>
                            <OptGroup label="Filter by ">
                              <Option value="">Filter by A to Z</Option>
                              <Option value="">Filter by Z to A</Option>
                            </OptGroup>
                          </Select>
                        </Col>
                        <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                          <Select defaultValue="Last Date" style={{ width: '100%' }}>
                            <OptGroup label="Date ">
                              <Option value="">05 Jan 2022</Option>
                              <Option value="">04 jan 2022</Option>
                            </OptGroup>
                          </Select>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                  <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                    <ViewBoxes />
                  </Col>
                </Row>
              </div>
              <div className="ptb-1">
                {/* <div>
                    
                  </div> */}
                <Row>
                  <Col xl={6} lg={6} md={8} sm={12} xs={24}>
                    <CreateModulePopup moduleName="Add New Module" />
                  </Col>
                  <Col xl={6} lg={6} md={8} sm={12} xs={24}>
                    <AssetManagementAppCard
                      card_title="Asset Management"
                      card_date="7th December, 2021"
                      footer_text="Crate New Page"
                      footer_status="* In Progress"
                    />
                  </Col>
                </Row>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default ModulePageCount;
