import React from 'react';
import SubheaderComponent from './SubheaderComponent';
import { Row, Col } from 'antd';
import PageTypeBox from '../../components/Cards/PageTypeBox';
import Business1 from '../../assets/images/Business decisions-bro 1.png';
import Pagetype1 from '../../assets/images/page_type/Blank Page.png';
import Pagetype2 from '../../assets/images/page_type/Blog Page.png';
import Pagetype3 from '../../assets/images/page_type/Form.png';
import Pagetype4 from '../../assets/images/page_type/Content.png';
import Pagetype5 from '../../assets/images/page_type/Analytics.png';
import Pagetype6 from '../../assets/images/page_type/Diagram.png';
import Pagetype7 from '../../assets/images/page_type/Spatial.png';
import { useLocation } from 'react-router-dom';

const AppComponent = () => {
  const { state } = useLocation();
  // console.log('new state in app created data', useLocation());
  console.log('PAGE DATA IS', state);
  return (
    <>
      <div className="page_type">
        <SubheaderComponent state={state} cancelUrl="/app_page_grid" />
        <div className="img-box" style={{ display: 'flex', justifyContent: 'center' }}>
          <img alt="example" src={Business1} />
        </div>
        <h3 className="page_main_title">What kind of page you want to create ? </h3>
        <div>
          <Row className="card-sm" justify="center" gutter={[15, 15]}>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox
                page_img_src={Pagetype1}
                page_title="Blank Page"
                page_link="/form_empty"
              />
            </Col>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox page_img_src={Pagetype2} page_title="Blog Page" />
            </Col>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox page_img_src={Pagetype3} page_title="Form" />
            </Col>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox page_img_src={Pagetype4} page_title="Content" />
            </Col>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox page_img_src={Pagetype5} page_title="Analyticsic" />
            </Col>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox page_img_src={Pagetype6} page_title="Diagram" />
            </Col>
            <Col xl={3} lg={6} md={6} sm={6} xs={24}>
              <PageTypeBox page_img_src={Pagetype7} page_title="Spatial" />
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};

export default AppComponent;
