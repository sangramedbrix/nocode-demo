import React from 'react';
import Headers from '../../components/Header';
import AppComponent from './component';
import '../../assets/styles/index.css';

const CanvasPage = () => {
  return (
    <>
      <Headers />
      <AppComponent />
    </>
  );
};

export default CanvasPage;
