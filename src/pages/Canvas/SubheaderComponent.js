import { MoreOutlined } from '@ant-design/icons';
import '../../config/config';
import React, { useState, useEffect } from 'react';
import { Button, Row, Col, Form, Select, Menu, Dropdown } from 'antd';
import axios from 'axios';
import '../../assets/styles/index.css';
import { useLocation, useParams, useNavigate } from 'react-router-dom';
import CancelPage from '../../components/Popup/CancelPage';
import PropTypes from 'prop-types';

const { Option } = Select;

const savemenus = (
  <Menu className="list_menu">
    <Menu.Item key="0">
      <a href="">Save as Draft</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="">Save as Local Copy</a>
    </Menu.Item>
    <Menu.Item key="3">Save as Version</Menu.Item>
  </Menu>
);

const menu = (
  <Menu className="list_menu">
    <Menu.Item key="0">
      <a href="">Edit Page Settings</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="">Show Version History</a>
    </Menu.Item>
    <Menu.Item key="3">Export to PDF</Menu.Item>
  </Menu>
);

const SubheaderComponent = (props) => {
  const { state } = useLocation();
  const navigate = useNavigate();

  const { appId, moduleId, pageId } = useParams();
  console.log('APPIS', appId);
  console.log('moduleId', moduleId);
  console.log('pageId', pageId);

  const [moduleList, setModuleList] = useState([]);
  const [pageList, setPageList] = useState([]);
  const [isPageId, setPageId] = useState(pageId);
  const [isModalVisible, setIsModalVisible] = useState(false);
  console.log('new state in app created', useLocation());

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  var getModules = async () => {
    console.log('app id data', appId);
    axios
      .post(global.config.API_URL + '/getModules', { appId: appId })
      .then((respondse) => {
        console.log('modules', respondse);
        setModuleList(respondse.data.data);
      })
      .catch((error) => console.log(error));
  };

  const getModulePages = () => {
    axios
      .post(global.config.API_URL + '/pageFilters', {
        appId: appId,
        moduleId: moduleId,
        searchTypeNameValue: '',
        searchTypeDateValue: '',
        searchTypeSortValue: '1'
      })
      .then((respons) => {
        console.log('search response', respons.data.data);
        setPageList(respons.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const getPage = () => {
    axios
      .post('http://localhost:3000/getPage', { pageId: pageId })
      .then((respons) => {
        console.log('page', JSON.parse(respons.data.message.pageJson));
        props.setPageJson(JSON.parse(respons.data.message.pageJson));
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeStatus = (event) => {
    axios
      .post(global.config.API_URL + '/pageFilters', {
        appId: appId,
        moduleId: event,
        searchTypeNameValue: '',
        searchTypeDateValue: '',
        searchTypeSortValue: '1'
      })
      .then((respons) => {
        console.log('search response', respons.data.data);
        setPageId('');
        setPageList(respons.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  // select page
  const selectPage = (event) => {
    console.log(event);
    props.getPageId(event);
    navigate(`/app/${appId}/module/${moduleId}/page/${event}`);
  };

  // save page function

  const savePage = () => {
    const paramsData = {
      appId: appId,
      moduleId: moduleId,
      pageId: pageId
    };
    props.savePage(paramsData);
  };

  useEffect(() => {
    getModules();
    getModulePages();
    getPage();
    setPageId(pageId);
  }, []);
  console.log('isPageId', isPageId);

  return (
    <div className="subheader_components">
      <Row className="subheader_components_box" gutter={[0, 10]} align="middle">
        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
          <Row className="first_row" align="middle" gutter={[10, 8]}>
            <Col xl={2} lg={2} md={4} sm={2} xs={5}>
              {/* <div  className='cancel-box'>
                <i className='icon_back_arrow'></i> Cancel

              </div> */}
              <div className="cancel-box text-center">
                {/* onClick={() => {
                navigate('/app/pages', {
                  state: { data: { _id: props.appId }, from: 'app list', moduleId: 0 }
                });
              }} */}
                <Button
                  type="link"
                  onClick={() => {
                    showModal();
                  }}>
                  <i className="icon_back_arrow text-center" />{' '}
                  <span style={{ marginTop: 7 }}>Cancel</span>
                </Button>
                <CancelPage
                  isModallVisible={isModalVisible}
                  stateData={state}
                  handleCancel={handleCancel}
                  popup_description="If you cancel this page , you will lose unsaved data and also not saved in your automatic generated version."
                  pageCancelYes={`/app/${appId}/module/${moduleId}`}
                  pageGoBack={handleCancel}
                />
              </div>
            </Col>
            <Col className="dropdown-border-none vertical-line" xl={9} lg={6} md={5} sm={6} xs={12}>
              <Form.Item label="Module" style={{ width: '100%' }}>
                {/* <span>{state.moduleName}</span> */}
                <Select
                  defaultValue={moduleId}
                  style={{ width: '100%' }}
                  onChange={handleChangeStatus}>
                  {moduleList.map((item, index) => (
                    <Option key={index} value={item._id}>
                      {item.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col className="dropdown-border-none vertical-line" xl={9} lg={6} md={6} sm={5} xs={10}>
              <Form.Item label="Page" style={{ width: '100%' }}>
                {/* <span>{state.pageName}</span> */}
                <Select defaultValue={pageId} onChange={selectPage}>
                  {pageList.map((item, index) => (
                    <Option key={index} value={item._id}>
                      {item.pageName}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            {/* <Col xl={6} lg={6} md={6} sm={8} xs={12}>
                {' '}
                Status: <span className="status-div"> Action Pending </span>
              </Col> */}
          </Row>
        </Col>
        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
          <Row className="seconed_row" align="middle" gutter={[10, 8]}>
            <Col xl={6} lg={6} md={6} sm={7} xs={8}>
              <Button type="link">
                <i className="icon_comment_link" />{' '}
                <span className="margin-right"> Comments (0)</span>
              </Button>
            </Col>
            <Col xl={4} lg={4} md={4} sm={4} xs={8}>
              <div className="preview-btn">
                <Dropdown overlay={savemenus} trigger={['click']} onClick={savePage}>
                  <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                    <Button style={{ width: '100%' }}>
                      <i className="circular_tick"></i>
                      Save
                      <i className="icon_triangle_down"></i>
                    </Button>
                  </a>
                </Dropdown>
              </div>
            </Col>
            <Col className="preview-btn" xl={4} lg={4} md={4} sm={4} xs={8}>
              <Button style={{ width: '100%' }}>
                <i className="icon_eye"></i>
                Preview
              </Button>
            </Col>
            <Col className="publish-btn" xl={4} lg={4} md={4} sm={4} xs={8}>
              <Button style={{ width: '100%' }} type="primary" icon={<i className="icon_rocket" />}>
                {' '}
                Publish{' '}
              </Button>
            </Col>
            <Col className="more-btn" xl={2} lg={2} md={2} sm={4} xs={3}>
              <Dropdown overlay={menu} trigger={['click']}>
                <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                  <Button type="" icon={<MoreOutlined />}>
                    {' '}
                  </Button>
                </a>
              </Dropdown>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default SubheaderComponent;

SubheaderComponent.propTypes = {
  setPageJson: PropTypes.func,
  getPageId: PropTypes.func,
  savePage: PropTypes.func
};
