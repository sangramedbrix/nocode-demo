import React from 'react';
import AppComponent from './component';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/Sidemenu';
import Headers from '../../components/Header';
import '../../assets/styles/index.css';
const { Sider, Content } = Layout;

export default function LandingPage() {
  return (
    <>
      {/* <AppComponent /> */}
      <Headers />
      <Layout>
        <Layout>
          <Sider>
            <SideMenuComponent activeKey="1" />
          </Sider>
          <Content>
            <AppComponent />
          </Content>
        </Layout>
      </Layout>
    </>
  );
}
