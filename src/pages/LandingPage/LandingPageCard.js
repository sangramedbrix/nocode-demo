import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import '../../config/config';
import { Row, Col, Button, Divider, Card, Modal, Select } from 'antd';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import InputFile from '../../components/InputsFields/InputFile';
import InputCheckbox from '../../components/InputsFields/InputCheckbox';
import Dropdowns from '../../components/Dropdowns/Dropdowns';
import AppImageBanner from '../../assets/images/app_banner.png';
import axios from 'axios';
// theme select popup import
import CardImageTop from '../../components/Cards/CardImageTop';
import SearchBox from '../../components/Dropdowns/SearchBox';
import ViewBoxes from '../../components/ViewBoxes/Index';
import { useNavigate } from 'react-router-dom';
import selecttheme from '../../assets/images/selectTheam.png';
import '../../assets/styles/index.css';

const { Option } = Select;

// end theme select import

export default function LandingPageCard(props) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [companyId, setCompanyId] = useState();
  const [isModalVisibleTheme, setIsModalVisibleTheme] = useState(false);
  const [appCategory, setAppCategory] = useState([]);
  const [appData, setAppData] = useState([]);
  const [authUser, setAuthUser] = useState([]);
  // global.config.API_URL + '/uploads/company/61e11f7dc8875217f6eb8023/app/61e93409cdcc17c9eec80d8a/logo/1642673161342.png
  const [createNewApp, setCreateNewApp] = useState({
    name: '',
    category: '',
    appType: '',
    description: '',
    isActive: 0,
    file: [],
    nameValidation: '',
    categoryValidation: '',
    appTypeValidation: '',
    descriptionValidation: '',
    imageValidation: ''
  });

  const navigate = useNavigate();

  const getAppCategory = () => {
    axios
      .get(global.config.API_URL + '/getAppCategory')
      .then((response) => {
        console.log(response);
        setAppCategory(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const createdApp = () => {
    axios
      .get(global.config.API_URL + '/getCreatedApp')
      .then((response) => {
        console.log('response data', response);
        setAppData(response.data.data[0]);
      })
      .catch((error) => console.log(error));
  };

  const nameChange = (name) => {
    console.log('app name', name);
    if (name != '') {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const categoryChange = (category) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      category: category,
      categoryValidation: false
    }));
  };

  const handleChangeApplicationType = (event) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      appType: event,
      appTypeValidation: false
    }));
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));
      console.log('empty');
    }
  };

  const changeStatus = (status) => {
    if (status === 1) {
      setCreateNewApp((prevState) => ({ ...prevState, isActive: status }));
    } else if (status === 0) {
      setCreateNewApp((prevState) => ({ ...prevState, isActive: status }));
    }
  };

  const selectFile = (file) => {
    console.log('file', file);
    setCreateNewApp((prevState) => ({ ...prevState, file: file, imageValidation: false }));
  };

  const onSubmit = () => {
    if (
      createNewApp.name != '' &&
      createNewApp.category != '' &&
      createNewApp.appType != '' &&
      createNewApp.file.length != 0
    ) {
      const data = JSON.parse(localStorage.getItem('isAuthenticated'));
      const formData = new FormData();
      formData.append('image', createNewApp['file'].file.originFileObj);
      formData.append('name', createNewApp.name);
      formData.append('description', createNewApp.description);
      formData.append('category', createNewApp.category);
      formData.append('isActive', createNewApp.isActive);
      formData.append('companyId', data[0].companyId);
      formData.append('appType', createNewApp.appType);
      formData.append('createdUserId', authUser._id);
      // global.config.API_URL + '/createNewApp
      axios
        .post(global.config.API_URL + '/createNewApp', formData)
        .then((response) => {
          console.log(response);
          createdApp();
          if (response.data.message === 'App created!') {
            setCreateNewApp((prevState) => ({
              ...prevState,
              name: '',
              description: '',
              category: ''
            }));
            setTimeout(() => {
              setIsModalVisibleTheme(true);
            }, 2000);
          }
        })
        .catch((error) => console.log(error));
    } else {
      if (createNewApp.name == '') {
        setCreateNewApp((prevState) => ({ ...prevState, nameValidation: true }));
      } else if (createNewApp.category == '') {
        setCreateNewApp((prevState) => ({ ...prevState, categoryValidation: true }));
      } else if (createNewApp.appType == '') {
        setCreateNewApp((prevState) => ({ ...prevState, appTypeValidation: true }));
      } else if (createNewApp.file.length == 0) {
        setCreateNewApp((prevState) => ({ ...prevState, imageValidation: true }));
      }
    }
  };
  useEffect(() => {
    getAppCategory();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    console.log('datas', data);
    setAuthUser(data[0]);
    setCompanyId(data[0].companyId);
  }, []);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleOkTheme = () => {
    setIsModalVisibleTheme(false);
  };

  const handleCancelTheme = () => {
    setIsModalVisibleTheme(false);
    // navigate('/app/module/list', { state: appData });
    navigate('/apps', { state: { appData: appData, appListActive: 2 } });
    // history.push('/app/module/list',{state:appData})
  };

  return (
    <>
      {/* modal for create your own app */}
      <div>
        <Modal
          className="modal-close-btn-white"
          width={'100%'}
          height={'100%'}
          style={{ top: 0 }}
          header={false}
          footer={false}
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}>
          <div className="flex-container bg-light-blue">
            <div className="containers">
              <div className="createApp-form-box">
                <div className="box-title">
                  <div className="headertext text-center">Create your own app today</div>
                  <div className="subheadertxt text-center">
                    No code App Maker to Build Your App
                  </div>
                </div>
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">
                    App Name <span style={{ color: 'red' }}> *</span>
                  </div>
                  <InputText
                    placeholder_text="Enter App Name"
                    value={createNewApp.name}
                    nameChangeFunction={nameChange}
                  />
                  {createNewApp.nameValidation === true ? (
                    <p className="text-danger"> App name is required </p>
                  ) : (
                    // <span style={{ color: '#a94442' }}></span>
                    ''
                  )}
                </div>
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">
                    Category <span style={{ color: 'red' }}> *</span>
                  </div>
                  <Dropdowns
                    option={appCategory}
                    dropdown_title="Select"
                    value={createNewApp.category}
                    categoryChangeFunction={categoryChange}
                  />
                  {createNewApp.categoryValidation === true ? (
                    <p className="text-danger"> Category is required </p>
                  ) : (
                    ''
                  )}
                  {/* <SelectDropdown option={appCategory} dropdown_title='Select' value={createNewApp.category} dropdwon_size='medium' categoryChangeFunction={categoryChange} style={{width:'100%' }}/>
                            {
                                createNewApp.categoryValidation === true 
                                ? <span style={{color:'red'}}>Category is required</span>
                                : ''
                            } */}
                </div>
                <div className=" bolder">
                  <div className="createAppSubtxt">
                    Application Type <span style={{ color: 'red' }}> *</span>
                  </div>
                  <Select
                    className="dd-box"
                    placeholder="Select Application Type"
                    onChange={handleChangeApplicationType}>
                    <Option value="1">Web</Option>
                    <Option value="2">Mobile</Option>
                  </Select>
                </div>
                {createNewApp.appTypeValidation === true ? (
                  <p className="text-danger">Appplication type is required</p>
                ) : (
                  ''
                )}
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">Description</div>
                  <InputTextArea
                    // placeholder_text=''
                    discriptionChangeFunction={discriptionChange}
                    value={createNewApp.description}
                  />
                </div>
                <div className="mtb-2">
                  <div className="createAppSubtxt">Status</div>
                  <div className="checkbox2">
                    {' '}
                    <InputCheckbox statusCheck={changeStatus} />
                    <span style={{ marginLeft: 8 }}> Yes, I agree to show my App to public</span>
                  </div>
                </div>
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">
                    App Icon <span style={{ color: 'red' }}> *</span>
                  </div>
                  <div className="text-input-upload">
                    <InputFile text="No choosen file" fileSelect={selectFile} />
                  </div>
                  {createNewApp.imageValidation === true ? (
                    <p className="text-danger">App icon is required</p>
                  ) : (
                    ''
                  )}
                </div>
                <div className="mt-5 btn-div">
                  <div className="app-link">
                    <Button onClick={handleCancel}>Cancel</Button>
                  </div>
                  <div className="app-button">
                    <Button onClick={onSubmit}>Select Theme</Button>
                    {/* <Buttons button_title='Select Theme' button_size='medium'  onClick={onSubmit} className='app-button-text' /> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="appimgcontainer">
              <div className="create-banner-text">
                <div className="banner-titletop">App Builder to build apps</div>
                <div className="banner-subtitletop">in five minutes without Code </div>
              </div>
              <div>
                <div className="side-img text-center">
                  <img className="m-auto" width="70%" alt="example" src={AppImageBanner} />
                </div>
                <div className="create-banner-text">
                  <div className="banner-title text-center">Design. Build. Launch</div>
                  <div className="banner-subtitle">
                    Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code
                    Required!!!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
      {/* end  modal for create your own app */}

      {/* modal popup for select theme */}
      <Modal
        className="modal-container-grey"
        width={'100%'}
        height={'100%'}
        style={{ top: 0 }}
        header={false}
        footer={false}
        visible={isModalVisibleTheme}
        onOk={handleOkTheme}
        onCancel={handleCancelTheme}>
        {/* <div className='containers '>
                <div className='select_theam_header'>
                    <Row gutter={70}>
                        <Col sm={{ span: 24 }} xl={{ span: 1 }} ><img src='Assets/Images/themeselecticon.png' /></Col>
                        <Col sm={{ span: 24 }} xl={{ span: 8 }} >
                            <p className='p-tag'>App</p>
                            <h2 className='p-tag'>{appData.name}</h2>
                        </Col>
                    </Row>
                    <Divider />
                </div>
                <div className='select_theam_search_box'>
                    <SearchBox />
                </div>
                <Row className='cardrow mtb-2 mb-5' justify='' gutter={[25, 75]} >
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                </Row>
              
            </div> */}

        <div className="containers">
          <div className="select_theam_header">
            <Row gutter={[70, 0]}>
              <Col xl={2} lg={1} md={1} sm={1} xs={1}>
                <img
                  src={
                    global.config.API_URL +
                    `/uploads/company/${companyId}/app/${appData._id}/logo/${appData.logoFileName}`
                  }
                />
              </Col>
              <Col xl={20} lg={20} md={20} sm={18} xs={18} className="header-text">
                <div>
                  <div className="app">App</div>
                  <h2 className="app_name">{appData.name}</h2>
                </div>
              </Col>
            </Row>
            <Divider />
          </div>
          <div className="select_theam_search_box">
            <Row>
              <Col xl={20} lg={20} md={24} sm={24} xs={24}>
                <SearchBox placeholder="Search ..." />
              </Col>
              <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                <ViewBoxes grid="active" list="inactive" />
              </Col>
            </Row>
          </div>
          <Row className="cardrow mt-3" type="flex" gutter={[16, 25]}>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
            <Col className="theam_select_card_col">
              {' '}
              <CardImageTop
                appData={appData}
                action_footer="false"
                card_img_src={selecttheme}
                card_title="Shades of Teal"
                card_description="Vibrant"
                card_color="Color"
              />
            </Col>
          </Row>
        </div>
      </Modal>

      {/* end modal popup for select theme */}

      <Card
        className="card-box"
        title={props.title}
        cover={<img alt="example" style={{ margin: '10px auto' }} src={props.banner_img} />}>
        <div className="small-font bold-500 mar-30 pr-2 pl-2">{props.desc}</div>
        {(() => {
          if (props.title === 'Apps') {
            return (
              <>
                <Button
                  icon={<i className="create_new" />}
                  className="mbt-5 mlr create-app-button create-app-button-text"
                  onClick={showModal}>
                  {' '}
                  Create New{' '}
                </Button>
              </>
            );
          } else if (props.title === 'Custom Blocks') {
            return (
              <Button
                icon={<i className="create_new" />}
                className="mbt-5 mlr create-app-button create-app-button-text">
                {' '}
                Create New{' '}
              </Button>
            );
          }
        })()}
        <Button
          icon={<i className="select_temp" />}
          className="mbt-5 mlr create-app-button2 create-app-button-text2">
          {' '}
          Select Template{' '}
        </Button>
      </Card>
    </>
  );
}

LandingPageCard.propTypes = {
  title: PropTypes.string,
  banner_img: PropTypes.string,
  desc: PropTypes.string,
  categoryChangeFunction: PropTypes.func
};
