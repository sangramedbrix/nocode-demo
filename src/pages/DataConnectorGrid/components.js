import React, { useEffect, useState } from 'react';
import { Row, Col, Input, Select, Button, Modal, Form, Dropdown, Menu } from 'antd';
import '../../components/Popup/popup.css';
import { DeleteOutlined } from '@ant-design/icons';
import TextHeadingH3 from '../../components/TextHeadings/TextHeadingH3';
import PropTypes from 'prop-types';
import ViewBoxes from '../../components/ViewBoxes/Index';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
const { Option, OptGroup } = Select;

const AppPageGrid = () => {
  const { state } = useLocation();
  let userData = JSON.parse(localStorage.getItem('isAuthenticated'));
  var params = JSON.parse(JSON.stringify(userData));
  let companyId = '6200f8124a600e40f2138649'; //params[0].companyId;
  const [connectors, setDataConnectors] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataConnectorName, setDataConnectorName] = useState('');
  const [errorEmailMessage, setEmailErrorMessage] = useState({ value: '' });
  const [isModalVisiblePage, setIsModalVisiblePage] = useState(false);
  const [pageName, setDeletePageName] = useState();
  const [setDeleteId, setDeletePageNameId] = useState();
  const [dataId, setDataId] = useState(0);

  const [searchTypeNameValue, setSearchTypeNameValue] = useState('');
  const [searchTypeSortValue, setSearchTypeSortValue] = useState(1);
  const [searchTypeDateValue, setSearchTypeDateValue] = useState('');

  var getDataConnectorList = () => {
    axios
      .post('http://localhost:3000/getDataConnectorList', {
        companyId: companyId,
        searchTypeNameValue: searchTypeNameValue,
        sortByStatusValue: searchTypeSortValue,
        searchTypeDateValue: searchTypeDateValue
      })
      .then((response) => {
        console.log('response data in app from 1', response);
        setDataConnectors(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  function deleteDataConnector(id, coonnectorTitle) {
    setDeletePageName(coonnectorTitle);
    setIsModalVisiblePage(true);
    setDeletePageNameId(id);
  }

  const getDataConnectorDetails = (id) => {
    setDataId(id);
    axios
      .get(`http://localhost:3000/getDataConnectorById/${id}`)
      .then((respons) => {
        setDataConnectorName(respons.data.data.title);
      })
      .catch((error) => console.log('error', error));
    setIsModalVisible(true);
  };

  const handleChangeInput = (event) => {
    setDataConnectorName(event.target.value);
  };

  const updatePageData = () => {
    if (dataId == 0) {
      if (dataConnectorName == '') {
        setEmailErrorMessage(() => ({
          value: 'This field is required'
        }));
      } else {
        axios
          .post('http://localhost:3000/saveDataConnector', {
            title: dataConnectorName,
            companyId: companyId,
            isActive: 1,
            createdUserId: params[0].companyId,
            updatedUserId: '61e11f7dc8875217f6eb8023'
          })
          .then((respons) => {
            setDataConnectorName('');
            console.log(respons);
            getDataConnectorList();
          })
          .catch((error) => console.log('error', error));
        setIsModalVisible(false);
        getDataConnectorList();
      }
    } else {
      if (dataConnectorName == '') {
        setEmailErrorMessage(() => ({
          value: 'This field is required'
        }));
      } else {
        axios
          .put(`http://localhost:3000/updateDataConnector/${dataId}`, {
            title: dataConnectorName,
            companyId: companyId,
            isActive: 1,
            createdUserId: params[0].companyId,
            updatedUserId: '61e11f7dc8875217f6eb8023'
          })
          .then((respons) => {
            setDataConnectorName('');
            setDataId(0);
            getDataConnectorList();
            console.log('dfdgfd', respons);
          })
          .catch((error) => console.log('error', error));
        setIsModalVisible(false);
        getDataConnectorList();
      }
    }
  };

  const handleChange = (event) => {
    setSearchTypeNameValue(event.target.value);
    axios
      .post('http://localhost:3000/getDataConnectorList', {
        companyId: companyId,
        searchTypeNameValue: event.target.value,
        sortByStatusValue: searchTypeSortValue,
        searchTypeDateValue: searchTypeDateValue
      })
      .then((response) => {
        setDataConnectors(response.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeDropDown = (event) => {
    setSearchTypeSortValue(event);
    console.log('searchTypeSortValue', event);
    axios
      .post('http://localhost:3000/getDataConnectorList', {
        companyId: companyId,
        searchTypeNameValue: searchTypeNameValue,
        sortByStatusValue: event,
        searchTypeDateValue: searchTypeDateValue
      })
      .then((response) => {
        console.log('getDataConnectorList', response);
        setDataConnectors(response.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  // function onChange(value, dateString) {
  //   console.log('Selected Time: ', value);
  //   console.log('Formatted Selected Time: ', dateString);
  //   setSearchTypeDateValue(dateString);
  //   axios
  //     .post('http://localhost:3000/getDataConnectorList', {
  //       companyId: companyId,
  //       searchTypeNameValue: searchTypeNameValue,
  //       sortByStatusValue: searchTypeSortValue,
  //       searchTypeDateValue: dateString
  //     })
  //     .then((response) => {
  //       console.log('getDataConnectorList', response);
  //       setDataConnectors(response.data.data);
  //     })
  //     .catch((error) => console.log('error', error));
  // }

  const deleteFunctionCall = () => {
    axios
      .delete(`http://localhost:3000/deleteDataConnector/${setDeleteId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisiblePage(false);
          getDataConnectorList();
        }
      })
      .catch((error) => console.log(error));
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
    setDataConnectorName('');
    setDataId(0);
  };

  const handleDeleteCancel = () => {
    setIsModalVisiblePage(false);
    setDataConnectorName('');
    setDataId(0);
  };

  const editModule = () => {
    setDataConnectorName('');
    setDataId(0);
    setIsModalVisible(true);
  };

  useEffect(() => {
    getDataConnectorList();
    setSearchTypeDateValue();
  }, []);

  return (
    <>
      <div className="Delete_popup">
        <Modal
          visible={isModalVisiblePage}
          className="delete_popup"
          onCancel={handleDeleteCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3
                className="text_align_center"
                text_h3={`You are about to delete a '${pageName}' page`}
              />
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={handleDeleteCancel}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>
      <div className="create_page">
        <Modal
          className="create_page_popup"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
          header={false}
          footer={false}>
          <Form layout="vertical">
            <Form.Item label="Name" name="Name" rules={[{ required: true }]}>
              <Input
                className="text-input-box"
                name="username"
                onChange={(e) => handleChangeInput(e)}
              />
              <span>
                {errorEmailMessage.value && (
                  <p className="text-danger"> {errorEmailMessage.value} </p>
                )}
              </span>
            </Form.Item>
            <div className="btn_div">
              <Button className="cancel_btn" type="link" onClick={handleCancel}>
                {' '}
                Cancel
              </Button>
              <Button className="submit_btn" type="primary" Type="submit" onClick={updatePageData}>
                {' '}
                Submit{' '}
              </Button>
            </div>
          </Form>
        </Modal>
      </div>

      <div className="create_page">
        <Modal
          className="create_page_popup"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
          header={false}
          footer={false}>
          <Form layout="vertical">
            <Form.Item label="Name" name="Name" rules={[{ required: true }]}>
              <Input
                className="text-input-box"
                placeholder="email@website.com"
                value={dataConnectorName}
                name="username"
                onChange={(e) => handleChangeInput(e)}
              />

              <span>
                {errorEmailMessage.value && (
                  <p className="text-danger"> {errorEmailMessage.value} </p>
                )}
              </span>
            </Form.Item>
            <div className="btn_div">
              <Button className="cancel_btn" type="link" onClick={handleCancel}>
                {' '}
                Cancel
              </Button>
              <Button className="submit_btn" type="primary" Type="submit" onClick={updatePageData}>
                {' '}
                Submit{' '}
              </Button>
            </div>
          </Form>
        </Modal>
      </div>
      <div style={{ marginRight: 20, marginLeft: 20 }} className="ptb-2 pl-2 pr-2">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <div className="Search_box">
              <Row gutter={[10, 10]}>
                <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                  <Input placeholder="Search ..." onChange={handleChange} />
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select
                    defaultValue="Sort by Layout"
                    onChange={handleChangeDropDown}
                    style={{ width: '100%' }}>
                    <OptGroup label="Sort by ">
                      <Option value="1">Sort by A to Z</Option>
                      <Option value="-1">Sort by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                {/* <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <DatePicker size="large" format="MM/DD/YYYY" onChange={onChange}>
                    <Input
                      placeholder="Last Date"
                      type=""
                      title=""
                      size="12"
                      autocomplete="off"
                      value=""
                    />
                  </DatePicker>
                </Col> */}
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <span onClick={() => editModule()}>Add</span>
                </Col>
              </Row>
            </div>
          </Col>
          <Col xl={4} lg={4} md={24} sm={24} xs={24}>
            <ViewBoxes
              grid="active"
              list="inactive"
              listlink="/dataconnector/list"
              gridlink="/dataconnector/grid"
              data={state}
            />
          </Col>
        </Row>
      </div>

      <div>
        <Row gutter={[16, 25]}>
          {(() => {
            return connectors.map((createdModule) => {
              return (
                <>
                  <Col xl={6} lg={6} md={8} sm={12} xs={24}>
                    <div className="AssetManagementAppCard">
                      <div className="AssetManagementAppCardDiv">
                        <div className="card-body">
                          <Row>
                            <Col xs={22}>
                              <h3 className="title">{createdModule.title} </h3>
                              <span>{createdModule.title}</span>
                            </Col>
                            <Col xs={2} className="option-dots">
                              <Dropdown
                                placement="bottomRight"
                                overlay={(() => {
                                  return (
                                    <Menu>
                                      <Menu.Item>
                                        <span
                                          target="_blank"
                                          onClick={() =>
                                            getDataConnectorDetails(createdModule._id)
                                          }>
                                          {' '}
                                          Edit{' '}
                                        </span>
                                      </Menu.Item>
                                      <Menu.Item>
                                        <a
                                          target="_blank"
                                          rel="noopener noreferrer"
                                          onClick={() =>
                                            deleteDataConnector(
                                              createdModule._id,
                                              createdModule.title
                                            )
                                          }>
                                          {' '}
                                          Delete{' '}
                                        </a>
                                      </Menu.Item>
                                    </Menu>
                                  );
                                })()}>
                                <a
                                  className="ant-dropdown-link"
                                  onClick={(e) => e.preventDefault()}>
                                  <i className="icon_more"></i>
                                </a>
                              </Dropdown>
                            </Col>
                          </Row>

                          <p className="sub_title">
                            {moment(createdModule.createdAt).format('Do dddd, YYYY')}{' '}
                          </p>
                          <div style={{ padding: '6% 0' }}>
                            <div className="small_txt">{/* {createdModule.description} */}</div>
                            {/* <div className="large-txt">Start now creating pages.</div> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </>
              );
            });
          })()}
        </Row>
      </div>
    </>
  );
};

export default AppPageGrid;

AppPageGrid.propTypes = {
  getPageCount: PropTypes.func
};
