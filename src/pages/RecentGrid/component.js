import React from 'react';
import AppList from './AppList';
import BlockGrid from './BlockGrid';
import { Tabs } from 'antd';
import { useLocation } from 'react-router-dom';

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const AppComponent = () => {
  const location = useLocation();
  console.log('location', location);
  return (
    <>
      <div className="containers bg-light-blue box-padding">
        <Tabs onChange={callback}>
          <TabPane tab="Recent Activies" key="1">
            {/* <RecentActivies /> */}
            <AppList />
          </TabPane>
          <TabPane tab="My Apps" key="2">
            <AppList />
          </TabPane>
          <TabPane tab="My Blocks" key="3">
            <BlockGrid />
          </TabPane>
        </Tabs>
      </div>
    </>
  );
};

export default AppComponent;
