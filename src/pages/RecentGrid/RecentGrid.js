import React, { useEffect, useState } from 'react';
import { Row, Col, Input, Select } from 'antd';
import '../../config/config';
import AppsGridView from '../../components/Cards/AppsGridView';
// import SearchBox from '../../components/Dropdowns/SearchBox';
import ViewBoxes from '../../components/ViewBoxes/Index';
import axios from 'axios';
import moment from 'moment';

const { Option, OptGroup } = Select;

const RecentGrid = () => {
  const [apps, setApps] = useState([]);
  const [companyId, setCompanyId] = useState();

  const getApps = () => {
    axios
      .get(global.config.API_URL + '/getApps')
      .then((response) => {
        console.log('response apps data', response);
        setApps(response.data.data);
        //  setCount(array)
      })
      .catch((error) => console.log(error));
  };

  const handleChange = (event) => {
    console.log('jnfgndfgfdg', event.target.value);
    axios
      .post(global.config.API_URL + '/searchByAlphabetically', {
        tableName: 'apps',
        searchString: event.target.value
      })
      .then((res) => {
        console.log('response data', res);
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  const handleChangeDropDown = (event) => {
    axios
      .post(global.config.API_URL + '/sortByAlphabetically', {
        tableName: 'apps',
        searchString: event
      })
      .then((res) => {
        setApps(res.data.data);
      })
      .catch((error) => console.log('error', error));
  };

  useEffect(() => {
    getApps();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setCompanyId(data[0].companyId);
  }, []);

  console.log('apps', apps);

  let cartData = [{}];
  let newData1 = JSON.stringify(apps);

  const valuesArray = JSON.parse(newData1);
  {
    valuesArray.map(function (item) {
      //call api
      axios
        .post(global.config.API_URL + '/getModuleCount', { appId: item._id })
        .then((countValue) => {
          setTimeout(function () {
            cartData.push(countValue.data);
          }, 5000);
          //console.log("countValue",Object.keys(countValue.data).length)
          // cartData.push({
          //   key: 1010
          // });
        })
        .catch((error) => console.log(error));

      console.log('item', item._id);
    });
  }

  // let newData11 = JSON.stringify(cartData);
  // valuesArray = JSON.parse(newData1);
  // console.log('Data', JSON.parse(newData11));
  console.log('cartData', cartData);
  var counts = Object.keys(cartData).length;
  console.log('fdlgldfgfdi', counts);
  // let newData11 = JSON.stringify(cartData);
  // const valuesArrays = JSON.parse(cartData);
  // {
  //   valuesArrays.map(function (items)
  //   {
  //     console.log('itemsss', items);
  //   });
  // }

  // let moduleCount = []
  // let newData = JSON.stringify(apps);
  // console.log('JSON DATA===>>>>>',newData)
  // const valuesArray = JSON.parse(newData);
  // {
  //   valuesArray.map(function (item) {
  //     console.log('item', item._id);
  //     axios
  //     .post(global.config.API_URL + '/getModuleCount',{appId:item._id})
  //     .then((countValue) => {
  //       moduleCount.push(countValue)
  //       console.log("countValue",countValue.data)
  //     })
  //     .catch((error) => console.log(error));

  //   });

  // }

  // console.log('module count', moduleCount.length)

  // console.log("module count")

  return (
    <>
      <div className="select_theam_search_box mb-5">
        <Row>
          <Col xl={20} lg={20} md={24} sm={24} xs={24}>
            <div className="Search_box">
              <Row gutter={[10, 10]}>
                <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                  <Input placeholder="Search ..." onChange={handleChange} />
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select
                    defaultValue="Sort by Layout"
                    onChange={handleChangeDropDown}
                    style={{ width: '100%' }}>
                    <OptGroup label="Sort by ">
                      <Option value="1">Sort by A to Z</Option>
                      <Option value="-1">Sort by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select defaultValue="Filter by " style={{ width: '100%' }}>
                    <OptGroup label="Filter by ">
                      <Option value="">Filter by A to Z</Option>
                      <Option value="">Filter by Z to A</Option>
                    </OptGroup>
                  </Select>
                </Col>
                <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                  <Select defaultValue="Last Date" style={{ width: '100%' }}>
                    <OptGroup label="Date ">
                      <Option value="">05 Jan 2022</Option>
                      <Option value="">04 jan 2022</Option>
                    </OptGroup>
                  </Select>
                </Col>
              </Row>
            </div>
          </Col>
          <Col xl={4} lg={4} md={24} sm={24} xs={24}>
            <ViewBoxes
              grid="active"
              list="inactive"
              listlink="/app/list"
              gridlink="/apps"
              // data={state=""}
            />
            {/* <ViewBoxes list="inactive" grid="active" /> */}
          </Col>
        </Row>
      </div>
      <div>
        <Row className="cardrow mtb-2 mb-5" gutter={[16, 25]}>
          {apps.map((app, i) => {
            return (
              <Col xl={6} lg={6} md={8} sm={12} xs={24} key={i}>
                {' '}
                <AppsGridView
                  app_grid_icon={
                    global.config.API_URL +
                    `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`
                  }
                  card_title={app.name}
                  app_grid_date={moment(app.createdAt).format('Do dddd, YYYY')}
                  status="&bull; In Progress"
                  footer_module="0 Modules"
                  footer_page="0 page"
                  footer_comments="0 Comments"
                  appData={app}
                  getApps={getApps}
                />
              </Col>
            );
          })}
        </Row>
      </div>
    </>
  );
};

export default RecentGrid;
