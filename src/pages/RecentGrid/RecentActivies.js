import React from 'react';
import { Row, Col, Select, Input } from 'antd';
import RecentActivityGridView from '../../components/Cards/RecentActivityGridView';
import ViewBoxes from '../../components/ViewBoxes/Index';
const { Option, OptGroup } = Select;

const RecentActivies = () => {
  return (
    <>
      <div>
        {/* <Row gutter={5} className="recent_grid_card_div">
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col sm={{ span: 24 }} xl={{ span: 6 }}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
        </Row> */}
        <div className="select_theam_search_box mb-5">
          <Row>
            <Col xl={20} lg={20} md={24} sm={24} xs={24}>
              <div className="Search_box">
                <Row gutter={[10, 10]}>
                  <Col xl={8} lg={8} md={9} sm={9} xs={24}>
                    <Input placeholder="Search ..." />
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select
                      defaultValue="Sort by Layout"
                      // onChange={handleChangeDropDown}
                      style={{ width: '100%' }}>
                      <OptGroup label="Sort by ">
                        <Option value="1">Sort by A to Z</Option>
                        <Option value="-1">Sort by Z to A</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select defaultValue="Filter by " style={{ width: '100%' }}>
                      <OptGroup label="Filter by ">
                        <Option value="">Filter by A to Z</Option>
                        <Option value="">Filter by Z to A</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                  <Col xl={4} lg={4} md={5} sm={5} xs={24}>
                    <Select defaultValue="Last Date" style={{ width: '100%' }}>
                      <OptGroup label="Date ">
                        <Option value="">05 Jan 2022</Option>
                        <Option value="">04 jan 2022</Option>
                      </OptGroup>
                    </Select>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xl={4} lg={4} md={24} sm={24} xs={24}>
              <ViewBoxes />
            </Col>
          </Row>
        </div>
        <Row className="cardrow mtb-2 mb-5" gutter={[16, 25]}>
          <Col xl={6} lg={6} md={8} sm={12} xs={24}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
          <Col xl={6} lg={6} md={8} sm={12} xs={24}>
            <RecentActivityGridView
              recent_activity_icon="Assets/Images/cardTop.png"
              card_title="- Student Management"
              card_description="App : Student Management"
              no_of_page=" No of Pages : 0"
              card_coments="Latest Comments"
              card_text="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... "
              card_footer_date="24 December 2021"
              card_footer_status="&bull; Approved"
            />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default RecentActivies;
