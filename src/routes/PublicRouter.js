import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ProtectedRoute from '../components/ProtectedRoute';
import LandingPage from '../pages/LandingPage';
import Signin from '../pages/LoginPage';
// import ThemeSelect from '../pages/ThemeSelect';
import AppCreated from '../pages/AppCreated';
import PageList from '../pages/PageList';
import PageTemplate from '../pages/Canvas';
import EditorDashboard from '../pages/Dashboard';
import DataAnalysisDashboard from '../pages/DataAnalysisDashboard';
import AppPageList from '../pages/AppPageList';
import ConnectorsList from '../pages/ConnectorsList';
import AppList from '../pages/AppList';
import RecentActivitiesList from '../pages/RecentActvitiesList';
import RecentActivies from '../pages/AppList/RecentActivies';
import AppListing from '../pages/AppListing';
import RecentAppListing from '../pages/RecentAppListing';
import AppPageGrid from '../pages/AppPageListGrid';
import DataConnectorGrid from '../pages/DataConnectorGrid';
import ModuleList from '../pages/ModuleList';
import AddConnector from '../pages/AddConnector';
import ModulePageCount from '../pages/ModulePageCount';
import FormEmpty from '../pages/Editor/FormEmpty';
import RecentAppList from '../pages/RecentAppList';
import EditorFormBlank from '../pages/Editor/EditorFormBlank';
import SettingEdit from '../pages/Editor/Settings';
import ThemeSelectEditor from '../pages/Editor/ThemeSelect';
import BlockList from '../pages/BlockListing';
import BlockGrid from '../pages/BlockGrid';
import EditorFormPage from '../pages/Editor/EditorFormPages';
import EditorFormPagePreview from '../pages/Editor/EditorFormPagesPreview';
import EditorElementsSettings from '../pages/Editor/EditorElementSetting';
import EditorChat from '../pages/Editor/EditorChat';
// import FormIO from '../components/Forms';
import AdminUserGrid from '../pages/Admin/UserGrid';
import AdminUserList from '../pages/Admin/UserList';
import ElementPopup from '../pages/DataAnalytics/ElementPopup';
import FormIO from '../pages/CustomiseFormIo/app';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/login" element={<Signin />} />

        <Route
          exact
          path="/home"
          element={
            <ProtectedRoute>
              <LandingPage />
            </ProtectedRoute>
          }
        />
        {/* <Route
          exact
          path="/themeselect"
          element={
            <ProtectedRoute>
              <ThemeSelect />
            </ProtectedRoute>
          }
        /> */}
        <Route
          exact
          path="/app/module/list/page/list"
          element={
            <ProtectedRoute>
              <PageList />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/add/connector"
          element={
            <ProtectedRoute>
              <AddConnector />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/app/modules"
          element={
            <ProtectedRoute>
              <AppCreated />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/modules"
          element={
            <ProtectedRoute>
              <AppCreated />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/modules/list"
          element={
            <ProtectedRoute>
              <ModuleList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/module/:moduleId"
          element={
            <ProtectedRoute>
              <AppPageList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/module/:moduleId/grid"
          element={
            <ProtectedRoute>
              <AppPageGrid />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/module/:moduleId/page/:pageId"
          element={
            <ProtectedRoute>
              <EditorFormPage />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/module/:moduleId/page/:pageId/preview"
          element={
            <ProtectedRoute>
              <EditorFormPagePreview />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/pages"
          element={
            <ProtectedRoute>
              <AppPageList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/pages/grid"
          element={
            <ProtectedRoute>
              <AppPageGrid />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/"
          element={
            <ProtectedRoute>
              <Signin />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/:appId/module/:moduleId/page/:pageId"
          element={
            <ProtectedRoute>
              <PageTemplate />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/dashboard"
          element={
            <ProtectedRoute>
              <EditorDashboard />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/dataanalytics"
          element={
            <ProtectedRoute>
              <DataAnalysisDashboard />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/pages"
          element={
            <ProtectedRoute>
              <AppPageList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/dataconnector/list"
          element={
            <ProtectedRoute>
              <ConnectorsList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/apps"
          element={
            <ProtectedRoute>
              <AppList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/RecentActivies"
          element={
            <ProtectedRoute>
              <RecentActivies />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/recent/activities/list"
          element={
            <ProtectedRoute>
              <RecentActivitiesList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/list"
          element={
            <ProtectedRoute>
              <AppListing />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/recentapp/list"
          element={
            <ProtectedRoute>
              <RecentAppListing />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/recentapps"
          element={
            <ProtectedRoute>
              <RecentAppList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app/pages/test"
          element={
            <ProtectedRoute>
              <AppPageList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/module/list"
          element={
            <ProtectedRoute>
              <ModuleList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/app_page_grid"
          element={
            <ProtectedRoute>
              <AppPageGrid />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/dataconnector/grid"
          element={
            <ProtectedRoute>
              <DataConnectorGrid />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/module_page_count"
          element={
            <ProtectedRoute>
              <ModulePageCount />
            </ProtectedRoute>
          }
        />

        {/* Editors */}
        <Route
          exact
          path="/theme_select_editor"
          element={
            <ProtectedRoute>
              <ThemeSelectEditor />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/form_empty"
          element={
            <ProtectedRoute>
              <FormEmpty />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/blocklist"
          element={
            <ProtectedRoute>
              <BlockList />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/blockgrid"
          element={
            <ProtectedRoute>
              <BlockGrid />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/editor_form_blank"
          element={
            <ProtectedRoute>
              <EditorFormBlank />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/designpage"
          element={
            <ProtectedRoute>
              <EditorFormPage />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/designpage"
          element={
            <ProtectedRoute>
              <EditorFormPage />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/editor_elements_settings"
          element={
            <ProtectedRoute>
              <EditorElementsSettings />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/editor_Chat"
          element={
            <ProtectedRoute>
              <EditorChat />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/blockslist"
          element={
            <ProtectedRoute>
              <BlockList />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/setting"
          element={
            <ProtectedRoute>
              <SettingEdit />
            </ProtectedRoute>
          }
        />
        {/* <Route
          exact
          path="/formio"
          element={
            <ProtectedRoute>
              <FormIO />
            </ProtectedRoute>
          }
        /> */}

        {/* ****** Admin ***** */}

        <Route
          exact
          path="/admin_user_grid"
          element={
            <ProtectedRoute>
              <AdminUserGrid />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/admin_user_list"
          element={
            <ProtectedRoute>
              <AdminUserList />
            </ProtectedRoute>
          }
        />

        {/* ** data analytics ** */}
        <Route
          exact
          path="/element_popup"
          element={
            <ProtectedRoute>
              <ElementPopup />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/formio"
          element={
            <ProtectedRoute>
              <FormIO />
            </ProtectedRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
