import '../config/config';
import axios from 'axios';

export const auth = {
  isValidAppModule,
  isValidAppModulePages,
  isValidObjectId
};

function isValidObjectId(appId, moduleId) {
  if (appId != '') {
    if (appId.match(/^[0-9a-fA-F]{24}$/)) {
      // return 1;
      console.log('APPID');
    } else {
      window.location.href = '/apps';
      console.log('INVALID APPID');
    }
  }
  if (moduleId != '') {
    if (moduleId.match(/^[0-9a-fA-F]{24}$/)) {
      // return 1;
      console.log('MODULEID');
    } else {
      console.log('INVALID MODULEID');
      window.location.href = `/app/${appId}/modules`;
    }
  }
}

function isValidAppModule(appId) {
  axios
    .post(global.config.API_URL + '/checkValidAppModule', {
      appId: appId
    })
    .then((response) => {
      console.log('RESSSSSS', response.data.data.length);
      if (response.data.data.length === 0) {
        window.location.href = '/apps';
      } else {
        return false;
      }
    })
    .catch((error) => console.log('error', error));
}

function isValidAppModulePages(appId, moduleId) {
  axios
    .post(global.config.API_URL + '/checkValidAppModulePage', {
      appId: appId,
      moduleId: moduleId
    })
    .then((response) => {
      console.log('RESSSSSS', response.data.data.length);
      if (response.data.data.length === 0) {
        window.location.href = '/apps';
      } else {
        return false;
      }
    })
    .catch((error) => console.log('error', error));
}
