const ONE_MINUTE = { cache: { maxAge: 1 * 60 * 1000 } };
const FIVE_MINUTES = { cache: { maxAge: 5 * 60 * 1000 } };

export default {
  ONE_MINUTE,
  FIVE_MINUTES
};
