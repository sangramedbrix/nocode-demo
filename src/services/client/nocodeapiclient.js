import axios from 'axios';
import { setupCache } from 'axios-cache-adapter';
import { API_HOST } from 'config/environment';

export const apiEndpoint = API_HOST;

const cache = setupCache({
  maxAge: 0, //disabled by default
  exclude: {
    query: false
  }
});

const createAPIClient = (url) => {
  const clientApi = axios.create({
    baseURL: url,
    withCredentials: true,
    adapter: cache.adapter
  });

  clientApi.interceptors.response.use(
    function (response) {
      return response;
    },
    async function (error) {
      return Promise.reject(error);
    }
  );

  return clientApi;
};

export const extractResponse = async (call) => {
  const response = await call;
  return response.data;
};

const client = createAPIClient(API_HOST);
export default client;
