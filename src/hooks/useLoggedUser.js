import { useState } from 'react';
import { getStoredUser, storeUser } from '../storage/userStorage';

export default function useLoggedUser() {
  const getUser = () => {
    const tokenString = getStoredUser();
    return JSON.parse(tokenString);
  };

  const [storedUser, setStoredUser] = useState(getUser());

  const saveUser = (userData) => {
    storeUser(userData);
    setStoredUser(userData);
  };

  return {
    setLoggedUser: saveUser,
    loggedUser: storedUser
  };
}
