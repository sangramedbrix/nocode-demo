import React from 'react';
import PropTypes from 'prop-types';
import '../Cards/cards.css';
import { Menu, Dropdown, Row, Col, Button } from 'antd';

const menu = (
  <Menu className="list_menu">
    <Menu.Item>
      <a href=""> Delete </a>
    </Menu.Item>
    <Menu.Item>
      <a href=""> Edit </a>
    </Menu.Item>
  </Menu>
);

const RecentActivitiesList = (props) => {
  return (
    <>
      <div className="myRecentActivityList">
        <div className="activity_box">
          <div className="activity_box_text">
            <h3> {props.card_title} </h3>
            <p className="sub_title" style={{ width: 300 }}>
              {' '}
              {props.card_description}{' '}
            </p>
          </div>
          <div className="recent-apps-body">
            <div className="recent_activity_list">
              <div className="recent_activity_list_item">
                <Row>
                  <Col xs={3}>
                    <div className="icon-circle">
                      {/* <img src={props.card_icon} alt='img' /> */}
                      <i className="icon_file_green"></i>
                    </div>
                  </Col>
                  <Col xs={20} className="m-auto">
                    <h5> {props.card_header}</h5>
                    <p className="date-desc">{props.card_desc} </p>
                  </Col>
                  <Col xs={1} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
              </div>
              <div className="recent_activity_list_file_item">
                <Row>
                  <Col xs={3}>
                    <div className="icon-circle-light">
                      <i className="icon_page_blue"></i>
                    </div>
                  </Col>
                  <Col xs={20}>
                    <h5> Personal Inormation</h5>
                    <p className="date-desc">
                      Status:{' '}
                      <span className="activity_status_pending">{props.activity_status}</span>
                      {props.card_desc}{' '}
                    </p>
                  </Col>
                  <Col xs={1} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
              </div>
              <div className="recent_activity_list_item">
                <Row>
                  <Col xs={3}>
                    <div className="icon-circle">
                      {/* <img src={props.card_icon} alt='img' /> */}
                      <i className="icon_file_green"></i>
                    </div>
                  </Col>
                  <Col xs={20}>
                    <h5> {props.card_header}</h5>
                    <p className="date-desc">{props.card_desc} </p>
                  </Col>
                  <Col xs={1} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
              </div>
              <div className="recent_activity_list_file_item">
                <Row>
                  <Col xs={3}>
                    <div className="icon-circle-light">
                      <i className="icon_page_blue"></i>
                    </div>
                  </Col>
                  <Col xs={20}>
                    <h5> Personal Inormation</h5>

                    <p className="date-desc">
                      Status: <span className="activity_status_actionpending">Action Pending</span>
                      {props.card_desc}{' '}
                    </p>
                  </Col>
                  <Col xs={1} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
              </div>
              <div className="recent_activity_list_file_item">
                <Row>
                  <Col xs={3}>
                    <div className="icon-circle-light">
                      <i className="icon_page_blue"></i>
                    </div>
                  </Col>
                  <Col xs={20}>
                    <h5> Personal Inormation</h5>
                    <p className="date-desc">
                      Status: <span className="activity_status_inprogress">In Progress</span>
                      {props.card_desc}{' '}
                    </p>
                  </Col>
                  <Col xs={1} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
          <div className="btn-div-sticky text-center">
            <Button className="create-app-button-text2 create-app-button2">
              View All Activities
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default RecentActivitiesList;

RecentActivitiesList.propTypes = {
  card_title: PropTypes.string,
  card_description: PropTypes.string,
  card_header: PropTypes.string,
  card_desc: PropTypes.string,
  card_icon: PropTypes.string,
  activity_status: PropTypes.string
};
