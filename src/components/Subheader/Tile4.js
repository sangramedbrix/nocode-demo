import React from 'react';
import { Row, Col, Button } from 'antd';
import './style.css';

const Tile4 = () => {
  return (
    <>
      <Row className="tile4">
        <Col>
          {/* <div>
            <Button className='ext-btn'>Add Page</Button>
          </div> */}
          <div>
            <Button className="ext-btn">Preview App</Button>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Tile4;
