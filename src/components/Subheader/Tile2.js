import React from 'react';
import '../components.css';
import { Row, Col } from 'antd';
import ButtonRound from '../../components/Buttons/ButtonRound';
import './style.css';

const Tile2 = () => {
  return (
    <>
      <div className="vertical tile2">
        <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={12}>
            <div className="tile2-info">
              <div className="tile2-title">Active Theme</div>
              <div className="tile2-subtitle"> Hexaqua Marina</div>
            </div>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={12} className="btn-blue">
            <ButtonRound button_title="Change" button_size="small" />
          </Col>
        </Row>
        <div className="progress"></div>
      </div>
    </>
  );
};

export default Tile2;
