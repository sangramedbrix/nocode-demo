import React from 'react';
import { Row, Col, Button } from 'antd';
import './style.css';
import { useNavigate, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';

const Tile5 = (props) => {
  const navigate = useNavigate();
  const { appId } = useParams();
  console.log('props value', props);
  return (
    <>
      <Row className="tile5">
        <Col>
          <div>
            <Button className="ext-btn">Preview App</Button>
          </div>
          <div>
            <Button
              className="ext-btn"
              onClick={() => {
                navigate(`/app/${appId}/pages`);
              }}>
              View All Pages
            </Button>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Tile5;

Tile5.propTypes = {
  appId: PropTypes.String
};
