import React from 'react';
import PropTypes from 'prop-types';
import '../components.css';
import { Row, Col } from 'antd';
import './style.css';
import { useNavigate, useParams } from 'react-router-dom';
import DefaultAppImage from '../../assets/images/default_image2.png';

const Tile1 = (props) => {
  console.log('props from tile', props);
  const navigate = useNavigate();

  const addDefaultSrc = (ev) => {
    ev.target.src = DefaultAppImage;
  };
  const { appId } = useParams();
  console.log('APPIS', appId);

  return (
    <>
      {/* <div className='flex-container'>
        <div className='img'  >
          <img src={Sidelogo}  />
        </div>
        <div className='text-content'>
          <div class='title'>Student Management</div>
          <div className='subtitle'>By Josep Carr - 3rd December , 2021 </div>
          <div style={{display:'flex',justifyContent:'space-around',fontWeight:500}}>
            <div className=''>
              <p>1 Module</p>
            </div>
            <div className=''>
              <p>0 Page</p>
            </div>
          </div>
        </div>
      </div> */}
      <Row className="vertical tile1" gutter={[5, 25]}>
        <Col xl={9} lg={7} md={7} sm={7} xs={7}>
          <img onError={addDefaultSrc} src={props.appLogo} />
        </Col>
        <Col xl={15} lg={17} md={17} sm={17} xs={17}>
          <div className="tile1-info">
            <div className="title1-title ">{props.appName}</div>
            <div className="title1-subtitle ">
              by {props.userName} - {props.createdDate}
            </div>
            <div className="pc-user-title">
              <Row>
                <Col xl={12} lg={12} sm={12} xs={12}>
                  <div className="module">
                    <p
                      className="p-tag"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        navigate(`/app/${appId}/modules`);
                      }}>
                      {props.moduleCount} Modules
                    </p>
                  </div>
                </Col>
                <Col xl={12} lg={12} sm={12} xs={12}>
                  <div className="module">
                    <p
                      className="p-tag"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        navigate(`/app/${appId}/pages`);
                      }}>
                      {props.pageCount} Pages
                    </p>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
      </Row>
      <div className="hide"></div>
    </>
  );
};

export default Tile1;

Tile1.propTypes = {
  appName: PropTypes.string,
  userName: PropTypes.string,
  createdDate: PropTypes.string,
  appLogo: PropTypes.string,
  moduleCount: PropTypes.number,
  pageCount: PropTypes.number,
  appId: PropTypes.string,
  appData: PropTypes.func
};
