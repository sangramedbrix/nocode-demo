import React, { useState } from 'react';
import { Row, Col, Button, Modal, Form } from 'antd';
import '../../config/config';
import './style.css';
import InputText from '../InputsFields/InputText';
import PropTypes from 'prop-types';
import TextHeadingH4 from '../TextHeadings/TextHeadingH4';
import InputTextArea from '../InputsFields/InputTextArea';
import axios from 'axios';
import { useLocation } from 'react-router-dom';
import '../Popup/popup.css';
const Tile6 = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });

  const { state } = useLocation();
  console.log('new state in app created data in tile 6', state);

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    }
  };

  const createPageModule = () => {
    if (createPage.name != '') {
      axios
        .post(global.config.API_URL + '/createPage', {
          moduleId: props.moduleId,
          pageName: createPage.name,
          description: createPage.description
        })
        .then((response) => {
          console.log(response);

          if (response.data.message === 'Page created!') {
            setCreatePage((prevState) => ({ ...prevState, name: '', description: '' }));
            setIsModalVisible(false);
            props.pageList();
            props.getPageCount();
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      if (createPage.name == '') {
        setCreatePage((prevState) => ({ ...prevState, nameValidation: true }));
      }
    }
  };

  return (
    <>
      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          destroyOnClose={true}
          className="create_page_popup"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger" style={{ marginRight: '214px' }}>
                  {' '}
                  Page name is required
                </p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={createPage.description}
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancel}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={createPageModule}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      <Row className="tile5">
        <Col>
          {/* {state.appId === 0 ? (
            <div>
              <Button className="ext-btn" onClick={showModal}>
                Add Page
              </Button>
            </div>
          ) : (
            ''
          )} */}

          <div>
            <Button className="ext-btn">Preview App</Button>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Tile6;

Tile6.propTypes = {
  appId: PropTypes.string,
  moduleId: PropTypes.string,
  pageList: PropTypes.func,
  getPageCount: PropTypes.func
};
