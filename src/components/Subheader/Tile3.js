import React from 'react';
import '../components.css';
import './style.css';
import { Row, Col } from 'antd';
import ButtonRound from '../../components/Buttons/ButtonRound';
import Template from '../../assets/images/template.png';

const Tile3 = () => {
  return (
    <>
      <div className="tile3-info tile3">
        <div className="tile3-title">App Template</div>
        <Row gutter={[]}>
          <Col xl={5} lg={5} md={5} sm={5} xs={8}>
            <img src={Template} alt="template" className="temp-img" />
          </Col>
          <Col xl={14} lg={12} md={12} sm={12} xs={8}>
            <div className="pc-user-info">
              <div className="tile3-subtitle">
                <p>Sidemenu - Standard </p>
              </div>
            </div>
          </Col>
          <Col xl={5} lg={5} md={5} sm={5} xs={8} className="btn-blue top-align">
            <ButtonRound button_title="Change" button_size="small" />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Tile3;
