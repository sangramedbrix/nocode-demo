// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Row, Col } from 'antd';
import './images.css';

const ImageLoginBsnner50_50 = () => {
  return (
    <>
      <Row>
        <Col span={12}></Col>
        <Col span={12}>
          <img src={PropTypes.img_src} />
        </Col>
      </Row>
    </>
  );
};

export default ImageLoginBsnner50_50;
