// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import './images.css';

const ImageLogoLarge = () => {
  return (
    <>
      <img src={PropTypes.img_src} style={{ width: '350px' }} />
    </>
  );
};

export default ImageLogoLarge;
