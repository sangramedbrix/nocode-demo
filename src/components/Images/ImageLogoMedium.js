// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import './images.css';

const ImageLogoMedium = () => {
  return (
    <>
      <img src={PropTypes.img_src} style={{ width: '250px' }} />
    </>
  );
};

export default ImageLogoMedium;
