// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import './images.css';

const ImageLogoSmall = () => {
  return (
    <>
      <img src={PropTypes.img_src} style={{ width: '150px' }} />
    </>
  );
};

export default ImageLogoSmall;
