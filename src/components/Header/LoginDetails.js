import React from 'react';
import 'antd/dist/antd.css';
import './header.css';
import Loggedin from '../../assets/images/userprofile.png';

const LoginDetails = () => {
  let userData = JSON.parse(localStorage.getItem('isAuthenticated'));
  var params = JSON.parse(JSON.stringify(userData));
  console.log('userData', params[0].email);
  return (
    <>
      <div className="user-profile">
        <div className="user-details">
          <div className="bio">Welcome</div>
          <div className="username">{params[0].firstName + ' ' + params[0].lastName}</div>
        </div>
        <img className="avatar" src={Loggedin} alt="Ash" />
      </div>
    </>
  );
};

export default LoginDetails;
