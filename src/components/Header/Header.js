import React from 'react';
import 'antd/dist/antd.css';
import Logo from '../../assets/images/logo.png';
import './header.css';
import LoginDetails from './LoginDetails';
import BellIcon from './BellIcon';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <>
      <div className="nav">
        <div className="nav-header">
          <div className="nav-title">
            <Link to="/home">
              <img src={Logo} />
            </Link>
          </div>
        </div>
        <div className="nav-links">
          <div className="d-flex">
            <BellIcon />
          </div>
          <div>
            <LoginDetails />
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
