import React from 'react';
import { BellOutlined } from '@ant-design/icons';
import './header.css';
import { Divider } from 'antd';

const BellIcon = () => {
  return (
    <div className="bell-container">
      <Divider type="vertical" className="vertical-line-bell"></Divider>
      <BellOutlined className="bell" />
      <Divider type="vertical" className="vertical-line-bell"></Divider>
      {/* <div className="bell vl">
          <BellOutlined />
        </div> */}
    </div>
  );
};

export default BellIcon;
