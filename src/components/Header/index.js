import React from 'react';
import Navbar from './Header';
import axios from 'axios';

const Headers = () => {
  axios.interceptors.request.use(
    function (config) {
      // Do something before request is sent
      console.log('Start Ajax Call');
      document.getElementById('ajaxloader').className = 'show';
      return config;
    },
    function (error) {
      // Do something with request error
      console.log('Error');
      document.getElementById('ajaxloader').className = 'hide';
      return Promise.reject(error);
    }
  );

  axios.interceptors.response.use(
    function (response) {
      // Do something with response data
      console.log('Done with Ajax call');

      document.getElementById('ajaxloader').className = 'hide';

      return response;
    },
    function (error) {
      // Do something with response error
      console.log('Error fetching the data');
      document.getElementById('ajaxloader').className = 'hide';
      return Promise.reject(error);
    }
  );

  return (
    <>
      <div id="ajaxloader">Please wait...</div>
      <Navbar />
    </>
  );
};

export default Headers;
