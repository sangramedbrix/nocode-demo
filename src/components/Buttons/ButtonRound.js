import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import './button.css';
import { Button } from 'antd';

const ButtonRound = (props) => {
  return (
    <>
      <Button
        type={props.button_color}
        size={props.button_size}
        shape="round"
        className="Button_small_round mar-2">
        {props.button_title}
      </Button>
    </>
  );
};

export default ButtonRound;

ButtonRound.propTypes = {
  button_color: PropTypes.string,
  button_size: PropTypes.string,
  button_title: PropTypes.string
};
