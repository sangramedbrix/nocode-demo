import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import './button.css';
import '../theme.css';
import { Button } from 'antd';

const ButtonOutline = (props) => {
  return (
    <>
      <Button type={props.button_color} className="mar-2" size={props.button_size} ghost>
        {props.button_title}
      </Button>
    </>
  );
};

export default ButtonOutline;

ButtonOutline.propTypes = {
  button_color: PropTypes.string,
  button_size: PropTypes.string,
  button_title: PropTypes.string
};
