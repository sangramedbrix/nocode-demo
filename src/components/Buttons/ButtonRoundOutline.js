// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import './button.css';
import { Button } from 'antd';
import { useNavigate } from 'react-router-dom';

const ButtonRoundOutline = (props) => {
  const navigate = useNavigate();
  return (
    <>
      <Button
        onClick={() => {
          navigate('/apps', { state: { appListActive: 2 } });
        }}
        type={props.button_color}
        size={props.button_size}
        shape="round"
        className="Button_small_round_outline mar-2"
        ghost>
        {props.button_title}
      </Button>
    </>
  );
};

export default ButtonRoundOutline;

ButtonRoundOutline.propTypes = {
  button_color: PropTypes.string,
  button_size: PropTypes.string,
  button_title: PropTypes.string
};
