// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import './button.css';
import { Button } from 'antd';
import '../theme.css';

const ButtonFull = (props) => {
  return (
    <>
      <Button type={props.button_color} className="Button_full" block>
        {props.button_title}
      </Button>
    </>
  );
};

export default ButtonFull;

ButtonFull.propTypes = {
  button_color: PropTypes.string,
  button_title: PropTypes.string
};
