import React from 'react';
import PropTypes from 'prop-types';
import './button.css';
import 'antd/dist/antd.css';
import { Button } from 'antd';

const Buttons = (props) => {
  console.log('button props', props);

  const submitApp = () => {
    props.pageCanelYes();
  };

  return (
    <>
      <Button
        type={props.button_color}
        size={props.button_size}
        className="Buton_small"
        onClick={submitApp}
        // onClick={props.submitApp}
      >
        {props.button_title}
      </Button>
    </>
  );
};
export default Buttons;

Buttons.propTypes = {
  button_color: PropTypes.string,
  button_size: PropTypes.string,
  submitApp: PropTypes.func,
  pageCanelYes: PropTypes.func,
  button_title: PropTypes.string
};
