import React, { useState, useEffect } from 'react';
import '../../config/config';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import './cards.css';
import { DeleteOutlined } from '@ant-design/icons';
import { Menu, Dropdown, Row, Col, Modal, Checkbox, Button, Select } from 'antd';
import axios from 'axios';
import moment from 'moment';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import Dropdowns from '../../components/Dropdowns/Dropdowns';
import InputFile from '../../components/InputsFields/InputFile';
import AppImageBanner from '../../assets/images/app_banner.png';
import TextHeadingH3 from '../TextHeadings/TextHeadingH3';
import '../../assets/styles/index.css';

const { Option } = Select;

const RecentAppsBox = (props) => {
  // console.log('PROPS ARE====>>>>',props)
  const [apps, setApps] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [authUser, setAuthUser] = useState([]);
  const [appId, setAppId] = useState();
  const [isModalVisibleApp, setIsModalVisibleApp] = useState(false);
  const [app, setApp] = useState([]);
  const [appCategory, setAppCategory] = useState([]);
  const [companyId, setCompanyId] = useState();
  const [checked, setChecked] = useState(false);
  const [deleteAppName, setDeleteAppName] = useState();
  const [createNewApp, setCreateNewApp] = useState({
    name: '',
    category: '',
    description: '',
    isActive: 0,
    file: [],
    originalFileName: '',
    nameValidation: '',
    categoryValidation: '',
    appTypeValidation: '',
    appType: '',
    descriptionValidation: '',
    imageValidation: ''
  });

  const navigate = useNavigate();
  const getApps = () => {
    axios
      .get(global.config.API_URL + '/appDashboard')
      .then((response) => {
        console.log('response apps data', response.data.data);

        setApps(response.data.data);

        //  setCount(array)
      })
      .catch((error) => console.log(error));
  };

  const handleOk = () => {
    setIsModalVisibleApp(false);
  };

  const handleCancelApp = () => {
    setIsModalVisibleApp(false);
  };

  const selectFile = (file) => {
    console.log('file', file);
    setCreateNewApp((prevState) => ({ ...prevState, file: file, imageValidation: false }));
  };

  const deleteApp = (appId, appName) => {
    console.log(appId);
    setAppId(appId);
    setDeleteAppName(appName);
    setIsModalVisible(true);
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deleteApp/${appId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisible(false);
          getApps();
        }
      })
      .catch((error) => console.log(error));
  };

  const editApp = (appId) => {
    console.log(appId);
    axios
      .post(global.config.API_URL + `/getDetailById`, { id: appId, tableName: 'apps' })
      .then((response) => {
        console.log(response);
        setApp(response.data.data);
        setCreateNewApp((prevState) => ({
          ...prevState,
          name: response.data.data.name,
          description: response.data.data.description,
          originalFileName: response.data.data.logoFileName,
          isActive: response.data.data.isActive,
          appType: response.data.data.appType,
          nameValidation: false,
          descriptionValidation: false
        }));
        if (response.data.data.isActive === 1) {
          setChecked(true);
        } else {
          setChecked(false);
        }
      })
      .catch((error) => console.log(error));

    setTimeout(() => {
      setIsModalVisibleApp(true);
    }, 1000);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const nameChange = (name) => {
    console.log('app name', name);
    if (name != '') {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };
  const categoryChange = (category) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      category: category,
      categoryValidation: false
    }));
  };

  const handleChangeApplicationType = (event) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      appType: event,
      appTypeValidation: false
    }));
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));
      console.log('empty');
    }
  };

  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      setChecked(true);
      setCreateNewApp((prevState) => ({ ...prevState, isActive: 1 }));
    } else {
      setChecked(false);
      setCreateNewApp((prevState) => ({ ...prevState, isActive: 0 }));
    }
  };

  const getAppCategory = () => {
    axios
      .get(global.config.API_URL + '/getAppCategory')
      .then((response) => {
        console.log(response);
        setAppCategory(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const onUpdate = () => {
    console.log(app._id);
    console.log(app.logoFileName);
    const formData = new FormData();
    setIsModalVisibleApp(false);
    formData.append('name', createNewApp.name);
    formData.append('categoryId', '61debe2c5cae3e30d6c1fb1b');
    formData.append('description', createNewApp.description);
    formData.append('isActive', createNewApp.isActive);

    if (createNewApp.file.length == 0) {
      const originalImage = app.logoFileName;
      formData.append('checkImage', 0);
      formData.append('logoFileName', originalImage);
    } else if (createNewApp.file.length == undefined) {
      formData.append('checkImage', 1);
      formData.append('logoFileName', createNewApp['file'].file.originFileObj);
    }
    formData.append('companyId', companyId);
    //localhost:3000/getAppCategory
    // global.config.API_URL + '/createNewApp
    axios
      .put(global.config.API_URL + `/updateApp/${app._id}`, {
        name: createNewApp.name,
        categoryId: '61debe2c5cae3e30d6c1fb1b',
        appType: createNewApp.appType,
        category: createNewApp.category,
        updatedUserId: authUser._id,
        description: createNewApp.description,
        isActive: createNewApp.isActive
      })
      .then((response) => {
        console.log(response);
        updateImage();
        setIsModalVisibleApp(false);
        getApps();
      })
      .catch((error) => console.log(error));

    // if (createNewApp.name != '' && createNewApp.category != '') {
    //   const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    //   const formData = new FormData();
    //   formData.append('image', createNewApp['file'].file.originFileObj);
    //   formData.append('name', createNewApp.name);
    //   formData.append('description', createNewApp.description);
    //   formData.append('category', createNewApp.category);
    //   formData.append('isActive', createNewApp.isActive);
    //   formData.append('companyId', data[0].companyId);

    //   axios
    //     .post(global.config.API_URL + '/createNewApp', formData)
    //     .then((response) => {
    //       console.log(response);
    //     })
    //     .catch((error) => console.log(error));
    // } else {
    //   if (createNewApp.name == '') {
    //     setCreateNewApp((prevState) => ({ ...prevState, nameValidation: true }));
    //   } else if (createNewApp.category == '') {
    //     setCreateNewApp((prevState) => ({ ...prevState, categoryValidation: true }));
    //   }
    // }
  };

  const updateImage = () => {
    const formData = new FormData();
    if (createNewApp.file.length == 0) {
      const originalImage = app.logoFileName;
      formData.append('checkImage', 0);
      formData.append('image', originalImage);
    } else if (createNewApp.file.length == undefined) {
      formData.append('checkImage', 1);
      formData.append('image', createNewApp['file'].file.originFileObj);
      formData.append('logoFileName', app.logoFileName);
    }

    formData.append('companyId', companyId);
    formData.append('id', app._id);

    axios
      .post(global.config.API_URL + `/updateImage`, formData)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getApps();
    getAppCategory();

    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setAuthUser(data[0]);
    setCompanyId(data[0].companyId);
  }, []);

  return (
    <>
      {/* delete popup */}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3={`You are about to delete the '${deleteAppName}' app`} />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version.
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* modal for create your own app */}
      <div>
        <Modal
          destroyOnClose={true}
          className="modal-container"
          width={'100%'}
          height={'100%'}
          style={{ top: 0 }}
          header={false}
          footer={false}
          visible={isModalVisibleApp}
          onOk={handleOk}
          onCancel={handleCancelApp}>
          <div className="flex-container bg-light-blue">
            <div className="containers">
              <div className="createApp-form-box">
                <div className="box-title">
                  <div className="headertext text-center">Edit your app...</div>
                  {/* <div className='subheadertxt text-center'>
                    No code App Maker to Build Your App
                  </div> */}
                </div>
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>App Name
                  </div>
                  <InputText
                    placeholder_text="Enter App Name"
                    nameChangeFunction={nameChange}
                    value={app.name}
                  />
                </div>
                <div className=" bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>Category
                  </div>
                  <Dropdowns
                    option={appCategory}
                    dropdown_title="Select"
                    value={createNewApp.category}
                    categoryId={app.categoryId}
                    categoryChangeFunction={categoryChange}
                  />
                </div>
                <div className=" bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>Application Type
                  </div>
                  <Select
                    className="dd-box"
                    defaultValue={createNewApp.appType === 1 ? 'Web' : 'Mobile'}
                    onChange={handleChangeApplicationType}>
                    <Option value="1">Web</Option>
                    <Option value="2">Mobile</Option>
                  </Select>
                </div>

                <div className="mtb-1 bolder">
                  <div className="createAppSubtxt">Description</div>
                  <InputTextArea
                    placeholder_text=""
                    discriptionChangeFunction={discriptionChange}
                    value={app.description}
                  />
                </div>
                <div className="mtb-1 ">
                  <div className="createAppSubtxt bolder">Status</div>
                  <div className="checkbox2">
                    {' '}
                    <Checkbox size="small" checked={checked} onChange={onChange}></Checkbox>
                    <span style={{ marginLeft: 8 }}> Yes, I agree to show my App to public</span>
                  </div>
                </div>
                <div className="mtb-2 ">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span> App Icon
                  </div>
                  <div className="text-input-upload">
                    <InputFile text="No choosen file" fileSelect={selectFile} />
                  </div>
                </div>

                <Row gutter={[70, 0]}>
                  <Col xl={1} lg={1} md={1} sm={1} xs={1}>
                    {/* <img
                        src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
                      /> */}
                  </Col>
                </Row>

                <div className="mtb-1 btn-div">
                  <div className="app-link">
                    <Button onClick={handleCancelApp}>Cancel</Button>
                  </div>
                  <div className="app-button">
                    <Button onClick={onUpdate}>Update</Button>
                    {/* <Buttons button_title='Select Theme' button_size='medium'  onClick={onSubmit} className='app-button-text' /> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="appimgcontainer">
              <div className="create-banner-text">
                <div className="banner-titletop">App Builder to build apps</div>
                <div className="banner-subtitletop">in five minutes without Code </div>
              </div>
              <div>
                <div className="side-img text-center">
                  <img className="m-auto" width="70%" alt="NoCode" src={AppImageBanner} />
                </div>
                <div className="create-banner-text">
                  <div className="banner-title text-center">Design. Build. Launch</div>
                  <div className="banner-subtitle">
                    Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code
                    Required!!!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
      {/* end  modal for create your own app */}

      <div className="RecentAppsBox">
        <div className="app_box">
          <div className="app_box_text">
            <h3> {props.card_title} </h3>
            <p className="sub_title"> {props.card_description} </p>
          </div>
          <div className="recent-apps-body">
            <div className="img_div">
              <img src={props.card_img_src} className="mtb-4" width="100%" height="100%" />
            </div>
            <div className="recent_apps_list">
              {apps.map((app, i) => {
                return (
                  <div key={i} className="recent_apps_list_item">
                    {/* {getPageCount(app._id)} */}

                    <Row>
                      <Col xs={22}>
                        <div
                          className="card-title-header"
                          onClick={() => navigate('/app/modules', { state: app })}>
                          {' '}
                          {app.name}{' '}
                        </div>
                      </Col>
                      <Col xs={2} className="option-dots-center">
                        <Dropdown
                          className="dropdown_list"
                          placement="bottomRight"
                          overlay={(() => {
                            return (
                              <Menu className="list_menu">
                                <Menu.Item>
                                  <a onClick={() => editApp(app._id)}> Edit </a>
                                </Menu.Item>
                                <Menu.Item>
                                  <a onClick={() => deleteApp(app._id, app.name)}> Delete </a>
                                </Menu.Item>
                              </Menu>
                            );
                          })()}>
                          <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                            <i className="icon_more"></i>
                          </a>
                        </Dropdown>
                      </Col>
                    </Row>
                    <p className="page-count">
                      {/* <FileOutlined /> {appCounts.appPageCount}{' '} */}
                    </p>
                    <div className="date-desc">
                      Last Edited On : {moment(app.updatedAt).format('DD/MM/YYYY h:mm:ss A')}
                    </div>
                  </div>
                );
              })}
              {/* onClick={() => {
                navigate('/apps', { state: { appListActive: 2 } });
              }} */}
            </div>
          </div>
          <div className="btn-div-sticky text-center">
            <Button
              onClick={() => {
                navigate('/apps', { state: { appListActive: 2 } });
              }}
              className="create-app-button-text2 create-app-button2">
              View All Apps
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default RecentAppsBox;

RecentAppsBox.propTypes = {
  card_title: PropTypes.string,
  card_description: PropTypes.string,
  card_img_src: PropTypes.string,
  recent_apps_list_item_heading: PropTypes.string,
  recent_apps_list_item_page: PropTypes.string,
  recent_apps_list_item_date: PropTypes.string
};
