import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import '../../config/config';
import { UserOutlined, AntDesignOutlined, DeleteOutlined } from '@ant-design/icons';
import { Row, Col, Avatar, Divider, Tooltip, Modal, Button, Menu, Dropdown, Select } from 'antd';
import './cards.css';
import '../../config/config';
import { useNavigate } from 'react-router-dom';
import TextHeadingH3 from '../TextHeadings/TextHeadingH3';
import '../Popup/popup.css';
import axios from 'axios';
import InputText from '../../components/InputsFields/InputText';
import InputTextArea from '../../components/InputsFields/InputTextArea';
import InputFile from '../../components/InputsFields/InputFile';

import Dropdowns from '../../components/Dropdowns/Dropdowns';
import AppImageBanner from '../../assets/images/app_banner.png';
import DefaultAppImage from '../../assets/images/default_image2.png';
import '../../assets/styles/index.css';
import { Checkbox } from 'antd';

const { Option } = Select;

const AppsGridView = (props) => {
  console.log('props', props);
  const navigate = useNavigate();
  const [appCounts, setPerson] = useState({ appModuleCount: 0, appPageCount: 0 });
  const [authUser, setAuthUser] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleApp, setIsModalVisibleApp] = useState(false);
  const [appId, setAppId] = useState();
  const [app, setApp] = useState([]);
  const [appCategory, setAppCategory] = useState([]);
  const [checked, setChecked] = useState(false);
  const [companyId, setCompanyId] = useState();
  const [deleteAppName, setDeleteAppName] = useState();

  const [createNewApp, setCreateNewApp] = useState({
    name: '',
    category: '',
    description: '',
    isActive: 0,
    file: [],
    originalFileName: '',
    nameValidation: '',
    categoryValidation: '',
    appTypeValidation: '',
    appType: '',
    descriptionValidation: '',
    imageValidation: ''
  });

  const getModuleCount = () => {
    axios
      .post(global.config.API_URL + '/getAllPages', {
        appId: props.appData._id,
        moduleId: 0
      })
      .then((response) => {
        console.log('PAGE DATA', response.data.data.length);
        setPerson((prevPerson) => {
          return {
            ...prevPerson,
            appPageCount: response.data.data.length
          };
        });
      })
      .catch((error) => console.log(error));

    axios
      .post(global.config.API_URL + '/getModuleCount', { appId: props.appData._id })
      .then((countValue) => {
        console.log('MODULE COUNT', countValue.data.data[0].count);
        setPerson((prevPerson) => {
          return {
            ...prevPerson,
            appModuleCount: countValue.data.data[0].count
          };
        });
      })
      .catch((error) => console.log(error));
  };

  const categoryChange = (category) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      category: category,
      categoryValidation: false
    }));
  };

  const getCategoryById = (categoryId) => {
    axios
      .post(global.config.API_URL + '/getCategoryById', { id: categoryId })
      .then((response) => {
        console.log('category response', response);
        setCreateNewApp((prevState) => ({
          ...prevState,
          category: response.data.data.name
        }));
      })
      .catch((error) => console.log(error));
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setCreateNewApp((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: true
      }));
      console.log('empty');
    }
  };

  const getAppCategory = () => {
    axios
      .get(global.config.API_URL + '/getAppCategory')
      .then((response) => {
        console.log(response);
        setAppCategory(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const deleteApp = (appId, appName) => {
    console.log(appId);
    setAppId(appId);
    setDeleteAppName(appName);
    setIsModalVisible(true);
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deleteApp/${appId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisible(false);
          props.getApps();
        }
      })
      .catch((error) => console.log(error));
  };

  const editApp = (appId) => {
    console.log(appId);

    axios
      .post(global.config.API_URL + `/getDetailById`, { id: appId, tableName: 'apps' })
      .then((response) => {
        console.log('edit app response', response);
        setApp(response.data.data);
        getCategoryById(response.data.data.categoryId);
        setCreateNewApp((prevState) => ({
          ...prevState,
          name: response.data.data.name,
          description: response.data.data.description,
          appType: response.data.data.appType,
          originalFileName: response.data.data.logoFileName,
          isActive: response.data.data.isActive,
          nameValidation: false,
          descriptionValidation: false
        }));
        if (response.data.data.isActive === 1) {
          setChecked(true);
        } else {
          setChecked(false);
        }
      })
      .catch((error) => console.log(error));

    setTimeout(() => {
      setIsModalVisibleApp(true);
    }, 1000);
  };
  const nameChange = (name) => {
    console.log('app name', name);
    if (name != '') {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreateNewApp((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const handleChangeApplicationType = (event) => {
    setCreateNewApp((prevState) => ({
      ...prevState,
      appType: event,
      appTypeValidation: false
    }));
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleOk = () => {
    setIsModalVisibleApp(false);
  };

  const handleCancelApp = () => {
    setIsModalVisibleApp(false);
  };

  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      setChecked(true);
      setCreateNewApp((prevState) => ({ ...prevState, isActive: 1 }));
    } else {
      setChecked(false);
      setCreateNewApp((prevState) => ({ ...prevState, isActive: 0 }));
    }
  };

  const selectFile = (file) => {
    console.log('file', file);
    setCreateNewApp((prevState) => ({ ...prevState, file: file, imageValidation: false }));
  };

  const onUpdate = () => {
    console.log(createNewApp);
    console.log(app.logoFileName);
    const formData = new FormData();

    formData.append('name', createNewApp.name);
    formData.append('categoryId', '61debe2c5cae3e30d6c1fb1b');
    formData.append('description', createNewApp.description);
    formData.append('isActive', createNewApp.isActive);

    if (createNewApp.file.length == 0) {
      const originalImage = app.logoFileName;
      formData.append('checkImage', 0);
      formData.append('logoFileName', originalImage);
    } else if (createNewApp.file.length == undefined) {
      formData.append('checkImage', 1);
      formData.append('logoFileName', createNewApp['file'].file.originFileObj);
    }
    formData.append('companyId', companyId);
    // global.config.API_URL + '/updateApp/
    axios
      .put(global.config.API_URL + `/updateApp/${app._id}`, {
        name: createNewApp.name,
        category: createNewApp.category,
        appType: createNewApp.appType,
        updatedUserId: authUser._id,
        description: createNewApp.description,
        isActive: createNewApp.isActive
      })
      .then((response) => {
        console.log(response);
        updateImage();
        setIsModalVisibleApp(false);
        props.getApps();
        // window.location.reload();
      })
      .catch((error) => console.log(error));
  };

  const addDefaultSrc = (ev) => {
    ev.target.src = DefaultAppImage;
  };

  const updateImage = () => {
    const formData = new FormData();
    if (createNewApp.file.length == 0) {
      const originalImage = app.logoFileName;
      formData.append('checkImage', 0);
      formData.append('image', originalImage);
    } else if (createNewApp.file.length == undefined) {
      formData.append('checkImage', 1);
      formData.append('image', createNewApp['file'].file.originFileObj);
      formData.append('logoFileName', app.logoFileName);
    }

    formData.append('companyId', companyId);
    formData.append('id', app._id);

    axios
      .post(global.config.API_URL + `/updateImage`, formData)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getAppCategory();
    getModuleCount();
    const data = JSON.parse(localStorage.getItem('isAuthenticated'));
    setAuthUser(data[0]);
    setCompanyId(data[0].companyId);
  }, []);

  return (
    <>
      {/* delete popup */}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3={`You are about to delete the '${deleteAppName}' app`} />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version.
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="cancel" onClick={() => handleCancel()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* <div className='create_module'>
        <Modal
          visible={isModalVisible}
          className='delete_popup'
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className='confirm_box'>
            <div className='icon'>
              <WarningOutlined className='d-flex text-center' />
            </div>
            <div className='heading'>
              <TextHeadingH3 text_h3='Are you sure you want to delete this app?' />
            </div>
            <div className='description'>
              <p>{props.popup_description} </p>
            </div>
            <div className='buttons_div d-flex text-center'>
              <Button
                type='primary'
                size='large'
                className='Buton_small'
                onClick={() => deleteFunctionCall()}>
                'Yes!
              </Button>
              <Button
                type='danger'
                size='large'
                className='Buton_small'
                onClick={() => handleCancel()}>
                No, Go Back!
              </Button>
            </div>
          </div>
        </Modal>
      </div> */}

      {/* modal for create your own app */}
      <div>
        <Modal
          className="modal-container"
          width={'100%'}
          height={'100%'}
          style={{ top: 0 }}
          header={false}
          footer={false}
          visible={isModalVisibleApp}
          onOk={handleOk}
          onCancel={handleCancelApp}>
          <div className="flex-container bg-light-blue">
            <div className="containers">
              <div className="createApp-form-box">
                <div className="box-title">
                  <div className="headertext text-center">Edit your app...</div>
                  {/* <div className='subheadertxt text-center'>
                    No code App Maker to Build Your App
                  </div> */}
                </div>
                <div className="mtb-2 bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>App Name
                  </div>
                  <InputText
                    placeholder_text="Enter App Name"
                    nameChangeFunction={nameChange}
                    value={app.name}
                  />
                </div>
                <div className="bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>Category
                  </div>
                  <Dropdowns
                    option={appCategory}
                    dropdown_title="Select"
                    value={createNewApp.category}
                    categoryId={app.categoryId}
                    categoryChangeFunction={categoryChange}
                  />
                </div>
                <div className=" bolder">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span>Application Type
                  </div>
                  <Select
                    className="dd-box"
                    defaultValue={createNewApp.appType === 1 ? 'Web' : 'Mobile'}
                    onChange={handleChangeApplicationType}>
                    <Option value="1">Web</Option>
                    <Option value="2">Mobile</Option>
                  </Select>
                </div>

                <div className="mtb-1 bolder">
                  <div className="createAppSubtxt">Description</div>
                  <InputTextArea
                    // placeholder_text=''
                    discriptionChangeFunction={discriptionChange}
                    value={app.description}
                  />
                </div>
                <div className="mtb-1 ">
                  <div className="createAppSubtxt bolder">Status</div>
                  <div className="checkbox2">
                    {' '}
                    <Checkbox size="small" checked={checked} onChange={onChange}></Checkbox>
                    <span style={{ marginLeft: 8 }}> Yes, I agree to show my App to public</span>
                  </div>
                </div>
                <div className="mtb-2 ">
                  <div className="createAppSubtxt">
                    <span style={{ color: 'red' }}>*</span> App Icon
                  </div>
                  <div className="text-input-upload">
                    <InputFile text="No choosen file" fileSelect={selectFile} />
                  </div>
                </div>

                <Row gutter={[70, 0]}>
                  <Col xl={1} lg={1} md={1} sm={1} xs={1}>
                    {/* <img
                        src={global.config.API_URL + `/uploads/company/${companyId}/app/${app._id}/logo/${app.logoFileName}`}
                      /> */}
                  </Col>
                </Row>

                <div className="mtb-1 btn-div">
                  <div className="app-link">
                    <Button onClick={handleCancelApp}>Cancel</Button>
                  </div>
                  <div className="app-button">
                    <Button onClick={onUpdate}>Update</Button>
                    {/* <Buttons button_title='Select Theme' button_size='medium'  onClick={onSubmit} className='app-button-text' /> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="appimgcontainer">
              <div className="create-banner-text">
                <div className="banner-titletop">App Builder to build apps</div>
                <div className="banner-subtitletop">in five minutes without Code </div>
              </div>
              <div>
                <div className="side-img text-center">
                  <img className="m-auto" width="70%" alt="NoCode" src={AppImageBanner} />
                </div>
                <div className="create-banner-text">
                  <div className="banner-title text-center">Design. Build. Launch</div>
                  <div className="banner-subtitle">
                    Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code
                    Required!!!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
      {/* end  modal for create your own app */}

      <div className="AppsGridView ">
        <div hovreable className="card">
          <div className="card_body">
            <div className="card_img">
              <img
                onError={addDefaultSrc}
                src={props.app_grid_icon}
                className="set"
                alt="cardTop"
              />
            </div>
            <div className="card_text">
              {/* &#10247; */}

              <Row>
                <Col xs={23}>
                  <h3> {props.card_title} </h3>
                </Col>
                <Col xs={1} className="option-dots mar-ad">
                  <Dropdown
                    placement="bottomRight"
                    overlay={(() => {
                      return (
                        <Menu className="list_menu">
                          <Menu.Item>
                            <a onClick={() => editApp(props.appData._id)}> Edit </a>
                          </Menu.Item>
                          <Menu.Item>
                            <a onClick={() => deleteApp(props.appData._id, props.appData.name)}>
                              {' '}
                              Delete{' '}
                            </a>
                          </Menu.Item>
                        </Menu>
                      );
                    })()}>
                    <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                      <i className="icon_more"></i>
                    </a>
                  </Dropdown>
                </Col>
              </Row>
              <div className="sub_title">
                <p> {props.app_grid_date} </p>
              </div>
              <div className="stetus_box">
                <p className="p-tag">
                  <Avatar.Group
                    maxCount={2}
                    maxPopoverTrigger="click"
                    size="large"
                    maxStyle={{
                      color: '#f56a00',
                      backgroundColor: '#fde3cf',
                      cursor: 'pointer'
                    }}>
                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    <Avatar
                      style={{
                        backgroundColor: '#f56a00'
                      }}>
                      K
                    </Avatar>
                    <Tooltip title="Ant User" placement="top">
                      <Avatar
                        style={{
                          backgroundColor: '#87d068'
                        }}
                        icon={<UserOutlined />}
                      />
                    </Tooltip>
                    <Avatar
                      style={{
                        backgroundColor: '#1890ff'
                      }}
                      icon={<AntDesignOutlined />}
                    />
                  </Avatar.Group>
                </p>
                {/* <p className="inprogress">
                  <p className="p-tag">{props.status} </p>
                </p> */}
                <p style={{ color: props.color }}>
                  <p className="p-tag">{props.status} </p>
                </p>
              </div>
            </div>
            <Divider />
            <div className="card_footer">
              {/* <Link to={{pathname:'/app/module/list',state:'fsd'}}> */}
              <div
                className="modules"
                onClick={() => {
                  navigate(`/app/${props.appData._id}/modules`, { state: props.appData });
                }}>
                {<i className="icon_modules"> </i>}
                <p className="p-tag"> {appCounts.appModuleCount} Modules </p>
              </div>
              {/* </Link> */}
              <div
                className="page"
                onClick={() => {
                  navigate(`/app/${props.appData._id}/pages`, {
                    state: { data: props.appData, from: 'app list', moduleId: 0 }
                  });
                }}>
                {<i className="icon_page"> </i>}
                <p className="p-tag"> {appCounts.appPageCount} Pages </p>
              </div>
              <div className="comment ">
                {<i className="icon_comment"> </i>}
                <p className="p-tag"> {props.footer_comments} </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AppsGridView;

AppsGridView.propTypes = {
  app_grid_icon: PropTypes.string,
  card_title: PropTypes.string,
  status: PropTypes.string,
  app_grid_date: PropTypes.string,
  footer_module: PropTypes.string,
  footer_page: PropTypes.string,
  footer_comments: PropTypes.string,
  appData: PropTypes.array,
  getApps: PropTypes.func,
  popup_description: PropTypes.string,
  appId: PropTypes.string,
  color: PropTypes.string
};
