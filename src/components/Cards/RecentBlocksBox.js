import React from 'react';
import PropTypes from 'prop-types';
// import { useNavigate } from 'react-router-dom';
import './cards.css';
import { Menu, Dropdown, Row, Col, Button } from 'antd';
import { useNavigate } from 'react-router-dom';
// const navigate = useNavigate();
const menu = (
  <Menu className="list_menu">
    <Menu.Item>
      <a href=""> Delete </a>
    </Menu.Item>
    <Menu.Item>
      <a href=""> Edit </a>
    </Menu.Item>
  </Menu>
);

const RecentBlocksBox = (props) => {
  const navigate = useNavigate();
  return (
    <>
      <div className="RecentAppsBox">
        <div className="app_box">
          <div className="app_box_text">
            <h3> {props.card_title} </h3>
            <p className="sub_title"> {props.card_description} </p>
          </div>
          <div className="recent-apps-body">
            <div className="img_div">
              <img src={props.card_img_src} className="mtb-4" width="100%" height="100%" />
            </div>
            <div className="recent_apps_list">
              <div className="recent_apps_list_item">
                <Row>
                  <Col xs={22}>
                    <div className="card-title-header"> Block 1 </div>
                  </Col>
                  <Col xs={2} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
                {/* <p className='page-count'><FileOutlined /> {props.recent_apps_list_item_page}  </p> */}
                <div className="date-desc">{props.recent_apps_list_item_date} </div>
              </div>
              <div className="recent_apps_list_item">
                <Row>
                  <Col xs={22}>
                    <div className="card-title-header"> Block 2 </div>
                  </Col>
                  <Col xs={2} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
                {/* <p className='page-count'><FileOutlined /> {props.recent_apps_list_item_page}  </p> */}
                <div className="date-desc">{props.recent_apps_list_item_date} </div>
              </div>
              <div className="recent_apps_list_item">
                <Row>
                  <Col xs={22}>
                    <div className="card-title-header"> Block 3 </div>
                  </Col>
                  <Col xs={2} className="option-dots-center">
                    <Dropdown overlay={menu} placement="bottomRight">
                      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                        <i className="icon_more"></i>
                      </a>
                    </Dropdown>
                  </Col>
                </Row>
                {/* <p className='page-count'><FileOutlined /> {props.recent_apps_list_item_page}  </p> */}
                <div className="date-desc mt-1">{props.recent_apps_list_item_date} </div>
              </div>
            </div>
          </div>
          <div className="btn-div-sticky text-center">
            <Button
              // onClick={() => {
              //   navigate('/apps', { state: { appListActive: 3 } });
              // }}
              className="create-app-button-text2 create-app-button2"
              onClick={() => navigate('/blockgrid')}>
              View All Blocks
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default RecentBlocksBox;

RecentBlocksBox.propTypes = {
  card_title: PropTypes.string,
  card_description: PropTypes.string,
  card_img_src: PropTypes.string,
  recent_apps_list_item_date: PropTypes.string,
  recent_apps_list_item_heading: PropTypes.string
};
