import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import '../../config/config';
import './cards.css';
import { Row, Col } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';
import { Modal, Button, Form, Dropdown, Menu, Checkbox } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import axios from 'axios';
import moment from 'moment';
import 'antd/dist/antd.css';
import InputText from '../InputsFields/InputText';
import InputTextArea from '../InputsFields/InputTextArea';
import TextHeadingH4 from '../TextHeadings/TextHeadingH4';
import TextHeadingH3 from '../TextHeadings/TextHeadingH3';
import '../Popup/popup.css';

const pagesMenu = (
  <Menu className="list_menu">
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer">
        {' '}
        Edit{' '}
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer">
        {' '}
        Delete{' '}
      </a>
    </Menu.Item>
  </Menu>
);

// const ModuleMenu = (

// );

const AssetManagementAppCard = (props) => {
  const [modules, setModules] = useState([]);
  const [pages, setPages] = useState([]);
  const [moduleId, setModuleId] = useState();
  const [checked, setChecked] = useState(false);
  const [createPage, setCreatePage] = useState({
    name: '',
    description: '',
    nameValidation: '',
    descriptionValidation: ''
  });
  const [createModule, setModule] = useState({
    name: '',
    description: '',
    isActive: 0,
    nameValidation: '',
    descriptionValidation: '',
    checked: true
  });
  console.log('modules data', props.getModules);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleDelete, setIsModalVisibleDelete] = useState(false);
  const [isModalVisibleModule, setIsModalVisibleModule] = useState(false);
  const [deleteModuleId, setDeleteModuelId] = useState();
  const [module, setModuleData] = useState([]);

  const { state } = useLocation();
  console.log('state\value', state);
  const navigate = useNavigate();

  function onChange(e) {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      setChecked(true);
      setModule((prevState) => ({ ...prevState, isActive: 1 }));
    } else if (e.target.checked === false) {
      setChecked(false);
      setModule((prevState) => ({ ...prevState, isActive: 0 }));
    }
  }

  const deleteModule = (moduleId) => {
    setDeleteModuelId(moduleId);
    setIsModalVisibleDelete(true);
  };

  const deleteFunctionCall = () => {
    axios
      .delete(global.config.API_URL + `/deleteModule/${deleteModuleId}`)
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisibleDelete(false);
          props.getModules();
          props.getModulesCount();
          getModules();
        }
      })
      .catch((error) => console.log(error));
  };
  console.log('search', props.searchFilter);

  var getModules = async () => {
    axios
      .post(global.config.API_URL + '/getModules', { appId: state._id })
      .then((response) => {
        console.log('response idsss', response);
        setModules(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const getPages = () => {
    axios
      .post(global.config.API_URL + '/gePages', { moduleId: '61de7af378842127c7f7acc2' })
      .then((response) => {
        console.log('response is', response);
        setPages(response.data.data);
      })
      .catch((error) => console.log(error));
  };

  const showModal = (module_id) => {
    setModuleId(module_id);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCancelDelete = () => {
    setIsModalVisibleDelete(false);
  };

  const nameChangeModule = (name) => {
    if (name != '') {
      setModule((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setModule((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const nameChange = (name) => {
    if (name != '') {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setCreatePage((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    if (description != '') {
      setCreatePage((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
      setModule((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    }
  };

  const handleCancelModule = () => {
    setIsModalVisibleModule(false);
    setModuleData([]);
  };

  const editModule = (moduleId) => {
    console.log(moduleId);

    axios
      .post(global.config.API_URL + `/getDetailById`, {
        id: moduleId,
        tableName: 'modules'
      })
      .then((response) => {
        console.log(typeof response.data.data.isActive);
        console.log('response', response);
        setModuleData(response.data.data);
        setModule((prevState) => ({
          ...prevState,
          name: response.data.data.name,
          description: response.data.data.description,
          isActive: response.data.data.isActive
        }));

        if (response.data.data.isActive === 1) {
          setChecked(true);
          console.log('if');
        } else {
          setChecked(false);
          console.log('else');
        }
      })
      .catch((error) => console.log(error));

    setTimeout(() => {
      setIsModalVisibleModule(true);
    }, 1000);
  };

  const onUpdateModule = () => {
    axios
      .put(global.config.API_URL + `/updateModule/${module._id}`, {
        name: createModule.name,
        description: createModule.description,
        isActive: createModule.isActive
      })
      .then((response) => {
        console.log(response);
        if (response.data.success === true) {
          setIsModalVisibleModule(false);
          getModules();
        }
      })
      .catch((error) => console.log(error));
  };
  const createPageModule = () => {
    console.log(createPage);
    if (createPage.name != '') {
      axios
        .post(global.config.API_URL + '/createPage', {
          moduleId: moduleId,
          pageName: createPage.name,
          description: createPage.description
        })
        .then((response) => {
          console.log(response);

          if (response.data.message === 'Page created!') {
            setCreatePage((prevState) => ({ ...prevState, name: '', description: '' }));

            // setTimeout(() => {
            //   navigate('/page_template');
            // }, 2000);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      if (createPage.name == '') {
        setCreatePage((prevState) => ({ ...prevState, nameValidation: true }));
      }
    }
  };

  useEffect(() => {
    getModules();
    getPages();
    console.log('propsdd', props);
  }, [isModalVisible]);

  return (
    <>
      {/* create page popup use here  */}
      <div className="create_page">
        <Modal
          className="create_module_popup"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center " text_h4="Create a Page" />
          <Form layout="vertical">
            <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
              <InputText
                placeholder_text="Enter Page Name"
                value={createPage.name}
                nameChangeFunction={nameChange}
              />
              {createPage.nameValidation === true ? (
                <p className="text-danger"> Page name is required</p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description" rules={[{ required: true }]}>
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={createPage.description}
              />
              {createPage.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Description is required</span>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancel}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={createPageModule}>
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* created modules display */}

      {/* edit modal popup here */}

      <div className="create_module">
        <Modal
          destroyOnClose={true}
          visible={isModalVisibleModule}
          className="create_module_popup"
          onCancel={handleCancelModule}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Module" />

          <Form layout="vertical">
            <Form.Item
              label="Module Name"
              name="Module Name"
              rules={[
                {
                  required: true
                }
              ]}>
              <InputText
                placeholder_text="Enter Module Name"
                value={module.name}
                nameChangeFunction={nameChangeModule}
              />
              {createModule.nameValidation === true ? (
                <p className="text-danger" style={{ marginRight: '193px' }}>
                  {' '}
                  Module name is required
                </p>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item label="Description">
              <InputTextArea
                discriptionChangeFunction={discriptionChange}
                value={module.description}
              />
              {createModule.descriptionValidation === true ? (
                <span style={{ color: 'red' }}>Module description is required</span>
              ) : (
                ''
              )}
            </Form.Item>

            <Form.Item>
              <Checkbox size="small" checked={checked} onChange={onChange}>
                Yes, I agree to show my module to public
              </Checkbox>
            </Form.Item>
            {/* <Form.Item
                    label='Status'
                    name='Status'
                    valuePropName='checked'
                    wrapperCol={{
                      offset: 1,
                      span: 20
                    }}>
                    <Checkbox checked={true} onChange={onChange}>
                      Yes, I agree to show my module to public
                    </Checkbox>
                  </Form.Item> */}

            <Form.Item
              wrapperCol={{
                offset: 4,
                span: 16
              }}>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancelModule}>
                  {' '}
                  Cancel
                </Button>
                <Button
                  className="submit_btn"
                  type="primary"
                  Type="submit"
                  onClick={onUpdateModule}>
                  {' '}
                  Update{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* delete popup */}
      <div className="Delete_popup">
        <Modal
          visible={isModalVisibleDelete}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center red" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3="You are about to delete a “module list”" />
            </div>
            <div className="description">
              <p>
                If you cancel this page, you will lose unsaved data and also not saved in your
                automatic generated version.
              </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button
                type="primary"
                size="large"
                className="cancel"
                onClick={() => handleCancelDelete()}>
                Cancel
              </Button>
              <Button
                type="danger"
                size="large"
                className="delete"
                onClick={() => deleteFunctionCall()}>
                Delete
              </Button>
            </div>
          </div>
        </Modal>
      </div>
      {(() => {
        if (props.moduleName === 'Module List') {
          return modules.map((createdModule, i) => {
            return (
              <>
                <Row key={i}>
                  <Col xl={24}>
                    <div className="AssetManagementAppCard">
                      <div className="AssetManagementAppCardDiv">
                        <div className="card-body">
                          <Row>
                            <Col xs={22}>
                              <h3 className="title">{createdModule.name} </h3>
                            </Col>
                            <Col xs={2} className="option-dots">
                              <Dropdown
                                placement="bottomRight"
                                overlay={(() => {
                                  console.log('modules', createdModule);
                                  return (
                                    <Menu className="list_menu">
                                      <Menu.Item>
                                        <a
                                          target="_blank"
                                          rel="noopener noreferrer"
                                          onClick={() => {
                                            navigate('/app/pages', {
                                              state: { data: createdModule, appId: 0 }
                                            });
                                          }}>
                                          {' '}
                                          View Pages{' '}
                                        </a>
                                      </Menu.Item>
                                      <Menu.Item>
                                        <a
                                          target="_blank"
                                          onClick={() => editModule(createdModule._id)}>
                                          {' '}
                                          Edit{' '}
                                        </a>
                                      </Menu.Item>
                                      <Menu.Item>
                                        <a
                                          target="_blank"
                                          rel="noopener noreferrer"
                                          onClick={() => deleteModule(createdModule._id)}>
                                          {' '}
                                          Delete{' '}
                                        </a>
                                      </Menu.Item>
                                    </Menu>
                                  );
                                })()}>
                                <a
                                  className="ant-dropdown-link"
                                  onClick={(e) => e.preventDefault()}>
                                  <i className="icon_more"></i>
                                </a>
                              </Dropdown>
                            </Col>
                          </Row>

                          <p className="sub_title">
                            {moment(createdModule.createdAt).format('Do dddd, YYYY')}{' '}
                          </p>
                          <div style={{ padding: '6% 0' }}>
                            <div className="small_txt" maxLength="20">
                              {createdModule.description}
                            </div>
                            <div className="large-txt">Start now creating pages.</div>
                          </div>
                          <div className="btn-create">
                            <Button
                              type="primary"
                              size="medium"
                              className="Buton_small"
                              onClick={() => showModal(createdModule._id)}>
                              Create New Page
                            </Button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </>
            );
          });
        } else if (props.moduleName === 'Page List') {
          return pages.map((page, i) => {
            return (
              <>
                <Row key={i}>
                  <Col xl={24}>
                    <div className="AssetManagementAppCard">
                      <div className="AssetManagementAppCardDiv">
                        <div className="card-body">
                          <div style={{ display: 'flex' }}>
                            <h3 className="title">{page.name} </h3>
                            <div className="option-dots">
                              <Dropdown placement="bottomRight" overlay={pagesMenu}>
                                <a
                                  className="ant-dropdown-link"
                                  onClick={(e) => e.preventDefault()}>
                                  <i className="icon_more"></i>
                                </a>
                              </Dropdown>
                            </div>
                          </div>
                          <p className="sub_title">
                            {moment(page.createdAt).format('Do dddd, YYYY')}{' '}
                          </p>
                          <div style={{ padding: '6% 0' }}>
                            <div className="small_txt">{/* {page.description} */}</div>
                            <div className="large-txt">
                              {page.description}
                              {/* {
                                                                (() => {
                                                                    var str = page.description
                                                                    if(str.length > 102){
                                                                        return str = str.substring(0,102)
                                                                    }
                                                                })()
                                                            } */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </>
            );
          });
        }
      })()}
    </>
  );
};

export default AssetManagementAppCard;

AssetManagementAppCard.propTypes = {
  moduleName: PropTypes.string,
  getModules: PropTypes.func,
  getModulesCount: PropTypes.func,
  popup_description: PropTypes.string,
  searchFilter: PropTypes.string
};
