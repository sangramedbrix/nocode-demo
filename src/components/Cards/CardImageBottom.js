// @FIXME: not being used
import React from 'react';
import { Card } from 'antd';
import anyone from '../../assets/images/LoginImageBanner.png';
const { Meta } = Card;

const CardImageBottom = () => {
  return (
    <>
      <Card hoverable style={{ width: 240 }}>
        <Meta title="Europe Street beat" description="www.instagram.com" imgsrc={anyone} />
      </Card>
    </>
  );
};

export default CardImageBottom;
