import React from 'react';
import PropTypes from 'prop-types';
import './cards.css';
import { Divider, Dropdown, Menu, Row, Col } from 'antd';

import CountBox from './CountBox';
const menu = (
  <Menu className="list_menu">
    <Menu.Item>
      <a href=""> Delete </a>
    </Menu.Item>
    <Menu.Item>
      <a href=""> Edit </a>
    </Menu.Item>
  </Menu>
);

const AssetManagementAppCard = (props) => {
  return (
    <>
      <div className="AssetManagementAppCard">
        <div className="AssetManagementAppCardDiv">
          <div className="card-body">
            <Row>
              <Col xs={22}>
                <h3> ggg </h3>
              </Col>
              <Col xs={2} className="option-dots mar-ad">
                <Dropdown overlay={menu} placement="bottomRight">
                  <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                    <i className="icon_more"></i>
                  </a>
                </Dropdown>
              </Col>
            </Row>
            {/* <h3 className='card_title'>{props.card_title} </h3> */}
            <p className="sub_title">{props.card_date} </p>
            <div className="counter_box">
              <CountBox no_of_count="154" count_box_title="No.of Usages" />
              <CountBox no_of_count="25" count_box_title="No.of Pages" />
            </div>
            <div className="counter_box">
              <CountBox no_of_count="154" count_box_title="No.of Usages" />
              <CountBox no_of_count="25" count_box_title="No.of Pages" />
            </div>
            <Divider />
            <div className="card_footer">
              <div className="sub_title">
                <i className="icon_page"></i>
                <span style={{ marginLeft: 12 }}>{props.footer_text}</span>
              </div>
              <div className=" inprogress">{props.footer_status} </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AssetManagementAppCard;

AssetManagementAppCard.propTypes = {
  card_title: PropTypes.string,
  card_date: PropTypes.string,
  footer_text: PropTypes.string,
  footer_status: PropTypes.string
};
