import React from 'react';
import PropTypes from 'prop-types';
import './cards.css';
import { useNavigate } from 'react-router-dom';

const PageTypeBox = (props) => {
  const navigate = useNavigate();
  return (
    <>
      <div
        style={{ cursor: 'pointer' }}
        className="page_type_box"
        onClick={() => {
          navigate(props.page_link);
        }}>
        <div className="page_type_img_box_">
          <img src={props.page_img_src} alt="NoCode" />
        </div>
        <div className="page_type_img_box_">
          <p className="p-tag d-flex text-center">{props.page_title} </p>
        </div>
      </div>
    </>
  );
};

export default PageTypeBox;

PageTypeBox.propTypes = {
  page_img_src: PropTypes.string,
  page_title: PropTypes.string,
  page_link: PropTypes.string
};
