import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Card, Button, Radio } from 'antd';
import { useNavigate } from 'react-router-dom';

const CardImageTop = (props) => {
  console.log('props.appData', props.appData);
  const navigate = useNavigate();
  return (
    <>
      <Card
        cover={<img alt="NoCode" src={props.card_img_src} />}
        // actions='false'
        // actions={[props.action_footer]}
        // hoverable
        className="card_img_top">
        <div className="radio-div">
          <Radio></Radio>
        </div>
        <p className="card-title p-tag">{props.card_title} </p>
        <p className="card-description ">{props.card_description}</p>
        <p className="card-color p-tag">{props.card_color}</p>
        <div className="select_div">
          <div className="d-flex">
            {/* <Radio.Group> */}
            <div className="red">
              <Radio name="color" />
            </div>
            <div className="blue">
              <Radio name="color" />
            </div>
            <div className="yellow">
              <Radio name="color" />
            </div>
            <div className="green">
              <Radio name="color" />
            </div>
            {/* </Radio.Group> */}
            {/* <p className='card-color-round p-tag'></p>
            <p className='card-color-round p-tag'></p> */}
          </div>
          <div className="btn_div">
            <Button size="small">Preview</Button>
            <Button
              type="primary"
              size="small"
              onClick={() => {
                navigate(`/app/${props.appData._id}/modules`, { state: props.appData });
              }}>
              Select
            </Button>
          </div>
        </div>
      </Card>
    </>
  );
};
export default CardImageTop;

CardImageTop.propTypes = {
  card_img_src: PropTypes.string,
  card_title: PropTypes.string,
  card_description: PropTypes.string,
  card_color: PropTypes.string,
  appData: PropTypes.array
};
