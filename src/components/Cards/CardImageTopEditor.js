import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Card, Button, Radio } from 'antd';

const CardImageTopEditor = (props) => {
  return (
    <>
      <Card
        cover={<img alt="NoCode" src={props.card_img_src} />}
        // actions='false'
        // actions={[props.action_footer]}
        // hoverable
        className="card_img_top_editor">
        <div className="radio-div">
          <Radio></Radio>
        </div>
        <div className="text-btn-div">
          <div>
            <p className="card-title p-tag">{props.card_title} </p>
            <p className="card-description ">{props.card_description}</p>
            <p className="card-color p-tag">{props.card_color}</p>
          </div>
          <div>
            <p className="card-title p-tag active_card">Active </p>
          </div>
        </div>

        <div className="select_div">
          <div className="d-flex">
            {/* <Radio.Group> */}
            <div className="red">
              <Radio name="color" />
            </div>
            <div className="blue">
              <Radio name="color" />
            </div>
            <div className="yellow">
              <Radio name="color" />
            </div>
            <div className="green">
              <Radio name="color" />
            </div>
            {/* </Radio.Group> */}
            {/* <p className="card-color-round p-tag"></p>
            <p className="card-color-round p-tag"></p> */}
          </div>
          <div className="btn_div">
            <Button size="small">Preview</Button>
            <Button type="primary" size="small">
              Select
            </Button>
          </div>
        </div>
      </Card>
    </>
  );
};
export default CardImageTopEditor;

CardImageTopEditor.propTypes = {
  card_img_src: PropTypes.string,
  card_title: PropTypes.string,
  card_description: PropTypes.string,
  card_color: PropTypes.string,
  appData: PropTypes.array
};
