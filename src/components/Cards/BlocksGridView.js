import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Row, Col, Avatar, Divider } from 'antd';
import './cards.css';

const menu = (
  <Menu className="list_menu">
    <Menu.Item>
      <a href=""> Delete </a>
    </Menu.Item>
    <Menu.Item>
      <a href=""> Edit </a>
    </Menu.Item>
  </Menu>
);

const BlocksGridView = (props) => {
  return (
    <>
      <div className="BlocksGridView">
        <div hovreable className="card">
          <div className="card_body">
            <div className="card_img">
              <img
                src={props.block_grid_icon}
                className="set"
                height="70px"
                width="70px"
                alt="NoCode"
              />
            </div>
            <div className="card_text">
              <Row>
                <Col xs={23}>
                  <h3> {props.card_title} </h3>
                </Col>
                <Col xs={1} className="option-dots mar-ad">
                  <Dropdown overlay={menu} placement="bottomRight">
                    <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                      <i className="icon_more"></i>
                    </a>
                  </Dropdown>
                </Col>
              </Row>
              <div className="sub_title">
                <p> {props.app_grid_date} </p>
              </div>
              <div className="status_box">
                <p className="p-tag"> {<Avatar src="https://joeschmoe.io/api/v1/random" />} </p>
                <p className="approved p-tag"> {props.status} </p>
              </div>
            </div>
            <Divider />
            <div className="card_footer">
              <p className="p-tag comments">
                {
                  <i className="icon_comment" style={{ marginRight: '7px' }}>
                    {' '}
                  </i>
                }{' '}
                {props.footer_comments}
              </p>
              <p className="p-tag lock">
                {
                  <i className="icon_lock" style={{ marginRight: '7px' }}>
                    {' '}
                  </i>
                }{' '}
                {props.footer_public_private}
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BlocksGridView;

BlocksGridView.propTypes = {
  block_grid_icon: PropTypes.string,
  card_title: PropTypes.string,
  status: PropTypes.string,
  app_grid_date: PropTypes.string,
  footer_comments: PropTypes.string,
  footer_public_private: PropTypes.string
};
