// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';
const { Meta } = Card;

const CardImageMiddle = (props) => {
  return (
    <>
      <Card
        style={{ width: 300 }}
        title={props.card_title}
        cover={<img alt="NoCode" src={props.card_img_src} />}
        actions={<h5>Footer</h5>}
        hoverable>
        <Meta description={props.card_description} />
      </Card>
    </>
  );
};

export default CardImageMiddle;

CardImageMiddle.propTypes = {
  card_title: PropTypes.string,
  card_img_src: PropTypes.string,
  card_description: PropTypes.string
};
