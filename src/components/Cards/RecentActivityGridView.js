import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import './cards.css';
import { Divider } from 'antd';

const RecentActivityGridView = (props) => {
  return (
    <>
      <div className="RecentActivityGridView">
        <div hovreable className="card">
          <div className="card_body">
            <div className="card_img">
              <img src={props.recent_activity_icon} alt="NoCode" />
            </div>
            <div className="card_text">
              <h3>
                {' '}
                <span className="green">New! </span> {props.card_title}{' '}
                {/* <Tag color='#2db7f5'>#2db7f5</Tag> */}
              </h3>
              <div className="sub_title">
                <p>{props.card_description} </p>
                <p> {props.no_of_page}</p>
              </div>
              <p className="comments"> {props.card_coments}</p>
              <h4 className="comment_desc">{props.card_text} </h4>
            </div>
            <Divider />
            <div className="card_footer">
              <div className="footer_date"> {props.card_footer_date} </div>
              <div className="approved"> {props.card_footer_status} </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default RecentActivityGridView;

RecentActivityGridView.propTypes = {
  recent_activity_icon: PropTypes.string,
  card_title: PropTypes.string,
  card_description: PropTypes.string,
  no_of_page: PropTypes.string,
  card_text: PropTypes.string,
  card_coments: PropTypes.string,
  card_footer_date: PropTypes.string,
  card_footer_status: PropTypes.string
};
