import React from 'react';
import { Select } from 'antd';
import { PlusOutlined, MinusOutlined, RedoOutlined, UndoOutlined } from '@ant-design/icons';
import { useState } from 'react';
import './style.css';

const { Option } = Select;

const FormBlank = () => {
  const [style, setStyle] = useState('wrapper-container');
  const [activeScreen, setActiveScreen] = useState('screen');
  const [tabViewScreen, setTabViewScreen] = useState('');
  const [mobileViewScreen, setMobileViewScreen] = useState('');

  const desktopView = () => {
    // console.log('you just clicked');
    setStyle('desktop-container-wrapper');
    setActiveScreen('activescreen');
    setTabViewScreen('');
    setMobileViewScreen('');
    // setActiveScreen('')
  };
  const tabView = () => {
    setStyle('tab-container-wrapper');
    setActiveScreen('');
    setTabViewScreen('activescreen');
    setMobileViewScreen('');
  };
  const mobileView = () => {
    setStyle('mobile-container-wrapper');
    setActiveScreen('');
    setTabViewScreen('');
    setMobileViewScreen('activescreen');
  };
  return (
    <>
      <div className="empty_box">
        <div className="search_bar">
          <div className="do_undo_div">
            <div className="icon_undo">
              <UndoOutlined />
            </div>
            <div className="icon_redo">
              <RedoOutlined />
            </div>
          </div>

          <div className="select_div">
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Search to Select"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }>
              <Option value="1">Personal Information</Option>
              <Option value="2">Personal Information</Option>
              <Option value="3">Personal Information</Option>
            </Select>
          </div>

          <div className="zoom-icons">
            <div className="icon_zoom_in">
              {' '}
              <PlusOutlined />{' '}
            </div>
            <span>100%</span>
            <div className="icon_zoom_out">
              {' '}
              <MinusOutlined />{' '}
            </div>
          </div>
          <div className="screens">
            <div className={`icon_desktop ${activeScreen}`} onClick={desktopView}></div>
            <div className={`icon_tab ${tabViewScreen}`} onClick={tabView}></div>
            <div className={`icon_mob ${mobileViewScreen}`} onClick={mobileView}></div>
          </div>
        </div>
        {/* <div className="empty_box_div"> */}
        <div className="temp">
          <div className={style}>{/* <div className="desktop-container-wrapper"></div> */}</div>
        </div>

        {/* </div> */}
      </div>
    </>
  );
};

export default FormBlank;
