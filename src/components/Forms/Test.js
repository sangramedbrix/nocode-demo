import React from 'react';
import { useState } from 'react';
const Test = () => {
  const [style, setStyle] = useState('cont');

  const changeStyle = () => {
    console.log('you just clicked');

    setStyle('cont2');
  };
  return (
    <div>
      <div className="App">CHANGE CSS STYLING WITH ONCLICK EVENT</div>
      <div className={style}>
        <button className="button" onClick={changeStyle}>
          Click me!
        </button>
      </div>
    </div>
  );
};

export default Test;
