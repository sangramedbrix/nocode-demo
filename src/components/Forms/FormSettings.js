import React from 'react';
import { Divider, Input, Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import './style.css';
import '../../assets/styles/index.css';

const FormSettings = () => {
  return (
    <>
      <div className="form-setting pad-2">
        <div className="section1">
          <div className="section1-title">Unique Code</div>
          <Input placeholder="Type Here" style={{ border: 'none', fontSize: 14 }} />
        </div>

        <Divider className="mtb-1" />
        <div>
          <h3 className="section6-title">Permissions</h3>
          <div className="section6">
            <div className="section6-active-title">Active Status</div>
            <div>
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                defaultChecked
              />
            </div>
          </div>
          <Divider className="mtb-2" />
          <div className="section6">
            <div className="section6-active-title">Validation</div>
            <div>
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                defaultChecked
              />
            </div>
          </div>
          <Divider className="mtb-2" />
          <div className="section6">
            <div>
              <span className="section6-active-title">Edit Access</span>
              <p className="section6-edit">
                If Yes, Admin and Data Analyst can edit and change the design
              </p>
            </div>
            <div>
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                defaultChecked
              />
            </div>
          </div>

          <Divider className="mtb-2" />
        </div>
      </div>
    </>
  );
};

export default FormSettings;
