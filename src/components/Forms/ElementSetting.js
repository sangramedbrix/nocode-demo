import React from 'react';
import '../../components/components.css';
import { Row, Col, Divider, Switch, Button, Select, Input } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import './style.css';

const { Option } = Select;

function handleChange(value) {
  console.log(`Selected: ${value}`);
}

const ElementSetting = () => {
  return (
    <>
      <div className="element_setting">
        <div className="heading_btn">
          <Row>
            <Col xl={14}>
              <span className="title">Element Settings</span>
            </Col>
            <Col xl={10}>
              <span className="btn_div">
                <Button className="cancal" size="small">
                  Cancel
                </Button>
                <Button className="apply" size="small" type="primary">
                  Apply
                </Button>
              </span>
            </Col>
          </Row>
        </div>

        <div className="element_type">
          <Row>
            <Col xl={24} lg={5} md={5} sm={5} xs={8}>
              <div className="sub_title">Type</div>
              <Select
                className="select_title"
                size="small"
                defaultValue="Input Text"
                onChange={handleChange}
                style={{ width: '100%' }}>
                <Option>Input Text Area</Option>
              </Select>
            </Col>
          </Row>
          <Divider className="mtb-1" />
          <Row>
            <Col xl={24} lg={5} md={5} sm={5} xs={8}>
              <div className="sub_title">Text Type</div>
              <Select
                className="select_title"
                size="small"
                defaultValue="Default"
                onChange={handleChange}
                style={{ width: '100%' }}>
                <Option>Default</Option>
              </Select>
            </Col>
          </Row>
          <Divider className="mtb-1" />
          <Row>
            <Col xl={24} lg={5} md={5} sm={5} xs={8}>
              <div className="sub_title">Label</div>
              <Input className="input_title" value="Email Address" />
            </Col>
          </Row>
          <Divider className="mtb-1" />
          <Row>
            <Col xl={24} lg={5} md={5} sm={5} xs={8}>
              <div className="sub_title">Placeholder</div>
              <Input className="input_title" value="Enter you email address" />
            </Col>
          </Row>
          <Divider className="mtb-1" />
          <Row className=" ">
            <Col xl={20} lg={5} md={5} sm={5} xs={8}>
              <div className="mandatory">Mandatory</div>
            </Col>
            <Col xl={3} lg={5} md={5} sm={5} xs={8}>
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                defaultChecked
              />
            </Col>
          </Row>
          <Divider className="mtb-1" />
        </div>

        <div>
          <div className="permissions">
            <div className="title">Permissions</div>
            <Row gutter={[]}>
              <Col xl={20} lg={5} md={5} sm={5} xs={8}>
                <div className="active_status_title">Active Status</div>
              </Col>
              <Col xl={3} lg={5} md={5} sm={5} xs={8}>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </Col>
            </Row>

            <Divider className="mtb-2" />
            <Row className="validation">
              <Col xl={20} lg={5} md={5} sm={5} xs={8}>
                <div className="title">Validation</div>
              </Col>
              <Col xl={3} lg={5} md={5} sm={5} xs={8}>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </Col>
            </Row>
            <Divider className="mtb-2" />
            <Row className="edit_access">
              <Col xl={20} lg={5} md={5} sm={5} xs={8}>
                <div className="title">Edit Access</div>
                <div className="section6-edit">
                  If Yes, Admin and Data Analyst can edit and change the design
                </div>
              </Col>
              <Col xl={3} lg={5} md={5} sm={5} xs={8}>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </Col>
            </Row>
            <Divider className="mtb-2" />
            <Row className="validation">
              <Col xl={20} lg={5} md={5} sm={5} xs={8}>
                <div className="title">Data Connector</div>
              </Col>
              <Col xl={3} lg={5} md={5} sm={5} xs={8}>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked
                />
              </Col>
            </Row>
            <Divider className="mtb-2" />
          </div>
        </div>
      </div>
    </>
  );
};

export default ElementSetting;
