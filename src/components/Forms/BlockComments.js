import React from 'react';
import { Input, Button, Avatar } from 'antd';
import sender from '../../assets/images/icons/sender.png';
import receiver from '../../assets/images/icons/receiver.png';
// import { Avatar, Divider, Tooltip } from 'antd';
// import { UserOutlined, AntDesignOutlined } from '@ant-design/icons';

import './chat.css';
import './style.css';

const BlockComments = () => {
  return (
    <>
      <div className="block_comment">
        <div className="header_comment_btn">
          <div>
            <i className="icon_comment_link" /> <span className="margin-right">Block Comments</span>
          </div>
          <div>
            <Button icon={<i className="icon_close_btn margin-right" />}></Button>
          </div>
        </div>

        <p className="title">“First Name” Selected</p>

        <div className="input_selected">
          <div className="label"> First Name </div>
          <Input className="selected-input" placeholder="Full Name" />
        </div>

        <div className="comments-div">
          <p className="comments-titile">
            Comments <span className="comments-sub"> (3 users discussing)</span>
          </p>
          <Avatar.Group>
            <Avatar src="https://joeschmoe.io/api/v1/random" />
            <Avatar src="https://joeschmoe.io/api/v1/random" />
            <Avatar src="https://joeschmoe.io/api/v1/random" />
          </Avatar.Group>
        </div>

        <div className="chat_div">
          <div className="receiver_div">
            <div className="receiver">
              <span className="recevier_img">
                <img src={receiver} />
              </span>
              <span className="receiver_span">Form Block Amet minim mollit non deserunt </span>
            </div>
            <span className="receiver_date_time">08:00 AM, 45 mins ago </span>
          </div>
          <div className="sender_div">
            <div className="sender">
              <span className="sender_span">Amet minim mollit deserunt </span>
              <span className="recevier_img">
                <img src={sender} />
              </span>
            </div>
            <span className="sender_date_time">08:00 AM, 45 mins ago </span>
          </div>
          <div className="receiver_div">
            <div className="receiver">
              <span className="recevier_img">
                <img src={receiver} />
              </span>
              <span className="receiver_span">
                Form Block Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
              </span>
            </div>
            <span className="receiver_date_time">08:00 AM, 45 mins ago </span>
          </div>
          <div className="sender_div">
            <div className="sender">
              <span className="sender_span">Amet minim mollit deserunt </span>
              <span className="recevier_img">
                <img src={sender} />
              </span>
            </div>
            <span className="sender_date_time">08:00 AM, 45 mins ago </span>
          </div>
        </div>
        <div className="send_btn_div">
          <Input placeholder="Write hear..." />
          <i className="icon_send" />
        </div>
      </div>
    </>
  );
};

export default BlockComments;
