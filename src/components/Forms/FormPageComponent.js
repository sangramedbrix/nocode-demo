import React from 'react';
import { Col, Row, Tabs } from 'antd';
import PageSettings from './PageSettings';
import FormSettings from './FormSettings';
import { CaretDownOutlined } from '@ant-design/icons';
import './style.css';

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const AppComponent = () => {
  return (
    <>
      <div className="form-page-container">
        <div>
          <Row className="form_page">
            <Col xs={4}>
              <i className="icn icon_page_form"></i>
            </Col>
            <Col xs={18} className="form-page-header">
              Form Page
            </Col>
            <Col>
              <CaretDownOutlined />
            </Col>
          </Row>
        </div>
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="Page Settings" key="1">
            <PageSettings />
          </TabPane>
          <TabPane tab="Form Settings" key="2">
            <FormSettings />
          </TabPane>
        </Tabs>
      </div>
    </>
  );
};

export default AppComponent;
