import React from 'react';
import { Divider, Input, Switch, Button } from 'antd';
import Template from '../../assets/images/template.png';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import './style.css';

const PageSettings = () => {
  return (
    <>
      <div className="page-setting pad-2">
        <div className="page-header">General</div>
        <div className="section1">
          <div className="section1-title">Page Title</div>
          <Input placeholder="Type Here" style={{ border: 'none', fontSize: 14 }} />
        </div>
        <Divider className="mtb-1" />
        <div className="section2">
          <div className="section2-title">Page Description</div>
          <Input
            placeholder="Type Here"
            style={{ border: 'none', fontSize: 14, color: '#333333' }}
          />
        </div>
        <Divider className="mtb-1" />
        <div className="section6">
          <div>
            <span className="section3-title">Active App Theme</span>
            <p className="section3-subtitle"> Hexaqua Marina</p>
          </div>
          <div>
            <Button className="btn-change mtb-2">Change</Button>
          </div>
        </div>
        <div className="progress" style={{ width: '98%' }}></div>
        <Divider className="mtb-1" />

        <div className="section1-title">Page Template</div>
        <div className="section6 mtb-1">
          <div>
            <img src={Template} alt="template" width="45px" className="temp-img" />
          </div>
          <div className="section4-subtitle">
            <span> Basic Information Form </span>
          </div>
          <div>
            <Button className="btn-change">Change</Button>
          </div>
        </div>
        <Divider />

        <div className="section5">
          <div className="section5-title">Change Page Theme</div>
          <div>
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
              defaultChecked
            />
          </div>
        </div>
        <div className="section5 mtb-1">
          <div>
            <img src={Template} alt="template" width="45px" className="temp-img" />
          </div>
          <div className="pc-user-info">
            <div className="section5-subtitle">
              <p> Hexaqua Marina </p>
            </div>
          </div>
          <div>
            <Button className="btn-change">Change</Button>
          </div>
        </div>
        <Divider />
      </div>
    </>
  );
};

export default PageSettings;
