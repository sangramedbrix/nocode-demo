import React from 'react';
import empty from '../../assets/images/empty.png';
import { Select } from 'antd';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import './style.css';

const { Option } = Select;

const FormEmpty = () => {
  return (
    <>
      <div className="empty_box">
        <div className="search_bar">
          <div className="do_undo_div">
            <i className="icon_undo" />
            <i className="icon_redo" />
          </div>

          <div className="select_div">
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Search to Select"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }>
              <Option value="1">Personal Information</Option>
              <Option value="2">Personal Information</Option>
              <Option value="3">Personal Information</Option>
            </Select>
          </div>

          <div className="zoom-icons">
            <div className="icon_zoom_in">
              {' '}
              <PlusOutlined />{' '}
            </div>
            <span>100%</span>
            <div className="icon_zoom_out">
              {' '}
              <MinusOutlined />{' '}
            </div>
          </div>

          {/* <div className="screen">
            <i className="icon_desktop" />
            <i className="icon_tab" />
            <i className="icon_mob" />
          </div> */}
        </div>
        <div className="empty_box_div">
          <div className="temp">
            <div className="empty">
              <img src={empty} alt="empty" />{' '}
            </div>
            <div className="text_div">I’m still empty.</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormEmpty;
