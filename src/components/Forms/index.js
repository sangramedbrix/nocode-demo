import React from 'react';
import { Row, Col } from 'antd';
import AppComponent from './component';
import FormEmpty from './FormEmpty';

const FormPageComponent = () => {
  return (
    <>
      <AppComponent />
      <Row gutter={[20, 20]}>
        <Col xl={4}></Col>
        <Col xl={13}>
          <FormEmpty />
        </Col>
        <Col xl={7}></Col>
      </Row>
    </>
  );
};

export default FormPageComponent;
