import React from 'react';
import { Input, Button } from 'antd';

import sender from '../../assets/images/icons/sender.png';
import receiver from '../../assets/images/icons/receiver.png';

import './chat.css';
import './style.css';

const ChatDispaly = () => {
  return (
    <>
      <div className="editor_coments ">
        <div className="header_comment_btn">
          <span>
            <i className="icon_comment_link" /> <span className="margin-right"> All Comments</span>
          </span>
          <span>
            <Button className="add_comment" size="small">
              Add Comment
            </Button>
          </span>
        </div>
        <div className="chat_div mt-3">
          <div className="receiver_div">
            <div className="receiver">
              <span className="recevier_img">
                <img src={receiver} />
              </span>
              <span className="receiver_span">Form Block Amet minim mollit non deserunt </span>
            </div>
            <span className="receiver_date_time">08:00 AM, 45 mins ago </span>
          </div>
          <div className="sender_div">
            <div className="sender">
              <span className="sender_span">Amet minim mollit deserunt </span>
              <span className="recevier_img">
                <img src={sender} />
              </span>
            </div>
            <span className="sender_date_time">08:00 AM, 45 mins ago </span>
          </div>
          <div className="receiver_div">
            <div className="receiver">
              <span className="recevier_img">
                <img src={receiver} />
              </span>
              <span className="receiver_span">
                Form Block Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
              </span>
            </div>
            <span className="receiver_date_time">08:00 AM, 45 mins ago </span>
          </div>
          <div className="sender_div">
            <div className="sender">
              <span className="sender_span">Amet minim mollit deserunt </span>
              <span className="recevier_img">
                <img src={sender} />
              </span>
            </div>
            <span className="sender_date_time">08:00 AM, 45 mins ago </span>
          </div>
        </div>
        <div className="send_btn_div">
          <Input placeholder="Write hear..." />
          <i className="icon_send" />
        </div>
      </div>
    </>
  );
};

export default ChatDispaly;
