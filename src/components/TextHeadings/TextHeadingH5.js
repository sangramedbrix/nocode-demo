// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH5 = () => {
  return (
    <>
      <Title type={PropTypes.textColor} level={5}>
        {' '}
        {PropTypes.text_h5}
      </Title>
    </>
  );
};
export default TextHeadingH5;
