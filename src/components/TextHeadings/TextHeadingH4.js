import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH4 = (props) => {
  return (
    <>
      <Title type={props.textColor} level={4}>
        {' '}
        {props.text_h4} {props.text1} {props.desc1} {props.desc2}
      </Title>
    </>
  );
};
export default TextHeadingH4;

TextHeadingH4.propTypes = {
  textColor: PropTypes.string,
  text1: PropTypes.string,
  desc2: PropTypes.string,
  desc1: PropTypes.string,
  text_h4: PropTypes.string
};
