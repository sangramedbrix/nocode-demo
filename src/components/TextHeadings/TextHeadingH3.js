import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH3 = (props) => {
  return (
    <>
      <Title type={props.textColor} level={3}>
        {' '}
        {props.text_h3}
      </Title>
    </>
  );
};
export default TextHeadingH3;

TextHeadingH3.propTypes = {
  textColor: PropTypes.string,
  text_h3: PropTypes.string
};
