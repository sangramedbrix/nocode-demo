// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH1 = () => {
  return (
    <>
      <Title type={PropTypes.textColor} level={1}>
        {' '}
        {PropTypes.text_h1}
      </Title>
    </>
  );
};
export default TextHeadingH1;
