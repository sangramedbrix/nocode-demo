// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH2 = (props) => {
  return (
    <>
      <Title type={props.textColor} level={2}>
        {' '}
        {props.text_h2}
      </Title>
    </>
  );
};
export default TextHeadingH2;

TextHeadingH2.propTypes = {
  textColor: PropTypes.string,
  text_h2: PropTypes.string
};
