import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const UserCount = (props) => {
  console.log(props);
  return (
    <>
      <div className="user_count_div">
        <div className={`user_icon ${props.bg_color}`}>
          <i className="icon_user"></i>
        </div>
        <div className="user_cont_text">
          <span className="count">{props.count}</span>
          <span className="text">{props.text}</span>
        </div>
      </div>
    </>
  );
};

export default UserCount;

UserCount.propTypes = {
  bg_color: PropTypes.string,
  count: PropTypes.string,
  text: PropTypes.string
};
