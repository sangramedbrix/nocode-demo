import React from 'react';
import default_img from '../../assets/images/defult_user_img.png';
import { Row, Col, Divider, Menu, Dropdown } from 'antd';
import './style.css';

const menu = (
  <Menu className="list_menu">
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer">
        Edit
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer">
        Delete
      </a>
    </Menu.Item>
  </Menu>
);

const UserCard = () => {
  return (
    <>
      <div className="user_card">
        <div className="card">
          <div className="d-flex space-between">
            <div className="user_img_div">
              <img src={default_img} />
            </div>
            <div></div>
            <Dropdown overlay={menu} placement="bottomRight">
              <i className="icon_more"></i>
            </Dropdown>
          </div>
          <div className="user_card_text">
            <div checked="user_text">
              <h3 className="user_title">Ronald Richards</h3>
              <h5 className="user_sub_title"> Data Analytics</h5>
            </div>
            <div className="">
              {' '}
              <span className="status_active">Active</span>{' '}
            </div>
          </div>
          <Divider />

          <div className="card_footer">
            <Row>
              <Col xl={12}>
                <span className="footer_box">
                  <i className="icon_apps"></i>
                  <span className="footer_text"> 6 Apps </span>
                </span>
              </Col>
              <Col xl={12}>
                <span className="footer_box">
                  <i className="icon_modules"></i> <span className="footer_text"> 3 Modules </span>
                </span>
              </Col>
            </Row>
            <Row>
              <Col xl={12}>
                {' '}
                <span className="footer_box">
                  <i className="icon_page"></i> <span className="footer_text"> 51 Pages </span>
                </span>
              </Col>
              <Col xl={12}>
                <span className="footer_box">
                  <i className="icon_comment"></i>{' '}
                  <span className="footer_text"> 15 Comments </span>
                </span>
              </Col>
            </Row>
          </div>
        </div>
        <div className="note">
          <span>
            {' '}
            <span className="bolder">Recent Activities: </span> New page created in Student
            Information APP
          </span>
        </div>
      </div>
    </>
  );
};

export default UserCard;
