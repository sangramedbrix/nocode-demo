import React from 'react';
import './style.css';
const { useState } = React;
import { Layout, Menu, Drawer } from 'antd';
import Layouts from '../Drawer/Layouts';
const { SubMenu } = Menu;
const { Sider } = Layout;
// import PropTypes from 'prop-types';

const SideBarComponent = () => {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };
  // const navigate = useNavigate();
  return (
    <>
      <div className="sidebar-component-container">
        <Layout>
          {/* <Header className="site-layout-sub-header-background" style={{ padding: 0 }} /> */}
          <Layout className="site-layout-background">
            <div className="site-drawer-render-in-current-wrapper">
              <Drawer
                title="Layouts"
                placement="left"
                onClose={onClose}
                visible={visible}
                closable={true}
                // onClose={this.onClose}
                // visible={this.state.visible}
                getContainer={false}
                style={{ position: 'absolute' }}>
                <Layouts />
              </Drawer>
            </div>
            <Sider className="site-layout-background">
              <Menu
                mode="inline"
                defaultSelectedKeys={['2']}
                defaultOpenKeys={['sub2']}
                style={{ height: '100%', background: '#001F63' }}>
                <SubMenu key="sub1" title="Templates">
                  <Menu.Item key="1" className="submenus" onClick={showDrawer}>
                    Layout Elements
                  </Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" title="Blocks">
                  <Menu.Item key="2" className="submenus" onClick={showDrawer}>
                    Headers
                  </Menu.Item>
                  <Menu.Item key="3" className="submenus">
                    Footers
                  </Menu.Item>
                  <Menu.Item key="4" className="submenus">
                    Cards
                  </Menu.Item>
                  <Menu.Item key="5" className="submenus">
                    Tables
                  </Menu.Item>
                  <Menu.Item key="6" className="submenus">
                    Forms
                  </Menu.Item>
                  <Menu.Item key="7" className="submenus">
                    Buttons
                  </Menu.Item>
                  <Menu.Item key="8" className="submenus">
                    Typography
                  </Menu.Item>
                  <Menu.Item key="9" className="submenus">
                    Media
                  </Menu.Item>
                  <Menu.Item key="10" className="submenus">
                    Map
                  </Menu.Item>
                  <Menu.Item key="11" className="submenus">
                    Comments
                  </Menu.Item>
                  <Menu.Item key="12" className="submenus">
                    Steps
                  </Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" title="My Blocks">
                  {/* <Menu.Item key="9">option9</Menu.Item>
                  <Menu.Item key="10">option10</Menu.Item>
                  <Menu.Item key="11">option11</Menu.Item>
                  <Menu.Item key="12">option12</Menu.Item> */}
                </SubMenu>
                <SubMenu key="sub4" title="Favorites"></SubMenu>
              </Menu>
            </Sider>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default SideBarComponent;

// SideBarComponent.propTypes = {
//   activeKey: PropTypes.string
// };
