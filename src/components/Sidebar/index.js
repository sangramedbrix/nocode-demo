import React from 'react';
import AppComponent from './component';
import PropTypes from 'prop-types';

const SideBarComponent = (props) => {
  return (
    <>
      <AppComponent activeKey={props.activeKey} />
    </>
  );
};

export default SideBarComponent;

SideBarComponent.propTypes = {
  activeKey: PropTypes.string
};
