import React from 'react';
import AppComponent from './component';
import PropTypes from 'prop-types';

const SideMenuComponent = (props) => {
  return (
    <>
      <AppComponent activeKey={props.activeKey} />
    </>
  );
};

export default SideMenuComponent;

SideMenuComponent.propTypes = {
  activeKey: PropTypes.string
};
