import React, { useState } from 'react';
import './style.css';
import { Menu, Tooltip, Button, Modal } from 'antd';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import './../Popup/popup.css';
import { InfoCircleOutlined } from '@ant-design/icons';
import TextHeadingH3 from '../TextHeadings/TextHeadingH3';

const SideMenuComponent = (props) => {
  const navigate = useNavigate();
  const [isModalVisible, setIsModalVisible] = useState(false);

  // const showModal = () => {
  //   setIsModalVisible(true);
  // };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const logout = () => {
    localStorage.removeItem('isAuthenticated');
    navigate('/login');
  };

  return (
    <>
      {/* logout popup */}

      <div className="Delete_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <InfoCircleOutlined className="d-flex text-center yellow" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3="Are you sure you want to logout?" />
            </div>
            <div className="description">
              <p>{props.popup_description} </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Button type="primary" size="large" className="Buton_small" onClick={handleCancel}>
                Cancel
              </Button>

              <Button type="danger" size="large" className="Buton_small" onClick={logout}>
                Logout
              </Button>
            </div>
          </div>
        </Modal>
      </div>

      {/* end logout popup */}

      {/* <SiderDemo /> */}
      <div className="sidemenu">
        {/* <Button type='primary' onClick={this.toggleCollapsed} style={{ marginBottom: 16 }}>
          {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
        </Button> */}
        <Menu
          defaultSelectedKeys={[props.activeKey]}
          defaultOpenKeys={['sub1']}
          mode="inline"
          //   theme='dark'
          //   inlineCollapsed={this.state.collapsed}
        >
          <Menu.Item
            key="1"
            onClick={() => {
              navigate('/home');
            }}>
            <Tooltip placement="leftTop" title="Home">
              <i className="icn icon_home"></i>
            </Tooltip>
          </Menu.Item>
          <Menu.Item key="2">
            <Tooltip placement="leftTop" title="Dashboard">
              <i
                className="icn icon_dashboard"
                onClick={() => {
                  navigate('/dashboard');
                }}></i>
            </Tooltip>
          </Menu.Item>
          <Menu.Item
            key="3"
            onClick={() => {
              navigate('/apps', { state: { appListActive: 2 } });
            }}>
            <Tooltip placement="leftTop" title="App List">
              <i className="icn icon_create"></i>
            </Tooltip>
          </Menu.Item>
          <Menu.Item key="4">
            <Tooltip placement="leftTop" title="Block Grid">
              <i className="icn icon_block_grid"></i>
            </Tooltip>
          </Menu.Item>
          {/* <Menu.Item
            key="5"
            onClick={() => {
              navigate('/dataanalytics', { state: { appListActive: 2 } });
            }}>
            <Tooltip placement="leftTop" title="Data Analytics">
              <i className="icn icon_create"></i>
            </Tooltip>
          </Menu.Item> */}
          <Menu.Item
            key="6"
            onClick={() => {
              localStorage.removeItem('isAuthenticated');
              navigate('/login');
            }}>
            <Tooltip placement="leftTop" title="Log Out">
              <i className="icn icon_logout"></i>
            </Tooltip>
          </Menu.Item>

          {/* <SubMenu key='sub1' icon={<MailOutlined />} title='Navigation One'>
            <Menu.Item key='5'>Option 5</Menu.Item>
            <Menu.Item key='6'>Option 6</Menu.Item>
            <Menu.Item key='7'>Option 7</Menu.Item>
            <Menu.Item key='8'>Option 8</Menu.Item>
          </SubMenu>
          <SubMenu key='sub2' icon={<AppstoreOutlined />} title='Navigation Two'>
            <Menu.Item key='9'>Option 9</Menu.Item>
            <Menu.Item key='10'>Option 10</Menu.Item>
            <SubMenu key='sub3' title='Submenu'>
              <Menu.Item key='11'>Option 11</Menu.Item>
              <Menu.Item key='12'>Option 12</Menu.Item>
            </SubMenu>
          </SubMenu> */}
        </Menu>
      </div>
    </>
  );
};

export default SideMenuComponent;

SideMenuComponent.propTypes = {
  activeKey: PropTypes.string,
  popup_description: PropTypes.string
};
