// @FIXME: not being used
import React from 'react';
import './dropdown.css';
import { Row, Col, Input, Select } from 'antd';

const { Option } = Select;

const RecentActivitesSearchBox = () => {
  return (
    <>
      <div className="Recent_Activites_Search_Box">
        <Row gutter={[10, 10]}>
          <Col xl={6} lg={8} md={8} sm={7} xs={24}>
            <Input placeholder="Search ..." />
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Type" style={{ width: '100%' }}>
              <Option value="">Type 1 </Option>
              <Option value="">Type 2 </Option>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Status" style={{ width: '100%' }}>
              <Option value="">Status 1</Option>
              <Option value="">Status 2</Option>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Comments" style={{ width: '100%' }}>
              <Option value="">Comments 1</Option>
              <Option value="">Comments 2</Option>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={4} sm={4} xs={24}>
            <Select defaultValue="Last Date" style={{ width: '100%' }}>
              <Option value="">Last Date 1</Option>
              <Option value="">Last Date 2</Option>
            </Select>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default RecentActivitesSearchBox;
