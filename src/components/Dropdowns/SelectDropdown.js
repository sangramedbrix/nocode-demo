import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Select } from 'antd';
import './dropdown.css';

const SelectDropdown = () => {
  console.log('PropTypes', PropTypes);

  const handleChange = (event) => {
    PropTypes.categoryChangeFunction(event);
    // console.log('dfsd',event)
  };

  return (
    <>
      <Select
        name="category"
        size={PropTypes.dropdwon_size}
        value={PropTypes.value}
        onChange={handleChange}></Select>
      {/* <Select defaultValue='lucy' style={{ width: 120 }}>
                <Option value='jack'>Jack</Option>
                <Option value='lucy'>Lucy</Option>
                <Option value='disabled' disabled>
                    Disabled
                </Option>
                <Option value='Yiminghe'>yiminghe</Option>
            </Select> */}
    </>
  );
};

export default SelectDropdown;
