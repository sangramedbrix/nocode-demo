import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import './dropdown.css';
import '../../components/Popup/popup.css';
import { Row, Col, Input, Select, DatePicker } from 'antd';

const { Option, OptGroup } = Select;

const SearchBox = (props) => {
  return (
    <>
      <div className="Search_box">
        <Row gutter={[10, 10]}>
          <Col xl={7} lg={8} md={9} sm={9} xs={24}>
            <Input placeholder={props.placeholder} />
          </Col>
          <Col xl={3} lg={4} md={5} sm={5} xs={24}>
            <div className="filter_popup filter_popup_box">
              <Select
                className="filter_popup_box"
                defaultValue="Sort by Layout"
                style={{ width: '100%' }}>
                <OptGroup label="Sort by ">
                  <Option value="">Sort 1</Option>
                  <Option value="">Sort 2</Option>
                </OptGroup>
              </Select>
            </div>
          </Col>
          <Col xl={3} lg={4} md={5} sm={5} xs={24}>
            <Select defaultValue="Filter by " style={{ width: '100%' }}>
              <OptGroup label="Filter by ">
                <Option value="">Filter by A to Z</Option>
                <Option value="">Filter by Z to A</Option>
              </OptGroup>
            </Select>
          </Col>
          <Col xl={3} lg={4} md={5} sm={5} xs={24}>
            <div className="datepicker">
              <DatePicker
                placeholder="Last Date"
                format="MM/DD/YYYY"
                // style={{boxShadow: '0px 2px 10px rgb(0 0 0 / 10%)'}}
              ></DatePicker>
            </div>
            {/* <Select defaultValue="Last Date" style={{ width: '100%' }}>
              <OptGroup label="Date ">
                <Option value="">05 Jan 2022</Option>
                <Option value="">04 jan 2022</Option>
              </OptGroup>
            </Select> */}
          </Col>
        </Row>
      </div>
    </>
  );
};

export default SearchBox;

SearchBox.propTypes = {
  placeholder: PropTypes.string
};
