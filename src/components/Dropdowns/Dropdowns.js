import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Select } from 'antd';
import '../../config/config';
import './dropdown.css';
import PropTypes from 'prop-types';
import axios from 'axios';

const { Option } = Select;

const Dropdowns = (props) => {
  const [categoryName, setCategoryName] = useState('');

  const handleChange = (event) => {
    setCategoryName(event);
    props.categoryChangeFunction(event);
  };

  const getCategory = () => {
    axios
      .post(global.config.API_URL + `/getCategoryById`, { id: props.categoryId })
      .then((response) => {
        console.log('category response', response);
        setTimeout(() => {
          setCategoryName(response.data.data.name);
        }, 2000);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getCategory();
  }, []);

  return (
    <>
      {/* <Select defaultValue={props.dropdown_title}  size={props.dropdwon_size} >
                {
                    props.option.map((optionValue) => {
                        return (
                            <Option value={optionValue.option}>{optionValue.option}</Option>
                        )
                    })
                } 
            </Select> */}

      <Select className="dd-box" value={`${categoryName}`} onChange={handleChange}>
        <option value="">
          <span style={{ color: '#bfbfbf', fontWeight: 500 }}>Select Category</span>
        </option>
        {props.option.map((optionValue, i) => {
          return (
            <Option value={optionValue.name} selected={categoryName == optionValue.name} key={i}>
              {optionValue.name}
            </Option>
          );
        })}
      </Select>
    </>
  );
};

export default Dropdowns;

Dropdowns.propTypes = {
  categoryChangeFunction: PropTypes.func,
  option: PropTypes.array,
  categoryId: PropTypes.string,
  placeholder: PropTypes.string
};
