import React from 'react';
import { Button, Tooltip } from 'antd';
import { AppstoreOutlined, BarsOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import './style.css';

const ViewBoxes = (props) => {
  const navigate = useNavigate();
  console.log('props view box', props);
  return (
    <div style={{ textAlign: 'right' }} className="grid-list-btn">
      <Tooltip title="Grid View">
        <Button
          onClick={() => navigate(props.gridlink, { state: props.data })}
          className={props.grid}
          icon={<AppstoreOutlined />}
        />
      </Tooltip>
      <Tooltip title="List View">
        <Button
          onClick={() => navigate(props.listlink, { state: props.data })}
          className={props.list}
          icon={<BarsOutlined />}
        />
      </Tooltip>
    </div>
  );
};

export default ViewBoxes;

ViewBoxes.propTypes = {
  gridlink: PropTypes.string,
  data: PropTypes.array,
  grid: PropTypes.string,
  listlink: PropTypes.string,
  list: PropTypes.string
};
