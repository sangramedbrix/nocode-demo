import React, { useState } from 'react';
import { DeleteOutlined } from '@ant-design/icons';
import TextHeadingH3 from '../TextHeadings/TextHeadingH3';
import Buttons from '../Buttons/Buttons';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Modal, Button } from 'antd';

const Approve = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <Button type="primary" onClick={showModal}>
        {' '}
        Delete{' '}
      </Button>
      <div className="approol_popup">
        <Modal
          visible={isModalVisible}
          className="delete_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="Delete_popup_box">
            <div className="icon">
              <DeleteOutlined className="d-flex text-center yellow" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3="Are you sure you want to Approve this page?" />
            </div>
            <div className="description">
              <p>{props.popup_description} </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Buttons
                className="cancel"
                button_color="primary"
                button_size="large"
                button_title="Cancel"
              />
              <Buttons
                className="delete bg-red"
                button_color="danger"
                button_size="large"
                button_title="Delete"
              />
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
};

export default Approve;

Approve.propTypes = {
  popup_description: PropTypes.string
};
