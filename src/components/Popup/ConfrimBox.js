// @FIXME: not being used
import React from 'react';
import 'antd/dist/antd.css';
import { Modal, Button, Space } from 'antd';

const ReachableContext = React.createContext();
const UnreachableContext = React.createContext();

const config = {
  title: 'Are you sure you want to cancel this page?',
  // description: 'If you cancel this page, you will lose unsaved data and also
  // not saved in your automatic generated version.'
  content: (
    <>
      <ReachableContext.Consumer>{(name) => `Reachable: ${name}!`}</ReachableContext.Consumer>
      <br />
      <UnreachableContext.Consumer>{(name) => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
    </>
  )
};

const ConfrimBox = () => {
  const [modal, contextHolder] = Modal.useModal();
  return (
    <>
      <ReachableContext.Provider
        value="If you cancel this page, you will lose unsaved data and also
not saved in your automatic generated version.">
        <Space>
          <Button
            onClick={() => {
              modal.confirm(config);
            }}>
            Confirm
          </Button>
        </Space>
        {/* `contextHolder` should always under the context you want to access */}
        {contextHolder}

        {/* Can not access this context since `contextHolder` is not in it */}
        <UnreachableContext.Provider value="sdssdddddddagag" />
      </ReachableContext.Provider>
    </>
  );
};

export default ConfrimBox;
