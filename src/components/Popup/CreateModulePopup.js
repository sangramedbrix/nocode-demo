import React, { useState } from 'react';
import PropTypes from 'prop-types';
import '../../config/config';
import 'antd/dist/antd.css';
import { Modal, Button, Form, Checkbox } from 'antd';
import TextHeadingH4 from '../TextHeadings/TextHeadingH4';
import InputText from '../InputsFields/InputText';
import InputTextArea from '../InputsFields/InputTextArea';
import './popup.css';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

const CreateModulePopup = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [createModule, setModule] = useState({
    name: '',
    description: '',
    isActive: 0,
    nameValidation: '',
    descriptionValidation: '',
    checked: true
  });

  const { state } = useLocation();
  console.log('state valuesss', state);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setModule((prevState) => ({
      ...prevState,
      name: '',
      description: '',
      nameValidation: false,
      descriptionValidation: false
    }));
    setIsModalVisible(false);
  };

  const nameChange = (name) => {
    console.log('Module name', name);
    if (name != '') {
      setModule((prevState) => ({ ...prevState, name: name, nameValidation: false }));
    } else {
      setModule((prevState) => ({ ...prevState, name: name, nameValidation: true }));
      console.log('empty');
    }
  };

  const discriptionChange = (description) => {
    console.log('description', description);
    if (description != '') {
      setModule((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
    } else {
      setModule((prevState) => ({
        ...prevState,
        description: description,
        descriptionValidation: false
      }));
      console.log('empty');
    }
  };

  function onChange(e) {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      setModule((prevState) => ({ ...prevState, isActive: 1 }));
    } else if (e.target.checked === false) {
      setModule((prevState) => ({ ...prevState, isActive: 0 }));
    }
  }

  const createNewModule = () => {
    if (createModule.name != '') {
      const newModuleData = {
        appId: state._id,
        name: createModule.name,
        description: createModule.description,
        isActive: createModule.isActive
      };
      axios
        .post(global.config.API_URL + '/createModule', newModuleData)
        .then((response) => {
          console.log(response);
          if (response.data.message === 'Module created !') {
            setIsModalVisible(false);
            props.getModulesData();
            props.getModulesCount();
            props.getModulesData();
            setModule((prevState) => ({ ...prevState, name: '', description: '' }));
          }
        })
        .catch((error) => console.log(error));
    } else {
      if (createModule.name == '') {
        setModule((prevState) => ({ ...prevState, nameValidation: true }));
      }
    }
  };

  return (
    <>
      {/* <Button type='primary' onClick={showModal}>  Create a Module  </Button> */}
      <div className="upload-container item-center">
        <div className="upload-box">
          <div className="plusicon">
            <i className="icon_plus" onClick={showModal} />
          </div>
          <div className="addModule">{props.moduleName}</div>
        </div>
      </div>
      {console.log(props.moduleName)}

      {(() => {
        if (props.moduleName === 'Add New Module') {
          return (
            <div className="create_module">
              <Modal
                destroyOnClose={true}
                visible={isModalVisible}
                className="create_module_popup"
                onCancel={handleCancel}
                closable={false}
                header={false}
                footer={false}>
                <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Module" />

                <Form layout="vertical">
                  <Form.Item
                    label="Module Name"
                    name="Module Name"
                    rules={[
                      {
                        required: true
                      }
                    ]}>
                    <InputText
                      placeholder_text="Enter Module Name"
                      value={createModule.name}
                      nameChangeFunction={nameChange}
                    />
                    {createModule.nameValidation === true ? (
                      <p className="text-danger"> Module name is required</p>
                    ) : (
                      ''
                    )}
                  </Form.Item>

                  <Form.Item label="Description">
                    <InputTextArea
                      discriptionChangeFunction={discriptionChange}
                      value={createModule.description}
                    />
                  </Form.Item>

                  <Form.Item
                    label="Status"
                    name="Status"
                    valuePropName="checked"
                    wrapperCol={{
                      offset: 1,
                      span: 25
                    }}>
                    <Checkbox onChange={onChange}>
                      <span className="module_check_box" style={{ fontSize: 12, color: '#595959' }}>
                        Yes, I agree to show my module to public
                      </span>
                    </Checkbox>
                  </Form.Item>

                  <Form.Item className="text-center">
                    <div className="btn_div">
                      <Button className="cancel_btn" type="link" onClick={handleCancel}>
                        {' '}
                        Cancel
                      </Button>
                      <Button
                        className="submit_btn"
                        type="primary"
                        Type="submit"
                        onClick={createNewModule}>
                        {' '}
                        Submit{' '}
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </Modal>
            </div>
          );
        } else if (props.moduleName === 'Create New Page') {
          return (
            <div className="create_page">
              <Modal
                className="create_page_popup"
                visible={isModalVisible}
                //className={'create_module_popup'}
                onCancel={handleCancel}
                closable={false}
                header={false}
                footer={false}>
                <TextHeadingH4 className="heading d-flex text-center" text_h4="Create a Page" />
                <Form layout="vertical">
                  <Form.Item label="Page Name" name="Page Name" rules={[{ required: true }]}>
                    <InputText placeholder_text="Enter Page Name" />
                  </Form.Item>

                  <Form.Item label="Description" rules={[{ required: true }]}>
                    <InputTextArea />
                  </Form.Item>

                  <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
                    <Button type="link" onClick={handleCancel}>
                      {' '}
                      Cancel
                    </Button>
                    <Button type="primary" Type="submit">
                      {' '}
                      Submit{' '}
                    </Button>
                  </Form.Item>
                </Form>
              </Modal>
            </div>
          );
        }
      })()}
    </>
  );
};

export default CreateModulePopup;

CreateModulePopup.propTypes = {
  moduleName: PropTypes.string,
  getModulesData: PropTypes.func,
  getModulesCount: PropTypes.func
};
