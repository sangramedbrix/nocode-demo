import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Modal } from 'antd';
import { WarningOutlined } from '@ant-design/icons';
import TextHeadingH3 from '../TextHeadings/TextHeadingH3';
import Buttons from '../Buttons/Buttons';
import './popup.css';
import { useNavigate, useLocation, useParams } from 'react-router-dom';

const CancelPage = (props) => {
  const navigate = useNavigate();

  const { appId, moduleId, pageId } = useParams();
  console.log('APPIS', appId);
  console.log('moduleId', moduleId);
  console.log('pageId', pageId);

  const location = useLocation();
  console.log('location in cancel page', location.state);

  console.log('props', props);
  const handleCancel = () => {
    props.handleCancel();
  };

  const pageCancel = () => {
    navigate(`/app/${appId}/module/${moduleId}`);
  };

  const pageGoBack = () => {
    props.handleCancel();
  };

  return (
    <>
      {/* <Button type="primary" onClick={showModal}>
        {' '}
        Cancel page{' '}
      </Button> */}
      <div className="cancel_page">
        <Modal
          visible={props.isModallVisible}
          className="cancel_popup"
          onCancel={handleCancel}
          // closable={false}
          header={false}
          footer={false}>
          <div className="confirm_box">
            <div className="icon">
              <WarningOutlined className="d-flex text-center" />
            </div>
            <div className="heading">
              <TextHeadingH3 text_h3="Are you sure you want to cancel this page?" />
            </div>
            <div className="description">
              <p>{props.popup_description} </p>
            </div>
            <div className="buttons_div d-flex text-center">
              <Buttons
                button_color="primary"
                button_size="large"
                button_title="Yes!"
                pageCanelYes={pageCancel}
              />
              <Buttons
                button_color="danger"
                button_size="large"
                button_title="No, Go Back!"
                pageCanelYes={pageGoBack}
              />
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
};

export default CancelPage;

CancelPage.propTypes = {
  popup_description: PropTypes.string,
  handleCancel: PropTypes.func,
  pageCancelYes: PropTypes.string,
  isModallVisible: PropTypes.string
};
