// @FIXME: not being used
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { Modal, Button, Form } from 'antd';
import InputText from '../InputsFields/InputText';
import InputTextArea from '../InputsFields/InputTextArea';
import TextHeadingH4 from '../TextHeadings/TextHeadingH4';
import './popup.css';

const CreatePagePopup = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  console.log('PropTypes values', PropTypes);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    console.log('ok');
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    console.log('cancel');
    setIsModalVisible(false);
  };

  useEffect(() => {
    setIsModalVisible(PropTypes.isModalVisible);
    console.log('PropTypes values effect', PropTypes);
  }, [PropTypes]);

  return (
    <>
      <div className="create_page">
        <Button type="primary" onClick={showModal}>
          {' '}
          Create page{' '}
        </Button>
        <Modal
          className="create_page_popup"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
          header={false}
          footer={false}>
          <TextHeadingH4 className="heading " text_h4="Create Page" />
          <Form layout="vertical">
            <Form.Item
              label="Page Name"
              name="Page Name"
              rules={[
                {
                  required: true
                }
              ]}>
              <InputText placeholder_text="Enter Page Name" />
            </Form.Item>

            <Form.Item label="Description">
              <InputTextArea />
            </Form.Item>

            <Form.Item>
              <div className="btn_div">
                <Button className="cancel_btn" type="link" onClick={handleCancel}>
                  {' '}
                  Cancel
                </Button>
                <Button className="submit_btn" type="primary" Type="submit">
                  {' '}
                  Submit{' '}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </>
  );
};

export default CreatePagePopup;
