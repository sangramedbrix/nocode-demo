// @FIXME: not being used
import React from 'react';
import 'antd/dist/antd.css';
import { Popover, Button, Divider } from 'antd';
import './popup.css';

const content = (
  <div className="action_box">
    <p>Preview</p>
    <Divider />
    <p>Edit</p>
    <Divider />
    <p>Delete</p>
    <Divider />
    <p>Rename</p>
  </div>
);

const ActionPopup = () => {
  return (
    <>
      <div className="action_popup">
        <Popover placement="bottomRight" content={content} trigger="click">
          <Button>Action Button</Button>
        </Popover>
      </div>
    </>
  );
};

export default ActionPopup;
