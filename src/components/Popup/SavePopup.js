// @FIXME: not being used
import React from 'react';
import 'antd/dist/antd.css';
import { Popover, Button, Divider } from 'antd';
import './popup.css';

const content = (
  <div className="save_popup_box">
    <p>Save as Draft</p>
    <Divider />
    <p>Save as Local Copy</p>
    <Divider />
    <p>Save as Version</p>
  </div>
);

const SavePopup = () => {
  return (
    <>
      <div className="save_popup">
        <Popover placement="bottomRight" content={content} trigger="click">
          <Button>Save Button</Button>
        </Popover>
      </div>
    </>
  );
};

export default SavePopup;
