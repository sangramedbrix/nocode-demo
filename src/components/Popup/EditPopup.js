// @FIXME: not being used
import React from 'react';
import 'antd/dist/antd.css';
import { Popover, Button, Divider } from 'antd';
import './popup.css';

const content = (
  <div className="edit_popup_box">
    <p>Edit Page Settings</p>
    <Divider />
    <p>Show Version History</p>
    <Divider />
    <p>Export to PDF</p>
  </div>
);

const EditPopup = () => {
  return (
    <>
      <div className="edit_popup">
        <Popover placement="bottomRight" content={content} trigger="click">
          <Button>Edit Button</Button>
        </Popover>
      </div>
    </>
  );
};

export default EditPopup;
