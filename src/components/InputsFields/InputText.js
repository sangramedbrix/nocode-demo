import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';
import { useForm } from 'react-hook-form';

const InputText = (props) => {
  console.log('props value', props.value);
  const {
    formState: { errors }
  } = useForm();
  const handleChange = (event) => {
    props.nameChangeFunction(event.target.value);
    console.log('sdfsdfs', errors);
  };
  return (
    <>
      <Input
        placeholder={props.placeholder_text}
        size={props.input_size}
        // value={props.value}
        defaultValue={props.value}
        onChange={handleChange}
        className="input-box-dark"></Input>
    </>
  );
};

export default InputText;

InputText.propTypes = {
  placeholder_text: PropTypes.string,
  input_size: PropTypes.string,
  value: PropTypes.string,
  nameChangeFunction: PropTypes.func
};
