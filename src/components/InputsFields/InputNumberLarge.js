// @FIXME: not being used
import React from 'react';
import { InputNumber } from 'antd';

function onChange(value) {
  console.log('changed', value);
}

const InputNumberLarge = () => {
  return (
    <>
      <InputNumber
        size="large"
        className="mr-2"
        min={1}
        max={100000}
        defaultValue={3}
        onChange={onChange}
      />
    </>
  );
};

export default InputNumberLarge;
