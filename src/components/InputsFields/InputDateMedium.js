// @FIXME: not being used
import React from 'react';
import { DatePicker } from 'antd';

function onChange(value) {
  console.log('changed', value);
}

const InputDateMedium = () => {
  return (
    <>
      <DatePicker onChange={onChange} size="middle" />
    </>
  );
};

export default InputDateMedium;
