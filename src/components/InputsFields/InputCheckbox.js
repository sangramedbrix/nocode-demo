import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from 'antd';

const InputCheckbox = (props) => {
  console.log(props);

  function onChange(e) {
    console.log(`checked = ${e.target.checked}`);
    if (e.target.checked === true) {
      props.statusCheck(1);
    } else if (e.target.checked === false) {
      props.statusCheck(0);
    }
  }

  useEffect(() => {}, []);

  return (
    <>
      <Checkbox onChange={onChange} size="small">
        {props.check_text}
      </Checkbox>
    </>
  );
};

export default InputCheckbox;

InputCheckbox.propTypes = {
  statusCheck: PropTypes.func,
  check_text: PropTypes.string
};
