import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';
const { TextArea } = Input;

const InputTextArea = (props) => {
  const handleChange = (event) => {
    props.discriptionChangeFunction(event.target.value);
    // console.log('dfsd',event)
  };

  return (
    <>
      <TextArea
        rows={3}
        placeholder={props.placeholder_text}
        defaultValue={props.value}
        onChange={handleChange}
        className="input-box-dark"
      />
    </>
  );
};

export default InputTextArea;

InputTextArea.propTypes = {
  discriptionChangeFunction: PropTypes.func,
  placeholder_text: PropTypes.string,
  value: PropTypes.string
};
