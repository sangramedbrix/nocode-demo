// @FIXME: not being used
import React from 'react';
import { InputNumber } from 'antd';

function onChange(value) {
  console.log('changed', value);
}

const InputNumberMedium = () => {
  return (
    <>
      <InputNumber
        size="middle"
        className="mr-2"
        min={1}
        max={100000}
        defaultValue={3}
        onChange={onChange}
      />
    </>
  );
};

export default InputNumberMedium;
