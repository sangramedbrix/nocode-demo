// @FIXME: not being used
import React from 'react';
import { DatePicker } from 'antd';

function onChange(value) {
  console.log('changed', value);
}

const InputDateLarge = () => {
  return (
    <>
      <DatePicker onChange={onChange} size="large" />
    </>
  );
};

export default InputDateLarge;
