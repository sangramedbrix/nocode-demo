import React from 'react';
import PropTypes from 'prop-types';
import { Button, Upload } from 'antd';
import '../../assets/styles/index.css';

const InputFile = (props) => {
  const handleChange = (response) => {
    console.log('response', response);
    props.fileSelect(response);
  };

  return (
    <>
      <Upload
        name="logo"
        action="//jsonplaceholder.typicode.com/posts/"
        listType="picture"
        onChange={handleChange}>
        <div className="choose-file-btn">
          <Button>Choose file</Button>
          {props.text}
        </div>
      </Upload>
    </>
  );
};

export default InputFile;

InputFile.propTypes = {
  fileSelect: PropTypes.func,
  text: PropTypes.string
};
