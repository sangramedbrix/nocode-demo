import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

const InputEmail = (props) => {
  return (
    <>
      <Input placeholder={props.placeholder_text} size={props.input_size} className="mtb-1"></Input>
    </>
  );
};

export default InputEmail;

InputEmail.propTypes = {
  placeholder_text: PropTypes.string,
  input_size: PropTypes.string
};
