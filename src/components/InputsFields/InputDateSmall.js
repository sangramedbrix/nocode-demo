// @FIXME: not being used
import React from 'react';
import { DatePicker } from 'antd';

function onChange(value) {
  console.log('changed', value);
}

const InputDateSmall = () => {
  return (
    <>
      <DatePicker onChange={onChange} size="small" />
    </>
  );
};

export default InputDateSmall;
