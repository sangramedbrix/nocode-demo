// @FIXME: not being used
import React from 'react';
import PropTypes from 'prop-types';
import { InputNumber } from 'antd';

function onChange(value) {
  console.log('changed', value);
}

const InputNumbers = (props) => {
  return (
    <>
      <InputNumber
        size={props.input_size}
        min={1}
        max={100000}
        defaultValue={3}
        onChange={onChange}
      />
    </>
  );
};

export default InputNumbers;

InputNumbers.propTypes = {
  input_size: PropTypes.string
};
